@php
    $paylink = "https://lk.timurgilmanov.com/product/wakeup-discount/"
@endphp
    <!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>Пробуждение женщины! Wakeup!</title>

    <link rel="stylesheet" href="{{asset('css/all/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/all/swiper.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="{{asset('css/wakeup/style.css')}}">

    <link rel="canonical" href="{{route('lander.wakeup')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="196x196" href="{{asset('img/icons/favicon-196.png')}}"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/icons/apple-icon-180.png')}}"/>
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset('img/icons/apple-icon-167.png')}}"/>
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/icons/apple-icon-152.png')}}"/>
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/icons/apple-icon-120.png')}}"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <script src="{{asset('js/all/sweetalert.min.js') }}"></script>
</head>

<body onunload="deleteAllCookies()">
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(66951796, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/66951796" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<div class="container">
    <div class="bg-light-green" id="bg-light-green">
        <div class="bg-dark-green">

            <header>

                <div class="row pt-2">
                    <a href="/" class="col-12 text-center-desktop">
                        <img src="{{asset('img/wakeup/v2/logo@2x.png')}}" alt=""
                             class="top-logo mw-80 animate__animated animate__backInDown animate__slow">
                    </a>
                </div>

                <div class="row">
                    <div class="col-12 animate__animated animate__fadeIn animate__slower">
                        <div class="text-t text-center pl-5 pr-5 mt-2  letter-spacing-3">
                            послe
                        </div>
                        <h1 class="header-text  mt-2 text-uppercase text-center text-shadow-red">
                            15 сентября
                        </h1>
                        <div class="text-t text-center pl-5 pr-5 bebas-top-fixer">
                            ваша жизнь не будет
                            такой как прежде
                        </div>
                    </div>
                </div>

                <div class="row pt-3 animate__animated animate__fadeIn animate__slower">
                    <div class="col-12 text-center">
                        <img src="{{asset('img/wakeup/v2/logo-green@2x.png')}}" class="img-fluid green-logo" alt="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 animate__animated animate__fadeIn animate__slower">
                        <div class="text-t text-center pl-5 pr-5 mt-2">
                            представляет легендарный курс
                        </div>
                    </div>
                </div>

                <div class="row pt-4">
                    <div class="col-12 animate__animated animate__fadeIn animate__slower">
                        <h2 class="header-text text-uppercase text-center text-shadow-red">
                            пробуждение женщины
                        </h2>
                    </div>
                </div>


            </header>

            <section id="slider">
                <div class="row pt-5">
                    <div class="col-12">
                        <div class="position-relative">
                            <img
                                {{--                                    src="{{asset('img/wakeup/v2/slide-1.png')}}"--}}
                                src=""
                                data-slide-1-m="{{asset('img/wakeup/v2/slide-1.png')}}"
                                data-slide-2-m="{{asset('img/wakeup/v2/slide-2.png')}}"
                                data-slide-1-d="{{asset('img/wakeup/v2/slide-1-d.png')}}"
                                data-slide-2-d="{{asset('img/wakeup/v2/slide-2-d.png')}}"
                                class="bg-img" alt="">
                            <div class="slider">
                                <div class="slider-inner">
                                    <div class="slide-text1 text-center pl-5 pr-5 m-auto">
                                        Начни жить жизнью мечты
                                        <div id="or-action" class="or-button btn-custom">или</div>
                                    </div>
                                    <div class="slide-text2 text-center  pl-5 pr-5 m-auto">
                                        Или погрязни в рутине
                                    </div>
                                    <div id="bottom-swiper-flag" class="position-absolute"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="course-info">
                <div class="row p-5 ">
                    <div class="col-12 fulltext  font-din fs-18">

                        Вы ведь хотите чтоб в конце курса, у вас как и у всех вокруг были результаты - расцвели,
                        насладились, встретились, полюбили, забеременели после тысячи неудачных попыток, родили, создали
                        бизнес, исполнили мечты, изменили свою судьбу, получили количественные и качественные
                        результаты, завершили День Сурка, вышли на новый уровень, выздоровели, вздохнули, задышали,
                        открылись, улыбнулись, расслабились, приняли, поблагодарили, начали жить здесь и сейчас, стали
                        счастливы, полюбили и стали любимыми️ ..., услышали голос Души, начали управлять ресурсами,
                        энергией и своей жизнью….

                        <div class="show-full-text font-din fs-18">
                            <br>
                            Все еще ждете волшебную таблетку? Вы ищите секретный инсайты? Прошли несколько курсов
                            в попытке самопознания? <br><br>

                            На самом деле из курса «Пробуждение женщины» вы узнаете, что самое главное - это Душа и
                            БОГатство Души, что вы- причина, а не следствие, что у вас оказывается есть силы и
                            возможности самим изменить и настроить свою жизнь , управлять ею, в полном согласии с
                            Душой и Богом (без неврозов и иных патологий ), что вы рождены счастливым человеком и не
                            важно - мужчиной или женщиной и имеете полное право быть таковым, что вы можете быть и жить
                            прямо сейчас. У вас есть силы терпеливо претерпеть во времени все трудности и лишения в
                            путешествии из страдания в счастье, из прошлого в будущее с помощью своего труда, ещё раз
                            - своего труда и ещё раз - своего труда!<br><br>

                            А в этом вам поможет энергия, эмоциональность и профессионализм Тимура Гильманова и его
                            команды . От соприкосновения с энергетикой этого курса, в воспитательной и обучающей форме
                            - изменения и трансформации начнутся в вас - мягко, мощно и очень глубоко. Вы - уже в
                            Пути. И это - так просто. И это так - Страшно - потому что по старому - уже невозможно, а
                            по новому - дух захватывает!
                        </div>
                        <div class="pt-3 text-center">
                            <a class="more-link" href="#" id="full-text">
                                подробнее
                            </a>
                        </div>
                    </div>

                </div>
            </section>

            <section id="swiper">
                <div class="row pt-3 pl-5 pr-5">
                    <div class="col-12">
                        <h2 class="header-text text-uppercase text-center text-shadow-red">
                            Этот курс для вас если:
                        </h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 pt-3">
                        <div class="swiper-container pb-3 ml-5 mr-5" id="swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide pb-3">
                                    <div class="col-12 text-center">
                                        <img class="swipet-icon img-fluid"
                                             src="{{asset('img/wakeup/v2/swiper-icon-1@2x.png')}}" alt="">
                                    </div>
                                    <div class="col-12">
                                        <p class="swipet-text text-center font-din pt-3 pl-3 pr-3">
                                            - ты недовольна собой или своей жизнью
                                        </p>
                                    </div>
                                </div>
                                <div class="swiper-slide pb-3">
                                    <div class="col-12 text-center">
                                        <img class="swipet-icon img-fluid"
                                             src="{{asset('img/wakeup/v2/swiper-icon-2@2x.png')}}" alt="">
                                    </div>
                                    <div class="col-12">
                                        <p class="swipet-text text-center font-din pt-3 pl-3 pr-3">
                                            - ты бесконечно конфликтуешь с мужем, детьми и мамой
                                        </p>
                                    </div>
                                </div>
                                <div class="swiper-slide pb-3">
                                    <div class="col-12 text-center">
                                        <img class="swipet-icon img-fluid"
                                             src="{{asset('img/wakeup/v2/swiper-icon-3@2x.png')}}" alt="">
                                    </div>
                                    <div class="col-12">
                                        <p class="swipet-text text-center font-din pt-3 pl-3 pr-3">
                                            - тебя перeполняют обиды
                                        </p>
                                    </div>
                                </div>

                                <div class="swiper-slide pb-3">
                                    <div class="col-12 text-center">
                                        <img class="swipet-icon img-fluid"
                                             src="{{asset('img/wakeup/v2/swiper-icon-4@2x.png')}}" alt="">
                                    </div>
                                    <div class="col-12">
                                        <p class="swipet-text text-center font-din pt-3 pl-3 pr-3">
                                            - ты переживаешь о будущем и жалеешь о прошлом
                                        </p>
                                    </div>
                                </div>

                                <div class="swiper-slide pb-3">
                                    <div class="col-12 text-center">
                                        <img class="swipet-icon img-fluid"
                                             src="{{asset('img/wakeup/v2/swiper-icon-5@2x.png')}}" alt="">
                                    </div>
                                    <div class="col-12">
                                        <p class="swipet-text text-center font-din pt-3 pl-3 pr-3">
                                            -ты устала ходить по тренингам в поисках смысла жизни
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-pagination"></div>
                            <!-- If we need navigation buttons -->
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                </div>

            </section>

            <section id="course-stats">
                <div class="row pt-3">
                    <div class="col-12">
                        <h2 class="header-text green-text text-center">
                            пробуждение женщины
                        </h2>
                        <div class="col-12">
                            <h3 class="font-bebas text-uppercase text-center">
                                -это легендарный курс
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-5 col-lg-3 offset-sm-1 offset-lg-0 mb-4">
                        <div class="course-stats-number-out position-relative text-center">
                            <img src="{{asset('img/wakeup/v2/10@2x.png')}}"
                                 class="course-stats-number img-fluid  m-auto" alt="">
                            <div class="course-stats-text green-text position-absolute w-100 text-center">
                                дней
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-5 col-lg-3 mb-4">
                        <div class="course-stats-number-out position-relative text-center">
                            <img src="{{asset('img/wakeup/v2/10@2x.png')}}"
                                 class="course-stats-number img-fluid  m-auto" alt="">
                            <div class="course-stats-text green-text position-absolute w-100 text-center">
                                уроков
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-5 col-lg-3 offset-sm-1 offset-lg-0  mb-4">
                        <div class="course-stats-number-out position-relative text-center">
                            <img src="{{asset('img/wakeup/v2/4@2x.png')}}"
                                 class="course-stats-number img-fluid  m-auto" alt="">
                            <div class="course-stats-text green-text position-absolute w-100 text-center">
                                вебинара
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-5 col-lg-3  mb-4">
                        <div class="course-stats-number-out position-relative text-center">
                            <img src="{{asset('img/wakeup/v2/1@2x.png')}}"
                                 class="course-stats-number img-fluid  m-auto" alt="">
                            <div class="course-stats-text green-text position-absolute w-100 text-center">
                                чат
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="author">
                <div class="row pt-3">
                    <div class="col-12">
                        <h2 class="header-text green-text text-center">
                            Автор
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <img class="author-icon img-fluid"
                             src="{{asset('img/wakeup/v2/timur@2x.png')}}" alt="">
                    </div>
                </div>

                <div class="row mt-2 pt">
                    <div class="col-12">
                        <div class="pl-4 pr-5 ml-5 font-din">
                            - Практикующий психолог, многодетный отец
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12">
                        <div class="pl-4 pr-5 ml-5 font-din">
                            - Блоггер - более 200 полезных и ёмких видео на тему личного развития
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12">
                        <div class="pl-4 pr-5 ml-5 font-din">

                            - Основатель «Woman Love Inside».
                        </div>
                    </div>
                </div>
                 <div class="row mt-2">
                    <div class="col-12">
                        <div class="pl-4 pr-5 ml-5 font-din">
                            - Писатель.
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-8 offset-2">
                        <div class="signature-out d-flex justify-content-end">
                            {{--                            <img src="{{asset('img/wakeup/v2/signature@2x.png')}}" class="signature img-fluid"--}}
                            {{--                                 alt="">--}}
                            <div class="signature">
                                <svg class="" version="1.1" x="0px" y="0px" viewBox="0 0 548.5 159.9"
                                     xml:space="preserve">

                                <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                      stroke-linejoin="round" d="M64.5,146.4c-0.1,0-0.1,0-0.3-0.1c-0.1-0.1-0.2-0.1-0.2-0.2c-0.2-0.1-0.3-0.3-0.4-0.5c-0.1-0.2-0.1-0.7-0.1-1.6
                                    c0-0.9,0.1-2.1,0.3-3.5c0.2-1.5,0.6-3.7,1.1-6.6s1.3-6.4,2.2-10.5c2.6-11.9,6.2-27,10.9-45.3c5.7-22.8,10.5-41.6,14.4-56.5
                                    c-16.8,1.5-32.2,3.8-46.2,6.8c-14,3-24,6.6-30.1,10.5c-3.2,2-4.9,4.2-5.3,6.4c-0.1,0.2-0.2,0.4-0.4,0.5C10.3,46,10.1,46,9.9,46
                                    c-0.2-0.1-0.4-0.2-0.6-0.4c-0.2-0.2-0.2-0.4-0.2-0.6c0.5-2.7,2.5-5.2,6.1-7.5c5.4-3.6,14.3-6.9,26.6-9.9c12.3-3,26.6-5.4,42.9-7.1
                                    c0.9-0.1,2.2-0.2,4.1-0.3c1.9-0.1,3.2-0.3,4.2-0.3c1.2-4.2,1.7-6.5,1.7-6.7c0-0.2,0.2-0.4,0.4-0.5c0.2-0.2,0.5-0.2,0.7-0.1
                                    c0.2,0,0.4,0.2,0.5,0.4c0.1,0.2,0.2,0.5,0.1,0.7c0,0.1-0.5,2.1-1.6,6.1c13.5-1.1,26-1.6,37.6-1.3c11.6,0.2,20.7,1.2,27.1,2.8
                                    c0.2,0,0.4,0.2,0.5,0.4c0.2,0.2,0.2,0.5,0.1,0.7c0,0.2-0.2,0.4-0.4,0.5c-0.3,0.1-0.5,0.2-0.7,0.1c-6.6-1.6-15.9-2.5-27.8-2.7
                                    c-12-0.2-24.3,0.3-37,1.4C87.8,46.7,83,65.3,80,77.1C69.4,119.4,64.5,142,65.2,145c0.3,0.3,0.3,0.7,0,1
                                    C64.9,146.2,64.7,146.4,64.5,146.4z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round" d="M96,91.8c-1.4,0-2.6-0.4-3.6-1.1c-2.5-2.1-2.3-7.5,0.6-16.2c0.1-0.2,0.2-0.4,0.5-0.5c0.2-0.1,0.4-0.1,0.6-0.1
                                    c0.2,0.1,0.4,0.2,0.5,0.5c0.1,0.2,0.1,0.4,0,0.6c-2.7,7.8-3.1,12.6-1.1,14.3c0.6,0.5,1.4,0.8,2.5,0.8c1.1,0,2.4-0.2,3.8-0.6
                                    c1.4-0.4,3.3-1.2,5.7-2.3c2.4-1.1,4.1-2,5.2-2.5c1-0.5,3.2-1.7,6.4-3.4l1.7-0.9c0.2-0.1,0.4-0.1,0.7,0c0.3,0.1,0.4,0.2,0.5,0.4
                                    c0.1,0.2,0.1,0.5,0,0.7c-0.1,0.3-0.2,0.4-0.4,0.5l-1.7,0.8c-2.9,1.6-5.1,2.7-6.7,3.5c-1.6,0.8-3.4,1.7-5.6,2.7
                                    c-2.2,1-4,1.7-5.6,2.2C98.6,91.6,97.2,91.8,96,91.8z M95.3,69.2c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.1,0c-0.2,0-0.4-0.2-0.5-0.4
                                    c-0.1-0.2-0.2-0.5-0.1-0.7c0.7-2.8,1.7-4.4,3-4.8c0.7-0.3,1.4-0.2,2.1,0.3c0.2,0.1,0.3,0.3,0.3,0.6c0,0.3,0,0.5-0.1,0.7
                                    c-0.1,0.2-0.3,0.3-0.6,0.3c-0.3,0-0.5,0-0.6-0.1L98,64.9c-0.3,0.1-0.7,0.6-1,1.3c-0.4,0.8-0.7,1.5-0.9,2.3
                                    C95.9,69,95.7,69.2,95.3,69.2z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round" d="M118.6,91.3c0,0-0.1,0-0.2,0c-0.1,0-0.1,0-0.2,0c-0.4-0.1-0.6-0.5-0.7-1c-0.1-0.5-0.1-1.5,0.1-2.9
                                    c0.1-1.2,0.5-3.3,1.1-6.2l1.8-9c0.1-0.2,0.2-0.4,0.4-0.6s0.4-0.2,0.6-0.2c0.2,0,0.4,0.2,0.6,0.4c0.2,0.3,0.2,0.5,0.2,0.7
                                    c-1.6,6.9-2.5,11.8-2.9,14.9c1.4-3.2,2.2-5,2.2-5.2c1.8-4.2,3-6.9,3.7-8.1c0.7-1.2,1.3-1.8,1.9-1.9c0.5,0,0.9,0.2,1.1,0.5
                                    c0.7,1.2,0.8,5.1,0.3,11.8c2-3.1,3.7-5.1,5.4-5.8c0.5-0.3,1.1-0.4,1.6-0.5c0.5-0.1,1.1,0,1.8,0.2c0.7,0.2,1.3,0.3,1.7,0.5
                                    c0.4,0.1,1.1,0.4,2.1,0.8c1.5,0.6,2.8,1.1,3.7,1.4c0.9,0.3,2.5,0.7,4.7,1.1c2.2,0.4,4.3,0.7,6.4,0.7c2.1,0,4.8-0.2,8.1-0.6
                                    c3.3-0.4,6.8-1.1,10.5-2.1c0.2-0.1,0.4,0,0.7,0.1c0.2,0.1,0.3,0.3,0.4,0.5c0.1,0.6-0.1,1-0.5,1.1c-3.8,1-7.4,1.7-10.8,2.2
                                    c-3.4,0.4-6.2,0.7-8.3,0.7c-2.2,0-4.4-0.2-6.6-0.7c-2.2-0.4-3.9-0.8-4.9-1.2c-1-0.3-2.3-0.8-3.8-1.4c-0.8-0.3-1.4-0.5-1.8-0.7
                                    s-0.9-0.3-1.4-0.4c-0.5-0.1-1-0.2-1.4-0.1c-0.4,0-0.8,0.1-1.2,0.3c-1.6,0.7-3.8,3.5-6.6,8.4c-0.2,0.4-0.6,0.5-1.1,0.4
                                    c-0.4-0.1-0.6-0.4-0.6-1c0.6-5.6,0.7-10.1,0.5-13.4c-0.2,0.5-1.4,3.2-3.7,8.1c-0.9,2.2-1.7,4.1-2.4,5.6
                                    C120,90.4,119.3,91.3,118.6,91.3z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round" d="M174.2,92.8h-0.4c-0.7-0.1-1.1-0.4-1.4-1.1c-0.9-1.6-0.3-5.7,1.9-12.2c0.5-1.4,1-2.8,1.5-4.4c0.6-1.5,1-2.7,1.4-3.5
                                    l0.5-1.2c0.1-0.2,0.2-0.3,0.5-0.4c0.2-0.1,0.5-0.1,0.7,0c0.2,0.1,0.3,0.2,0.4,0.5c0.1,0.2,0.1,0.5,0,0.7c-1.2,2.8-2.3,5.8-3.4,8.9
                                    c-2,5.9-2.6,9.6-2,10.9l0.2,0.2c1.4,0,3.8-2.7,7-8.1c0.8-1.3,1.5-2.7,2.3-4.2c0.8-1.5,1.4-2.7,1.8-3.5l0.6-1.3
                                    c0.2-0.4,0.6-0.6,1.1-0.5c0.4,0.1,0.6,0.4,0.6,1c-0.8,5.9-0.3,9.5,1.5,11c0.4,0.3,1,0.6,1.6,0.7c0.6,0.1,1.4,0.1,2.4-0.1
                                    c0.9-0.2,1.8-0.3,2.7-0.6c0.8-0.2,2-0.6,3.4-1.1s2.6-1,3.6-1.4c1-0.4,2.4-0.9,4.1-1.7c0.3-0.1,0.7-0.3,1.3-0.5
                                    c0.6-0.2,1-0.4,1.3-0.5c0.2-0.1,0.4-0.1,0.7,0c0.2,0.1,0.4,0.3,0.5,0.5c0.1,0.2,0.1,0.4,0,0.7s-0.2,0.4-0.4,0.5
                                    c-0.3,0.1-0.7,0.3-1.3,0.5s-1,0.4-1.3,0.5c-2.1,0.8-3.6,1.5-4.6,1.8c-1,0.4-2.3,0.9-3.9,1.4c-1.6,0.6-2.8,1-3.7,1.1
                                    c-0.8,0.2-1.8,0.3-2.8,0.4c-1,0.1-1.9,0.1-2.6-0.1c-0.7-0.2-1.4-0.5-2-0.9c-1.7-1.3-2.5-4-2.4-8c-0.2,0.3-1.1,2-2.9,5.1
                                    C179,89.8,176.2,92.8,174.2,92.8z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round"
                                          d="M218.3,91.2c-0.5,0-0.8-0.1-1-0.3c-0.2-0.2-0.4-0.5-0.4-0.8c-0.1-0.3,0-0.8,0.1-1.4c0.1-0.6,0.3-1.2,0.4-1.8
                                    c0.2-0.5,0.4-1.4,0.8-2.5c0.9-2.8,1.4-4.2,1.4-4.4c0.1-0.5,0.2-0.9,0.2-1.3s0-0.6-0.1-0.7l-0.1-0.1h-0.2c-2.8,0.1-5.1-0.2-6.9-0.8
                                    c-1.1,1.9-1.9,3.3-2.3,4.2c-0.1,0.2-0.2,0.3-0.5,0.4c-0.2,0.1-0.5,0.1-0.7,0c-0.2-0.1-0.3-0.3-0.4-0.5c-0.1-0.3-0.1-0.5,0-0.7
                                    c0.3-0.7,1.1-2.1,2.3-4.2c-0.9-0.6-1.5-1.3-1.7-2.2c-0.5-1.5-0.3-3.1,0.7-4.8c1-1.8,2.2-3.2,3.5-4.2c1.4-1.1,2.5-1.3,3.3-0.6
                                    c0.9,0.6,0.9,2.1,0.1,4.5c-0.5,1.2-1.6,3.4-3.5,6.6c1.5,0.5,3.5,0.7,5.9,0.6c0.7,0,1.3,0.2,1.6,0.7c0.3,0.3,0.4,0.8,0.5,1.3
                                    c0.1,0.5,0,1.1-0.1,1.9c-0.1,0.7-0.3,1.5-0.6,2.2c-0.2,0.8-0.5,1.7-0.9,2.8c-0.1,0.3-0.2,0.8-0.5,1.4c-0.2,0.7-0.4,1.2-0.5,1.7
                                    c-0.1,0.5-0.2,0.8-0.2,1.1c1.1-0.5,2.5-1.7,4.3-3.6c0.7-0.8,1.3-1.4,1.7-1.8s1-0.9,1.7-1.5s1.5-1.1,2.2-1.4
                                    c0.8-0.3,1.6-0.6,2.4-0.7c0.2,0,0.4,0,0.6,0.2c0.2,0.2,0.4,0.4,0.4,0.6c0,0.2,0,0.4-0.2,0.6c-0.2,0.2-0.4,0.3-0.6,0.4
                                    c-0.9,0.1-1.9,0.5-2.8,1.1c-0.9,0.6-1.7,1.1-2.2,1.6c-0.6,0.5-1.3,1.3-2.1,2.2c-0.7,0.8-1.3,1.4-1.6,1.7c-0.3,0.3-0.8,0.7-1.4,1.3
                                    c-0.6,0.5-1.2,0.9-1.8,1.1C219.1,91.2,218.8,91.2,218.3,91.2z M215.8,66c-0.3,0-0.8,0.3-1.5,0.8c-0.7,0.6-1.4,1.3-2,2.1
                                    c-1.5,2-1.9,3.7-1.2,5.1c0.3,0.4,0.5,0.7,0.8,0.8c1.9-3.3,3-5.4,3.4-6.4C215.6,67.5,215.8,66.7,215.8,66z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round"
                                          d="M271.3,152.8c-1,0-1.9-0.1-2.7-0.3c-3-0.7-5.1-2-6.3-3.9c-1.2-2-1.5-4.4-0.8-7.5c1.1-5,4.6-11.3,10.7-18.9
                                    c5.1-6.5,11.8-13.3,20.1-20.6c6.3-5.5,12.2-10,17.7-13.6c0.7-2.7,1.1-4.3,1.2-4.7c0.2-1,0.6-3.1,1.2-6.2
                                    c-11,10.6-20.6,17.7-28.9,21.5c-8,3.7-14.6,4.1-19.8,1.4c-4.2-2.2-7.1-6.4-8.5-12.6c-1.4-5.8-1.3-12.4,0.1-19.8
                                    c1.1-5.9,2.9-11.7,5.5-17.4c2.6-5.7,5.7-10.8,9.3-15.3c4.2-5.3,9-9.5,14.2-12.7c5.2-3.1,10.7-5,16.5-5.8
                                    c12.3-1.5,20.5,0.4,24.5,5.6c2.4,3.1,3.4,7.4,2.9,12.9c-0.5,5.4-2,11.5-4.8,18.5c-0.1,0.2-0.2,0.4-0.5,0.5s-0.5,0.1-0.7,0
                                    c-0.2-0.1-0.4-0.2-0.5-0.5c-0.1-0.2-0.1-0.5,0-0.7c5.5-14.5,6.2-24.4,2.1-29.8c-3.5-4.6-11.2-6.3-22.9-4.9
                                    c-4.5,0.5-8.9,1.9-13.1,4.1c-4.2,2.2-8,4.9-11.3,8.1c-3.3,3.3-6.3,6.9-9,11c-2.7,4.1-4.9,8.4-6.7,12.9c-1.8,4.5-3.1,9-3.9,13.6
                                    c-1.4,7.4-1.4,13.9,0,19.4s3.9,9.2,7.6,11.1c5,2.7,11.7,1.9,20-2.3c8.3-4.2,17.8-11.5,28.4-22c0.7-4.5,0.9-8,0.7-10.5
                                    c-0.2-2.5-0.6-4.1-1.4-4.7c-0.5-0.4-1.3-0.2-2.6,0.7c-0.2,0.1-0.4,0.1-0.6,0.1c-0.3-0.1-0.5-0.2-0.6-0.3c-0.1-0.2-0.2-0.4-0.2-0.7
                                    c0-0.3,0.2-0.5,0.3-0.6c1.9-1.4,3.5-1.5,4.8-0.5c2.2,1.8,2.6,7.5,1.1,16.9v0.2c-0.4,2.6-0.9,5.6-1.7,8.9c-0.1,0.6-0.4,1.6-0.7,3.1
                                    c8.7-5.3,15-7.5,18.9-6.6c0.2,0,0.4,0.2,0.5,0.4s0.2,0.5,0.1,0.7c0,0.2-0.2,0.4-0.4,0.5c-0.2,0.2-0.5,0.2-0.7,0.1
                                    c-3.8-0.8-10.1,1.7-19,7.3c-3.4,13.2-7.8,25.3-13.2,36.2c-4.2,8.6-8.8,15.5-13.8,20.6C279.9,150.5,275.5,152.8,271.3,152.8z
                                     M309.3,90.6c-5.8,4-11.2,8.1-16,12.4c-8.2,7.2-15,14.3-20.5,21.3s-8.7,12.8-9.7,17.3c-0.6,2.6-0.4,4.6,0.6,6.1s2.7,2.5,5.2,3.1
                                    c4.3,1,8.8-0.8,13.6-5.3c4.8-4.5,9.5-11.5,14.2-20.9C301.6,114.7,305.8,103.4,309.3,90.6z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round"
                                          d="M334.6,91.8c-1.4,0-2.6-0.4-3.6-1.1c-2.5-2.1-2.3-7.5,0.6-16.2c0.1-0.2,0.2-0.4,0.5-0.5c0.2-0.1,0.4-0.1,0.6-0.1
                                    c0.2,0.1,0.4,0.2,0.5,0.5s0.1,0.4,0,0.6c-2.7,7.8-3.1,12.6-1.1,14.3c0.6,0.5,1.4,0.8,2.5,0.8c1.1,0,2.4-0.2,3.8-0.6
                                    s3.3-1.2,5.7-2.3c2.4-1.1,4.1-2,5.2-2.5c1-0.5,3.2-1.7,6.4-3.4l1.7-0.9c0.2-0.1,0.4-0.1,0.7,0s0.4,0.2,0.5,0.4
                                    c0.1,0.2,0.1,0.5,0,0.7c-0.1,0.3-0.2,0.4-0.4,0.5l-1.7,0.8c-2.9,1.6-5.1,2.7-6.7,3.5c-1.6,0.8-3.4,1.7-5.6,2.7s-4,1.7-5.6,2.2
                                    C337.2,91.6,335.8,91.8,334.6,91.8z M333.9,69.2c0,0-0.1,0-0.1,0s-0.1,0-0.1,0c-0.2,0-0.4-0.2-0.5-0.4s-0.2-0.5-0.1-0.7
                                    c0.7-2.8,1.7-4.4,3-4.8c0.7-0.3,1.4-0.2,2.1,0.3c0.2,0.1,0.3,0.3,0.3,0.6c0,0.3,0,0.5-0.1,0.7c-0.1,0.2-0.3,0.3-0.6,0.3
                                    c-0.3,0-0.5,0-0.6-0.1l-0.5-0.1c-0.3,0.1-0.7,0.6-1,1.3s-0.7,1.5-0.9,2.3C334.6,69,334.3,69.2,333.9,69.2z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round" d="M352.5,115.8h-0.3c-0.4-0.1-0.8-0.4-1-0.9c-1.4-3,1.2-17.1,8-42.4c1.5-5.5,3.1-11.4,4.9-17.6c1.8-6.2,3.2-11,4.2-14.5
                                    c1-3.4,1.5-5.2,1.5-5.2c0-0.2,0.2-0.4,0.4-0.5c0.2-0.1,0.5-0.2,0.7-0.1c0.2,0,0.4,0.2,0.5,0.4c0.1,0.2,0.2,0.5,0.1,0.7
                                    c0,0-0.5,1.8-1.6,5.3c-1,3.5-2.5,8.4-4.3,14.7c-1.8,6.3-3.5,12.2-4.9,17.8c-3.1,11.6-5.3,20.6-6.6,27c-1.6,7.8-2.1,12.4-1.4,13.7
                                    c0.4,0.2,0.6,0.5,0.5,1C353.2,115.5,352.9,115.8,352.5,115.8z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round" d="M366.8,91.3c0,0-0.1,0-0.2,0c-0.1,0-0.1,0-0.2,0c-0.4-0.1-0.6-0.5-0.7-1c-0.1-0.5-0.1-1.5,0.1-2.9
                                    c0.1-1.2,0.5-3.3,1.1-6.2l1.8-9c0.1-0.2,0.2-0.4,0.4-0.6c0.2-0.1,0.4-0.2,0.6-0.2c0.2,0,0.4,0.2,0.6,0.4c0.2,0.3,0.2,0.5,0.2,0.7
                                    c-1.6,6.9-2.5,11.8-2.9,14.9c1.4-3.2,2.2-5,2.2-5.2c1.8-4.2,3-6.9,3.7-8.1c0.7-1.2,1.3-1.8,1.9-1.9c0.5,0,0.9,0.2,1.1,0.5
                                    c0.7,1.2,0.8,5.1,0.3,11.8c2-3.1,3.7-5.1,5.4-5.8c0.5-0.3,1.1-0.4,1.6-0.5c0.5-0.1,1.1,0,1.8,0.2c0.7,0.2,1.3,0.3,1.7,0.5
                                    s1.1,0.4,2.1,0.8c1.5,0.6,2.8,1.1,3.7,1.4c0.9,0.3,2.5,0.7,4.7,1.1c2.2,0.4,4.3,0.7,6.4,0.7c2.1,0,4.8-0.2,8.1-0.6
                                    c3.3-0.4,6.8-1.1,10.5-2.1c0.2-0.1,0.4,0,0.7,0.1c0.2,0.1,0.3,0.3,0.4,0.5c0.1,0.6-0.1,1-0.5,1.1c-3.8,1-7.4,1.7-10.8,2.2
                                    c-3.4,0.4-6.2,0.7-8.3,0.7c-2.2,0-4.4-0.2-6.6-0.7c-2.2-0.4-3.9-0.8-4.9-1.2s-2.3-0.8-3.8-1.4c-0.8-0.3-1.4-0.5-1.8-0.7
                                    c-0.4-0.2-0.9-0.3-1.4-0.4c-0.5-0.1-1-0.2-1.4-0.1s-0.8,0.1-1.2,0.3c-1.6,0.7-3.8,3.5-6.6,8.4c-0.2,0.4-0.6,0.5-1.1,0.4
                                    c-0.4-0.1-0.6-0.4-0.6-1c0.6-5.6,0.7-10.1,0.5-13.4c-0.2,0.5-1.4,3.2-3.7,8.1c-0.9,2.2-1.7,4.1-2.4,5.6
                                    C368.2,90.4,367.5,91.3,366.8,91.3z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round" d="M433.4,94.6c-1.2,0-2.3-0.4-3.1-1.1c-0.9-0.8-1.3-2-1.4-3.6s0.4-3.7,1.2-6.4c-1.9,2-3.8,3.7-5.9,5c-2,1.3-3.8,1.6-5.3,0.8
                                    c-0.5-0.3-0.7-0.8-0.6-1.7c0.1-0.9,0.7-2.2,1.6-3.9c0.9-1.7,2.1-3.2,3.5-4.7c1.4-1.5,2.6-2.3,3.8-2.4c1.3-0.2,2.2,0.5,2.8,1.9
                                    c0.1,0.2,0.1,0.4,0,0.7s-0.2,0.4-0.4,0.5c-0.2,0.1-0.5,0.1-0.7,0c-0.2-0.1-0.4-0.2-0.5-0.4c-0.2-0.4-0.4-0.7-0.6-0.8
                                    c-0.2-0.1-0.4-0.1-0.6,0c-1.2,0.3-2.8,1.9-4.7,4.6c-1.5,2.2-2.3,3.8-2.4,4.8c0.9,0.2,2.1-0.2,3.6-1.1c2.7-1.7,5.4-4.5,8.2-8.4
                                    c0.2-0.5,0.5-1.1,0.8-2c0.4-0.8,0.7-1.4,0.8-1.7c0.1-0.3,0.3-0.5,0.7-0.5c0.4,0,0.6,0.1,0.8,0.3c0.7,0.7,0.2,2.3-1.6,4.8
                                    c-2.9,7.3-3.6,11.7-2,13.2c0.3,0.3,0.7,0.5,1.2,0.6c0.5,0.1,1.2,0,2-0.1c0.8-0.2,1.5-0.4,2.2-0.6c0.7-0.2,1.6-0.6,2.8-1.1
                                    c1.2-0.5,2.1-1,2.9-1.4c0.8-0.4,1.9-0.9,3.3-1.7c1.4-0.7,2.5-1.3,3.2-1.7c0.7-0.4,1.8-1,3.3-1.8c0.7-0.4,1.5-0.9,2.4-1.3
                                    s1.9-1,2.9-1.6c1.1-0.6,1.9-1,2.5-1.3c0.2-0.1,0.4-0.1,0.6,0c0.2,0.1,0.4,0.2,0.5,0.4c0.1,0.2,0.1,0.4,0,0.7
                                    c-0.1,0.3-0.2,0.4-0.4,0.5c-3.2,1.7-5.7,3.1-7.7,4.2c-3.6,2-6.3,3.4-8.1,4.4c-1.8,0.9-3.8,1.9-6.2,2.8
                                    C436.7,94.2,434.8,94.6,433.4,94.6z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round"
                                          d="M458.2,93.9H458l-0.2-0.1l-0.2-0.1c0,0-0.1-0.1-0.1-0.1c-0.1,0-0.1-0.1-0.1-0.1c0,0,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2V93
                                    c-0.1-0.8,0.2-2.7,0.8-5.6c0.7-2.9,1.3-5.6,2-8c1-3.7,1.8-6.4,2.5-8.3c0.9-2.6,1.7-4.1,2.4-4.5c0.5-0.3,0.8-0.1,1.1,0.3
                                    c0.3,0.4,0.2,0.8-0.3,1.1c-0.2,0.2-0.4,0.7-0.8,1.5c-0.3,0.8-0.8,2.2-1.4,4.1c-0.6,1.9-1.2,4-1.9,6.5c-1.2,4.5-2,7.5-2.2,8.9
                                    c0.1-0.2,0.3-0.5,0.5-0.9s0.4-0.7,0.5-0.9c2.3-3.7,4-6.5,5.3-8.2c1.3-1.7,2.4-2.6,3.4-2.7c0.6-0.1,1.1,0.1,1.6,0.4
                                    c0.5,0.5,0.9,1.1,1.1,1.8s0.4,1.8,0.6,3c0.1,0.7,0.2,1.3,0.3,1.7c0.1,0.4,0.2,0.9,0.4,1.5c0.2,0.6,0.5,1,0.8,1.4
                                    c0.3,0.4,0.7,0.7,1.1,0.8c2.9,1.4,9.4-0.9,19.4-6.9c0.2-0.1,0.5-0.1,0.7-0.1c0.2,0.1,0.4,0.2,0.6,0.4c0.3,0.5,0.2,0.9-0.4,1.2
                                    c-5.4,3.2-9.8,5.3-13.2,6.5c-3.4,1.1-6,1.2-7.8,0.4c-0.6-0.3-1.2-0.7-1.6-1.2c-0.5-0.5-0.8-1.1-1.1-1.8c-0.3-0.7-0.4-1.3-0.6-1.8
                                    c-0.1-0.5-0.3-1.2-0.4-2c-0.3-2-0.7-3.3-1.2-3.8h-0.1c-0.5,0-1.6,1-3.1,3.1c-0.7,0.9-2.2,3.2-4.4,6.9c-1.9,3-3,4.8-3.3,5.2
                                    c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1s-0.1,0-0.1,0c-0.1,0-0.1,0-0.1,0H458.2z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round"
                                          d="M492.8,94.9c-0.6,0-1.1-0.2-1.6-0.7c-0.4-0.5-0.7-1.1-0.8-2c-0.1-0.8,0-1.9,0.3-3.1c0.3-1.2,0.8-2.8,1.6-4.5
                                    c0.8-1.8,1.9-3.7,3.2-5.9c0.1-0.2,0.3-0.4,0.6-0.4c0.2-0.1,0.5,0,0.7,0.1c0.5,0.3,0.6,0.7,0.3,1.2c-1,1.7-1.9,3.2-2.6,4.7
                                    c-0.7,1.5-1.2,2.7-1.5,3.6c-0.3,1-0.5,1.8-0.6,2.6c-0.1,0.8-0.1,1.3,0,1.7c0.1,0.4,0.2,0.6,0.3,0.8c0,0.2,0.2,0.2,0.6,0
                                    c0.5-0.2,1-0.7,1.6-1.5c0.6-0.8,1.1-1.9,1.6-3.4c0.5-1.5,0.9-3.3,1-5.5c0.1-2.2,0.1-4.7-0.2-7.6c0-0.2,0.1-0.4,0.3-0.6
                                    c0.2-0.2,0.4-0.3,0.6-0.3c0.2,0,0.4,0.1,0.6,0.2c0.2,0.2,0.3,0.3,0.3,0.5c0.3,3.1,0.4,5.8,0.2,8.1c-0.2,2.3-0.6,4.3-1.2,6
                                    c-0.6,1.7-1.3,3-2,3.9c-0.7,0.9-1.4,1.6-2.1,1.9C493.5,94.8,493.2,94.9,492.8,94.9z"/>
                                    <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                          stroke-linejoin="round" d="M508.4,100H508c-1.8-0.3-2.7-2.8-2.8-7.3c-0.1-3.3,0.2-7.3,0.9-12.1c0.2-1.3,0.4-2.5,0.6-3.4c0.2-1,0.4-1.7,0.6-2.2
                                    c0.2-0.5,0.3-0.9,0.5-1.1c0.2-0.3,0.3-0.5,0.4-0.5c0.1-0.1,0.2-0.1,0.4-0.1c0.2-0.1,0.5,0,0.7,0.1c0.2,0.1,0.4,0.3,0.4,0.5
                                    c0.1,0.5,0,0.8-0.5,1c-0.3,0.4-0.6,1.5-1,3.4c-0.4,1.8-0.7,3.9-0.9,6.4c-0.6,6.1-0.4,10.3,0.4,12.6c0.3,0.7,0.5,1.1,0.7,1.1
                                    c0.2,0,0.4-0.1,0.7-0.4c0.3-0.2,1.1-1.4,2.4-3.5s2.7-5,4.3-8.8c2.6-6.1,4.8-11.5,6.5-16.3l0.1-0.3c0.2-0.5,0.6-0.7,1.1-0.5
                                    c0.2,0.1,0.4,0.2,0.5,0.5c0.1,0.2,0.1,0.4,0,0.6l-0.1,0.3c-2.2,5.9-4.4,11.4-6.6,16.6c-1.6,3.6-3,6.4-4.2,8.5
                                    c-1.2,2.1-2.2,3.4-2.8,4C509.6,99.7,509,100,508.4,100z"/>

                            </svg>
                            </div>
                            <div id="inViewport"></div>
                        </div>
                    </div>
                </div>


            </section>

            <section id="price" class="pb-5">
                <div class="row pt-5">
                    <div class="col-12">
                        <h2 class="header-text green-text text-center">
                            стоимость
                        </h2>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-12">
                        <div class="price-block text-center pt-2 pr-2 pb-5 pl-2 position-relative">
                            <div class="price-text light-green-text">полная цена</div>
                            <div class="price-numb bebas-font line-through bebas-top-fixer green-text">
                                4500
                            </div>
                            <div class="price-text red-text">акционная цена</div>
                            <div class="price-numb red-text bebas-font">
                                990 <span class="font-arial rub-symbol">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="510" height="510"
                                         viewBox="0 0 510.1 510.1"><path
                                            d="M34.8 429h81.2v69.6c0 3.4 1.1 6.2 3.3 8.3 2.2 2.2 5 3.3 8.3 3.3h60.5c3.1 0 5.9-1.1 8.2-3.3 2.3-2.2 3.4-4.9 3.4-8.3v-69.6h183c3.4 0 6.2-1.1 8.3-3.3 2.2-2.2 3.3-5 3.3-8.3v-46.4c0-3.4-1.1-6.2-3.3-8.3 -2.2-2.2-5-3.3-8.3-3.3H199.6v-42.8h123.2c48.3 0 87.7-14.7 118.3-44.2 30.6-29.4 45.8-67.5 45.8-114.1 0-46.6-15.3-84.6-45.8-114.1C410.5 14.7 371.1 0 322.8 0H127.5c-3.4 0-6.2 1.1-8.3 3.3 -2.2 2.2-3.3 5-3.3 8.3v227.9H34.8c-3.4 0-6.2 1.1-8.3 3.4 -2.2 2.3-3.3 5-3.3 8.2v54c0 3.4 1.1 6.2 3.3 8.3 2.2 2.2 4.9 3.3 8.3 3.3h81.2v42.8H34.8c-3.4 0-6.2 1.1-8.3 3.3 -2.2 2.2-3.3 5-3.3 8.3v46.4c0 3.4 1.1 6.2 3.3 8.3C28.6 427.9 31.4 429 34.8 429zM199.6 77.2h115.9c25.6 0 46.2 7.5 62 22.5 15.7 15 23.5 34.5 23.5 58.7 0 24.2-7.9 43.7-23.5 58.7 -15.7 15-36.4 22.5-62 22.5H199.6V77.2z"/></svg>
                                </span>
                            </div>
                            <a href="https://lk.timurgilmanov.com/product/wakeup-discount/"
                               class="btn btn-block btn-danger btn-custom p-2 w-75 m-auto price-block-button">
                                зарегистрироваться
                            </a>
                        </div>
                    </div>
                </div>

            </section>

            <section id="comments" class="mt-5">

                <div class="row pt-5">
                    <div class="col-12">
                        <h2 class="header-text text-center">
                            Отзывы
                        </h2>
                    </div>
                </div>
                @include('blocks.comments',['page'=>'wakeup'])
            </section>

        </div>
    </div>
</div>

<section id="fixed-button">
    <div class="col-12 text-center ">
        <a href="#price" data-block="price" class="btn btn-block btn-danger btn-custom m-auto btn-custom-fix-reg"
           id="toPrice">
            зарегистрироваться
        </a>
    </div>
</section>

<footer>
    <div class="pl-5 pr-5 pt-3 pb-3 container">
        <div class="row d-flex flex-row align-items-center text-center-mobile justify-content-center">
            <div class="col-12 col-sm-5 offset-sm-1 pl-lg-5 p-1 footer-text bebas-font">
                Курс Пробуждение Женщины
            </div>
            <div class="col-12 col-sm-4 pr-sm-5 p-1 text-center">
                <a class="footer-link d-inline-block" href="https://www.instagram.com/woman_love_inside/"
                   target="_blank">
                    <svg enable-background="new 0 0 50 50"
                         viewBox="0 0 256 256"
                         version="1.1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink"
                         preserveAspectRatio="xMidYMid" width="70%" height="70%" class="ig-icon mt-1">
                        <g>
                            <path
                                d="M127.999746,23.06353 C162.177385,23.06353 166.225393,23.1936027 179.722476,23.8094161 C192.20235,24.3789926 198.979853,26.4642218 203.490736,28.2166477 C209.464938,30.5386501 213.729395,33.3128586 218.208268,37.7917319 C222.687141,42.2706052 225.46135,46.5350617 227.782844,52.5092638 C229.535778,57.0201472 231.621007,63.7976504 232.190584,76.277016 C232.806397,89.7746075 232.93647,93.8226147 232.93647,128.000254 C232.93647,162.177893 232.806397,166.225901 232.190584,179.722984 C231.621007,192.202858 229.535778,198.980361 227.782844,203.491244 C225.46135,209.465446 222.687141,213.729903 218.208268,218.208776 C213.729395,222.687649 209.464938,225.461858 203.490736,227.783352 C198.979853,229.536286 192.20235,231.621516 179.722476,232.191092 C166.227425,232.806905 162.179418,232.936978 127.999746,232.936978 C93.8200742,232.936978 89.772067,232.806905 76.277016,232.191092 C63.7971424,231.621516 57.0196391,229.536286 52.5092638,227.783352 C46.5345536,225.461858 42.2700971,222.687649 37.7912238,218.208776 C33.3123505,213.729903 30.538142,209.465446 28.2166477,203.491244 C26.4637138,198.980361 24.3784845,192.202858 23.808908,179.723492 C23.1930946,166.225901 23.0630219,162.177893 23.0630219,128.000254 C23.0630219,93.8226147 23.1930946,89.7746075 23.808908,76.2775241 C24.3784845,63.7976504 26.4637138,57.0201472 28.2166477,52.5092638 C30.538142,46.5350617 33.3123505,42.2706052 37.7912238,37.7917319 C42.2700971,33.3128586 46.5345536,30.5386501 52.5092638,28.2166477 C57.0196391,26.4642218 63.7971424,24.3789926 76.2765079,23.8094161 C89.7740994,23.1936027 93.8221066,23.06353 127.999746,23.06353 M127.999746,0 C93.2367791,0 88.8783247,0.147348072 75.2257637,0.770274749 C61.601148,1.39218523 52.2968794,3.55566141 44.1546281,6.72008828 C35.7374966,9.99121548 28.5992446,14.3679613 21.4833489,21.483857 C14.3674532,28.5997527 9.99070739,35.7380046 6.71958019,44.1551362 C3.55515331,52.2973875 1.39167714,61.6016561 0.769766653,75.2262718 C0.146839975,88.8783247 0,93.2372872 0,128.000254 C0,162.763221 0.146839975,167.122183 0.769766653,180.774236 C1.39167714,194.398852 3.55515331,203.703121 6.71958019,211.845372 C9.99070739,220.261995 14.3674532,227.400755 21.4833489,234.516651 C28.5992446,241.632547 35.7374966,246.009293 44.1546281,249.28042 C52.2968794,252.444847 61.601148,254.608323 75.2257637,255.230233 C88.8783247,255.85316 93.2367791,256 127.999746,256 C162.762713,256 167.121675,255.85316 180.773728,255.230233 C194.398344,254.608323 203.702613,252.444847 211.844864,249.28042 C220.261995,246.009293 227.400247,241.632547 234.516143,234.516651 C241.632039,227.400755 246.008785,220.262503 249.279912,211.845372 C252.444339,203.703121 254.607815,194.398852 255.229725,180.774236 C255.852652,167.122183 256,162.763221 256,128.000254 C256,93.2372872 255.852652,88.8783247 255.229725,75.2262718 C254.607815,61.6016561 252.444339,52.2973875 249.279912,44.1551362 C246.008785,35.7380046 241.632039,28.5997527 234.516143,21.483857 C227.400247,14.3679613 220.261995,9.99121548 211.844864,6.72008828 C203.702613,3.55566141 194.398344,1.39218523 180.773728,0.770274749 C167.121675,0.147348072 162.762713,0 127.999746,0 Z M127.999746,62.2703115 C91.698262,62.2703115 62.2698034,91.69877 62.2698034,128.000254 C62.2698034,164.301738 91.698262,193.730197 127.999746,193.730197 C164.30123,193.730197 193.729689,164.301738 193.729689,128.000254 C193.729689,91.69877 164.30123,62.2703115 127.999746,62.2703115 Z M127.999746,170.667175 C104.435741,170.667175 85.3328252,151.564259 85.3328252,128.000254 C85.3328252,104.436249 104.435741,85.3333333 127.999746,85.3333333 C151.563751,85.3333333 170.666667,104.436249 170.666667,128.000254 C170.666667,151.564259 151.563751,170.667175 127.999746,170.667175 Z M211.686338,59.6734287 C211.686338,68.1566129 204.809755,75.0337031 196.326571,75.0337031 C187.843387,75.0337031 180.966297,68.1566129 180.966297,59.6734287 C180.966297,51.1902445 187.843387,44.3136624 196.326571,44.3136624 C204.809755,44.3136624 211.686338,51.1902445 211.686338,59.6734287 Z"
                                fill="#fff"></path>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</footer>

<script src="{{asset('js/all/jquery.min.js') }}"></script>
<script src="{{asset('js/all/swiper.min.js') }}"></script>
<script src="{{asset('js/all/popper.min.js') }}"></script>
<script src="{{asset('js/all/bootstrap.min.js') }}"></script>
<script src="{{asset('js/all/scripts.js') }}"></script>

<script>

    (function () {
        window.signature = {
            initialize: function () {
                console.log("-1");
                return $(".signature svg").each(function () {
                    var delay, i, len, length, path, paths, previousStrokeLength, results, speed;
                    paths = $("path, circle, rect", this);
                    delay = 0;
                    results = [];
                    for (i = 0, len = paths.length; i < len; i++) {
                        //if (window.CP.shouldStopExecution(0)) break;
                        path = paths[i];
                        length = path.getTotalLength();
                        previousStrokeLength = speed || 0;
                        speed = length < 100 ? 20 : Math.floor(length);
                        delay += previousStrokeLength + 100;
                        results.push($(path).css("transition", "none").attr("data-length", length).attr("data-speed", speed).attr("data-delay", delay).attr("stroke-dashoffset", length).attr("stroke-dasharray", length + "," + length));
                    }
                    // window.CP.exitedLoop(0);
                    return results;
                });
            },
            animate: function () {
                console.log("2");
                return $(".signature svg").each(function () {
                    var delay, i, len, length, path, paths, results, speed;
                    paths = $("path, circle, rect", this);
                    results = [];
                    for (i = 0, len = paths.length; i < len; i++) {
                        //if (window.CP.shouldStopExecution(1)) break;
                        path = paths[i];
                        length = $(path).attr("data-length");
                        speed = $(path).attr("data-speed");
                        delay = $(path).attr("data-delay");
                        results.push($(path).css("transition", "stroke-dashoffset " + speed + "ms " + delay + "ms linear").attr("stroke-dashoffset", "0"));
                    }
                    //window.CP.exitedLoop(1);
                    return results;
                });
            }
        };

    }).call(this);

    //ParalLax
    var parallax = document.querySelectorAll("body")
    speed = 0.55;
    window.onscroll = function () {

        [].slice.call(parallax).forEach(function (el, i) {

            var windowYOffset = window.pageYOffset,
                elBackgrounPos = "50% " + (windowYOffset * speed).toFixed(0) + "px";
            $("#bg-light-green").css("background-position", elBackgrounPos)

        });
    };


    //Comments Functions
    $("#vkontakte-button").on("click", function (e) {
        e.preventDefault();
        window.open('{{ route('auth.login',['auth'=>'vkontakte'])  }}', "Checking Auth", "height=380,width=650");
        return !1;
    })

    $("#facebook-button").on("click", function (e) {
        e.preventDefault();
        window.open('{{ route('auth.login',['auth'=>'facebook'])  }}', "Checking Auth", "height=600,width=800");
        return !1;
    })


    function preloader(device) {
        // counter
        //var i = 0;

        // create object
        imageObj = new Image();

        ///images = new Array();
        if ( device == "mobile" )
            image = "{{asset('img/wakeup/v2/slide-2.png')}}"
        if ( device == "desktop" )
            image = "{{asset('img/wakeup/v2/slide-2-d.png')}}"

        imageObj.src = image;

        /*
        data-slide-1-m="{{asset('img/wakeup/v2/slide-1.png')}}"
        data-slide-2-m="{{asset('img/wakeup/v2/slide-2.png')}}"
        data-slide-1-d="{{asset('img/wakeup/v2/slide-1-d.png')}}"
        data-slide-2-d="{{asset('img/wakeup/v2/slide-2-d.png')}}"
         */

        // start preloading
        // for (i = 0; i <= 1; i++) {
        //     imageObj.src = images[i];
        // }
    }

jQuery.fn.scrollTo = function(elem, speed) {
    $(this).animate({
        scrollTop:  $(this).scrollTop() - $(this).offset().top + $(elem).offset().top
    }, speed == undefined ? 1000 : speed);
    return this;
};

    $(function () {


        $('#or-action').on('click',function (){
            // $("html, body").animate({
            //         //scrollTop: $('.slide-text2').offset().top + 60
            //         scrollTop: $('.slider').height()
            //     }, "slow");
            $(".slider").scrollTo(".slide-text2", 2000); //custom animation speed

            return !1
        })

        window.signature.initialize();
        var executed = false;
        $(window).on("scroll", function () {
            if ( !executed ) {
                if ( $("#inViewport").isOnScreen() ) {
                    console.log("1")
                    executed = true;
                    window.signature.animate();
                }
            }
        });


        //Swiper
        var swiper = new Swiper(".swiper-container", {
            // Optional parameters
            //direction: 'vertical',
            loop: true,
            // If we need pagination
            pagination: {
                el: ".swiper-pagination",
            },
            // Navigation arrows
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });


        //signarure
        //window.signature.initialize();


        $(window).on("scroll", function () {
            //toggle BUY button
            if ( $("#price").isOnScreen() ) {
                $("#fixed-button").fadeOut()
            } else {
                $("#fixed-button").fadeIn()
            }
            //
            // if ( $("#course-info").isOnScreen() ) {
            //     console.info("fire course-info")
            //     $("body").css("overflow-y", "hidden")
            // }
            // if (
            //     $("#bottom-swiper-flag").isOnScreen() ||
            //     $(".slide-text2").isOnScreen()
            // ) {
            //     console.info("fire bottom-swiper-flag")
            //     $("body").css("overflow-y", "visible")
            // }

        });


        //fixing slider
        const img = $(".bg-img");
        if ( $(window).width().toFixed(0) > 768 ) {
            img.attr("src", img.attr("data-slide-1-d"))
            //preload(img.attr("data-slide-2-d"));
            preloader("desktop")
        } else {
            img.attr("src", img.attr("data-slide-1-m"))
            //preload(img.attr("data-slide-2-m"));
            preloader("mobile")
        }
        img.on("load", function () {
            $(".slider").css("height", img.height())
            $(".slider-inner").css("height", img.height() * 2)
            $(".slide-text1").css("top", img.height() - $(".slide-text1").height() - 50)
            $(".slide-text2").css("top", img.height() * 2 - $(".slide-text2").height() - 250)
        });

        var some = 0;
        var newScrollVal = 0;
        $(".slider").on("DOMMouseScroll scroll touchmove mousewheel", function (event) {
            //newScrollVal = ($(this).scrollTop() / 5.5).toFixed(0)
            newScrollVal = ($(this).scrollTop() / (img.height() - $(".slide-text1").height() - 70) * 100).toFixed(0)

            if ( newScrollVal >= 100 ) {
                if (
                    (img.attr("src") == '{{asset('img/wakeup/v2/slide-1.png')}}') ||
                    (img.attr("src") == '{{asset('img/wakeup/v2/slide-1-d.png')}}')
                ) {
                    img.fadeOut("fast", function () {
                        //$(this).attr("src", '{{asset('img/wakeup/v2/slide-2.png')}}')
                        if ( $(window).width().toFixed(0) > 768 ) {
                            // console.log("new src " + $(this).attr("data-slide-2-d"))
                            $(this).attr("src", $(this).attr("data-slide-2-d"))
                        } else {
                            // console.log("new src " + $(this).attr("data-slide-2-m"))
                            $(this).attr("src", $(this).attr("data-slide-2-m"))
                        }

                        $(this).fadeIn("fast")
                    })
                }
                //$('.bg-img').attr('src','{{asset('img/wakeup/v2/slide-2.png')}}')
            } else {
                if (
                    (img.attr("src") == '{{asset('img/wakeup/v2/slide-2.png')}}') ||
                    (img.attr("src") == '{{asset('img/wakeup/v2/slide-2-d.png')}}')
                ) {
                    img.fadeOut("fast", function () {
                        //$(this).attr("src", '{{asset('img/wakeup/v2/slide-1.png')}}')

                        if ( $(window).width().toFixed(0) > 768 ) {
                            console.log("new src " + $(this).attr("data-slide-1-d"))
                            $(this).attr("src", $(this).attr("data-slide-1-d"))
                        } else {
                            console.log("new src " + $(this).attr("data-slide-1-m"))
                            $(this).attr("src", $(this).attr("data-slide-1-m"))
                        }

                        $(this).fadeIn("fast")
                    })
                }
                //$('.bg-img').attr('src','{{asset('img/wakeup/v2/slide-1.png')}}')
            }
            img.css("filter", "grayscale(" + newScrollVal + "%)")
        });

        var amplTop = 20
        var amplLeft = 5
        setInterval(function () {
            //mySwiper.update();
            //console.log("Test");
            amplLeft = 50 + amplLeft
            //elBackgrounPos = amplLeft + "% " + (amplTop) + "px";
            var backgroundPos = $("#bg-light-green").css("background-position").split(" ");
            // console.log(
            //     "old: " + backgroundPos[1].slice(0, -2) +
            //     "new " + parseInt(parseInt(backgroundPos[1].replace("px", "")) + amplTop)
            // )
            elBackgrounPos = amplLeft + "% " + parseInt(parseInt(backgroundPos[1].replace("px", "")) + amplTop) + "px";
            $("#bg-light-green").css("background-position", elBackgrounPos)
            amplTop = amplTop * -1;
            amplLeft = 50 - amplLeft
        }, 3000);


    })

</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(55714759, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/55714759" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
