<?php // /*	if (isset($_GET['ref'])) {
//			$refid = htmlspecialchars($_GET['ref']);
//			$paylink = "https://lk.timurgilmanov.com/product/fog-apr2020dis/?ref=" . $refid;
//		} else */
//$paylink = "https://lk.timurgilmanov.com/checkout/?add-to-cart=7936&quantity=1";
//?>
@php
    $paylink = "https://lk.timurgilmanov.com/checkout/?add-to-cart=7936&quantity=1"
@endphp
    <!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Пробуждение женщины! Wakeup!</title>
    <link rel="canonical" href="https://womanloveinside.com/"/>
    {{--    <meta property="og:locale" content="ru_RU" />--}}
    {{--    <meta property="og:type" content="website" />--}}
    {{--    <meta property="og:title" content="Пробуждение женщины! Wakeup!" />--}}
    {{--    <meta property="og:description" content="Авторский тренинг Тимура Гильманова для женщин. Меняем жизнь через внутренние изменения." />--}}
    {{--    <meta property="og:image" content="https://iwanttobeawoman.ru./img/index/idk-fb.jpg" />--}}
    {{--    <meta property="og:url" content="https://womanloveinside.com/" />--}}
    {{--    <meta property="og:site_name" content="Хочу быть женщиной" />--}}
    {{--    <meta name="twitter:card" content="summary_large_image" />--}}
    {{--    <meta name="twitter:title" content="Я хочу быть женщиной, забыла как" />--}}
    {{--    <meta name="twitter:image" content="https://womanloveinside.com./img/index/idk-fb.jpg" />--}}
    <link rel="stylesheet" href="./css/index/style.css">
    <link rel="stylesheet" href="./css/index/media.css">
    <link rel="stylesheet" href="./css/index/slick.css"/>
    <link rel="stylesheet" href="./css/index/slick-theme.css"/>
    <link rel="stylesheet" href="./css/index/owl.carousel.css">
    <link rel="stylesheet" href="./css/index/owl.theme.default.css">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

    <link rel="canonical" href="{{route('index')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="196x196" href="{{asset('img/icons/favicon-196.png')}}"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/icons/apple-icon-180.png')}}"/>
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset('img/icons/apple-icon-167.png')}}"/>
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/icons/apple-icon-152.png')}}"/>
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/icons/apple-icon-120.png')}}"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <script src="{{asset('js/all/sweetalert.min.js') }}"></script>

    <style>
        @media (max-width: 768px){
            .reviews {
                width: 100%;
                margin: 0 auto;
            }
        }
        .providericon svg{
            filter: grayscale(.7);
            opacity: .65;
            transition: all .5s;
            cursor: pointer;
        }
        .providericon svg:hover,
        .providericon svg.providericon-active{
            opacity: 1;
            filter: grayscale(0);
        }
    </style>

</head>

<body onunload="deleteAllCookies()">
<div class="container">
    <header>
        <div class="header-wrapper">
            <a href="https://lk.timurgilmanov.com/">
                <img src="./img/index/access.png" class="access" title="Вход в личный кабинет"/>
            </a>
            <div class="header-item">
                <h1>
                    «Пробуждение женщины!»
                </h1>

                <img class="not-container" src="./img/index/img1.png" alt="">
                <img class="not-container" src="./img/index/img2.png" alt="">
            </div>
            <div class="header-item">
                <div class="margin-left">
                    <div class="header-data">15 августа</div>
                    <p class="header-p">Наладить связь со своей душой и научиться слышать себя</p>
                    <div class="center">
                        <a class="button" href="<?=$paylink?>">Иду на тренинг</a>
                    </div>
                    <div class="header-shapes-wrapper">
                        <div class="header-shapes">
                            <div class="header-shape">
                                <img src="./img/index/shape1.png" alt="">
                                <p>10<br>дней</p>
                            </div>
                            <div class="header-shape">
                                <img src="./img/index/shape2.png" alt="">
                                <p>4 онлайн-<br>вебинара</p>
                            </div>
                            <div class="header-shape">
                                <img src="./img/index/shape3.png" alt="">
                                <p>2<br>видеоурока<br><small></small></p>
                            </div>
                            <div class="header-shape">
                                <img src="./img/index/shape4.png" alt="">
                                <!-- <p>20-30 человек<br>в  -->
                                    <p>чат <br> с куратором</p>
                            </div>
                            <!--<div class="header-shape">
                                <img src="./img/index/shape5.png" alt="">
                                <p>Чек-листы<br>с техниками</p>
                            </div>-->
                            <!-- <div class="header-shape">
                                <img src="./img/index/shape6.png" alt="">
                                <p>Материалы<br>в личном<br>кабинете</p>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>
    <section>
        <div class="text-wrapper">
            <div class="text-img">
                <img src="./img/index/img3.png" alt="">
            </div>
            <div class="text">
                <p>«Хочу быть женщиной, но забыла как» - Авторский тренинг Тимура Гильманова для женщин.
                    Меняем жизнь через внутренние изменения.
                    Через техники ты:
                    <br>- учишься контролю над эмоциями,
                    <br>- трансформируешь своё недовольство и раздражительность,
                    <br>- отпускаешь сожаления и обиды,
                    <br>- практикуешь принятие,
                    <br>- наполняешься любовью и начинаешь видеть смысл.</p>
            </div>
        </div>
        <div class="button-wrapper">
            <a class="button" href="<?=$paylink?>">Принять участие</a>
        </div>

    </section>
    <section>
        <div class="for-you">
            <h2>Этот тренинг для тебя, если:</h2>
            <!--<div class="slider">
                <div class="item">
                    <img src="./img/index/slide1.png" alt="Первый слайд">
                    <div class="slideText">Ты устала ходить по тренингам и гуру
                        в поисках смыслов и перемен в жизни и в себе</div>
                </div>

                <div class="item">
                    <img src="./img/index/slide2.png" alt="Второй слайд">
                    <div class="slideText">Ты недовольна собой и своей судьбой</div>
                </div>

                <div class="item">
                    <img src="./img/index/slide3.png" alt="Третий слайд">
                    <div class="slideText">Тебя переполняют обиды</div>
                </div>

                <a class="prev" onclick="minusSlide()">&#10094;</a>
                <a class="next" onclick="plusSlide()">&#10095;</a>
            </div>

            <div class="slider-dots">
                <span class="slider-dots_item" onclick="currentSlide(1)"></span>
                <span class="slider-dots_item" onclick="currentSlide(2)"></span>
                <span class="slider-dots_item" onclick="currentSlide(3)"></span>
            </div>-->
        </div>
        <div class="slidersss">
            <div class="slide1">
                <img src="./img/index/slide1.png" alt="Первый слайд">
                <div class="slideText">Ты устала ходить по тренингам и гуру
                    в поисках смыслов и перемен в жизни и в себе
                </div>
            </div>

            <div class="slide1">
                <img src="./img/index/slide2.png" alt="Второй слайд">
                <div class="slideText">Ты недовольна собой и своей судьбой</div>
            </div>

            <div class="slide1">
                <img src="./img/index/slide3.png" alt="Третий слайд">
                <div class="slideText">Тебя переполняют обиды</div>
            </div>
            <div class="slide1">
                <img src="./img/index/slide4.png" alt="Первый слайд">
                <div class="slideText">Ты чувствуешь себя нереализованной как женщина и как личность</div>
            </div>

            <div class="slide1">
                <img src="./img/index/slide5.png" alt="Второй слайд">
                <div class="slideText">Ты бесконечно конфликтуешь с мужем и детьми</div>
            </div>

            <div class="slide1">
                <img src="./img/index/slide6.png" alt="Третий слайд">
                <div class="slideText">Ты переживаешь о будущем и погрязла в сожалениях о прошлом</div>
            </div>
        </div>
    </section>
    <section>
        <div class="secret">
            <div class="secret-item">
                <div class="secret-item-padding">
                    <h3></h3>
                    <p></p>
                </div>
            </div>
            <div class="secret-item">

            </div>
        </div>
    </section>
    <section>
        <div class="for-you">
            <h2>Что даст тебе тренинг:</h2>
            <div class="sliderss">
                <div class="slide1">
                    <img src="./img/index/2slide1.png" alt="Первый слайд">
                    <div class="slideTitle">Отношения с мужчиной</div>
                    <div class="slidesubtitle">Ты перестанешь ругаться со своим мужчиной, бороться с ним, уберешь
                        сопротивление. Тебе понравится, когда мужчина начнёт относиться к тебе, как к женщине. Ты
                        ощутишь, какое это счастье, когда тебя носят на руках
                    </div>
                    <div class="slideText"></div>
                </div>

                <div class="slide1">
                    <img src="./img/index/2slide2.png" alt="Второй слайд">
                    <div class="slideTitle">Гармония в семье</div>
                    <div class="slidesubtitle">Ты поймешь, что все проблемы в семье - из-за отсутствия баланса ума и
                        сердца. Твой мужчина станет меньше пить, курить, играть в игры. Ты увидишь, что можешь на
                        это влиять.
                    </div>
                    <div class="slideText"></div>
                </div>

                <div class="slide1">
                    <img src="./img/index/2slide3.png" alt="Третий слайд">
                    <div class="slideTitle">Принятие</div>
                    <div class="slidesubtitle">Ты узнаешь все об уровнях принятия и поймешь, как с этим
                        взаимодействовать. Как женщине научиться принимать своего мужчину, детей, свои эмоции и все
                        происходящее, при этом не тратя на принятие огромное количество энергии.
                    </div>
                    <div class="slideText"></div>
                </div>
                <div class="slide1">
                    <img src="./img/index/2slide4.png" alt="Первый слайд">
                    <div class="slideTitle">Самоконтроль</div>
                    <div class="slidesubtitle">Ты начнёшь контролировать свой гнев, меньше срываться на мужа и
                        детей. Научишься управлять своими эмоциями.
                    </div>
                    <div class="slideText"></div>
                </div>

                <div class="slide1">
                    <img src="./img/index/2slide5.png" alt="Второй слайд">
                    <div class="slideTitle">Самосознание</div>
                    <div class="slidesubtitle">Ты начнёшь контролировать свой гнев, меньше срываться на мужа и
                        детей. Научишься управлять своими эмоциями.
                    </div>
                    <div class="slideText"></div>
                </div>

                <div class="slide1">
                    <img src="./img/index/2slide6.png" alt="Третий слайд">
                    <div class="slideTitle">Достижение целей</div>
                    <div class="slidesubtitle">Всё это приведет к тому, что ты перестанешь бояться счастья, любви,
                        гармонии и богатства. Ты научишься это материализовывать с помощью своей природной женской
                        силы.
                    </div>
                    <div class="slideText"></div>
                </div>
            </div>
        </div>
        <div class="button-wrapper">
            <a class="button" href="<?=$paylink?>">Принять участие</a>
        </div>
    </section>
    <section>
        <div class="autor">
            <div class="autor-item">
                <div class="autor-item-padding">
                    <h3>Ведущий курса</h3>
                    <p class="timur-tochka">Практикующий психолог, многодетный отец</p>
                    <p class="timur-tochka">Автор 8-ми книг</p>
                    <p class="timur-tochka">Создатель курса «Я хочу быть женщиной, но забыла как»</p>
                    <p class="timur-tochka">Блоггер - более 200 полезных и ёмких видео на тему личного развития</p>
                    <p class="timur-tochka-last">Основатель «Антишколы психологии»</p>
                </div>
            </div>
            <div class="autor-item-timur">
                <div class="timur">Тимур Гильманов</div>
            </div>
        </div>
    </section>
    <section>
        <div class="program">
            <div class="program-item">
                <h4>
                    Программа курса
                </h4>
            </div>
            <div class="program-item">
                <div class="nav">
                    <!--<img src="./img/index/img6.png" alt="">-->
                </div>
            </div>
        </div>
        <div class="program-slides owl-carousel owl-theme slide-one">
            <div class="program-slide">
                <div class="when">
                    15-17 августа
                </div>
                <p>Самоценность</p>
                <a class="program-a open_modal" href="#modal1">Подробнее</a>
            </div>
            <div class="program-slide">
                <div class="when">
                    18-20 августа
                </div>
                <p>Самобытность</p>
                <a class="program-a open_modal" href="#modal2">Подробнее</a>
            </div>
            <div class="program-slide">
                <div class="when">
                    21-24 августа
                </div>
                <p>Самосознание</p>
                <a class="program-a open_modal" href="#modal3">Подробнее</a>
            </div>
        </div>
        <div class="button-wrapper">
            <a class="button" href="<?=$paylink?>">Принять участие</a>
        </div>
    </section>
    <section id="pricing">
        <h5>Лучшая инвестиция в свою женственность</h5>

        <div class="price">
            <div class="price-item" id="price1">
                <!--<div class="paket">
                          <div class="what-paket">
                              10 видеоуроков<br>4 онлайн-вебинара<br>Чек-листы<br>Чат с куратором
                          </div>
                          <div class="price-price">
                              Стоимость участия
                          </div>
                          <div class="price-value">
                              1990 ₽
                          </div>
                          <a class="button" href="https://iwanttobeawoman.ru/checkout/?add-to-cart=4973&quantity=1" onclick="ym(55714759, 'reachGoal', 'packet2'); return true;">Купить пакет</a>
                  </div>-->
            </div>
            <div class="price-item"></div>
            <div class="price-item">
                <div class="price-item" id="price2">
                    <div class="paket">

                    </div>
                    <div class="what-paket">
                        2 видеоурока<br>4 онлайн-вебинара<br>Чек-листы<br>Чат с куратором
                    </div>
                    <div class="price-price">
                        Стоимость участия
                    </div>
                    <div class="price-value">
                        1500 ₽
                    </div>
                    <a class="button" href="<?=$paylink?>" onclick="ym(55714759, 'reachGoal', 'packet2'); return true;">Иду
                        на тренинг</a>
                </div>
            </div>
        </div>
    </section>


    <section style="display: none">
        @include('blocks.comments',['page'=>'wakeup'])
{{--        <h5>Отзывы о курсе</h5>--}}

{{--        <div class="row">--}}
{{--            <div class="col-12">--}}

{{--                <div class="d-flex flex-row justify-content-center align-content-center">--}}
{{--                    <div--}}
{{--                        class="d-flex flex-column justify-content-center align-content-center mr-3 comments-number bebas-font text-center">--}}
{{--                        {{ $landerComments->count()  }}--}}
{{--                    </div>--}}
{{--                    <img height="36" class="w-auto" src="{{asset('img/cms/comments-icon-black@2x.png')}}" alt="">--}}
{{--                </div>--}}
{{--                <div class="text-center total-rating  bebas-font">--}}

{{--                    @if ($landerComments->count()>0)--}}
{{--                        {{ round($landerCommentsSum/$landerComments->count(),1)  }}--}}
{{--                        @for( $i=1; $i<=5; $i++ )--}}
{{--                            @if(round($landerCommentsSum/$landerComments->count())>=$i)--}}
{{--                                <img src="{{asset('img/cms/star-full@2x.png')}}"--}}
{{--                                     title="{{round($landerCommentsSum/$landerComments->count(),1) }}"--}}
{{--                                     class="comment-star comment-star-full" alt="">--}}
{{--                            @else--}}
{{--                                <img src="{{asset('img/cms/star-empty@2x.png')}}"--}}
{{--                                     title="round($landerCommentsSum/$landerComments->count(),1) "--}}
{{--                                     class="comment-star comment-star-empty" alt="">--}}
{{--                            @endif--}}
{{--                        @endfor--}}
{{--                    @endif--}}

{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}

{{--        <div class="reviews">--}}


{{--            <div class="row">--}}
{{--                <div class="col-12">--}}

{{--                    <div class="text-center p-3 leave-comment-txt-block">--}}

{{--                        <div class="leave-comment-txt mb-3">--}}
{{--                            оставьте отзыв, <br> нажав на иконку:--}}
{{--                        </div>--}}

{{--                        <div id="uLoginc513b193"--}}
{{--                             data-ulogin="display=panel;fields=first_name,last_name,photo,nickname,photo_big;theme=flat;providers=vkontakte,instagram,facebook;callback=getForm"></div>--}}


{{--                        <div class="col-12 d-flex justify-content-center align-content-center" >--}}

{{--                            <div id="instagram-button" class="m-2 providericon instagram" style="width: 50px; height: auto">--}}
{{--                                --}}{{----}}{{--                                <?xml version="1.0" ?><!DOCTYPE svg  PUBLIC '-//W3C//DTD SVG 1.1//EN'  'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>--}}
{{--                                <svg enable-background="new 0 0 50 50" id="Layer_1" version="1.1" viewBox="0 0 50 50" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M34.1,12.6c0,0-18.5,0-18.6,0c-1.6,0-2.9,1.3-2.9,2.9l0,0c0,0,0,19,0,19c0,1.6,1.3,2.9,2.9,2.9  c0,0,19,0,19,0c1.5-0.1,2.9-1.3,2.9-2.9c0,0,0-19,0-19C37.3,14,35.6,12.7,34.1,12.6z M25,20.5c2.5,0,4.5,2,4.5,4.5  c0,2.5-2,4.5-4.5,4.5c-2.5,0-4.5-2-4.5-4.5C20.5,22.5,22.5,20.5,25,20.5z M34.1,32.4c0,0.9-0.7,1.6-1.6,1.6H25h-7.4  c-0.9,0-1.6-0.7-1.6-1.6v-9.5h1.9c-0.2,0.7-0.3,1.3-0.3,2.1c0,4.1,3.3,7.4,7.4,7.4s7.4-3.3,7.4-7.4c0-0.7-0.1-1.4-0.3-2.1h1.9V32.4z   M34.1,16.8v2.5h0c0,0.4-0.4,0.8-0.8,0.8v0h-2.5c-0.4,0-0.8-0.4-0.8-0.8h0v-2.5h0c0,0,0,0,0,0c0-0.5,0.4-0.8,0.8-0.8h2.5v0  C33.7,15.9,34.1,16.3,34.1,16.8C34.1,16.8,34.1,16.8,34.1,16.8L34.1,16.8z" fill="#527FA3" id="camera"/><path d="M25,1C11.7,1,1,11.7,1,25s10.7,24,24,24s24-10.7,24-24S38.3,1,25,1z M25,44C14.5,44,6,35.5,6,25S14.5,6,25,6  s19,8.5,19,19S35.5,44,25,44z" fill="#527FA3"/></svg>--}}
{{--                            </div>--}}
{{--                            <div id="facebook-button" class="m-2 providericon facebook" style="width: 50px; height: auto">--}}
{{--                                --}}{{--                                <?xml version="1.0" ?><!DOCTYPE svg  PUBLIC '-//W3C//DTD SVG 1.1//EN'  'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>--}}
{{--                                <svg enable-background="new 0 0 50 50" id="Layer_2" version="1.1" viewBox="0 0 50 50" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M30,26l1-5l-5,0v-4c0-1.5,0.8-2,3-2h2v-5c0,0-2,0-4,0c-4.1,0-7,2.4-7,7v4h-5v5h5v14h6V26H30z" fill="#3A5BA0" id="f_1_"/><path d="M25,1C11.7,1,1,11.7,1,25s10.7,24,24,24s24-10.7,24-24S38.3,1,25,1z M25,44C14.5,44,6,35.5,6,25S14.5,6,25,6  s19,8.5,19,19S35.5,44,25,44z" fill="#3A5BA0"/></svg>--}}
{{--                            </div>--}}
{{--                            <div id="vkontakte-button" class="m-2 providericon vkontakte" style="width: 50px; height: auto">--}}
{{--                                --}}{{--                                <?xml version="1.0" ?><!DOCTYPE svg  PUBLIC '-//W3C//DTD SVG 1.1//EN'  'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>--}}
{{--                                <svg enable-background="new 0 0 50 50" id="Layer_3" version="1.1" viewBox="0 0 50 50" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M26,34c1,0,1-1.4,1-2c0-1,1-2,2-2s2.7,1.7,4,3c1,1,1,1,2,1s3,0,3,0s2-0.1,2-2c0-0.6-0.7-1.7-3-4  c-2-2-3-1,0-5c1.8-2.5,3.2-4.7,3-5.3c-0.2-0.6-5.3-1.6-6-0.7c-2,3-2.4,3.7-3,5c-1,2-1.1,3-2,3c-0.9,0-1-1.9-1-3c0-3.3,0.5-5.6-1-6  c0,0-2,0-3,0c-1.6,0-3,1-3,1s-1.2,1-1,1c0.3,0,2-0.4,2,1c0,1,0,2,0,2s0,4-1,4c-1,0-3-4-5-7c-0.8-1.2-1-1-2-1c-1.1,0-2,0-3,0  c-1,0-1.1,0.6-1,1c2,5,3.4,8.1,7.2,12.1c3.5,3.6,5.8,3.8,7.8,3.9C25.5,34,25,34,26,34z" fill="#54769B" id="VK_1_"/><path d="M25,1C11.7,1,1,11.7,1,25s10.7,24,24,24s24-10.7,24-24S38.3,1,25,1z M25,44C14.5,44,6,35.5,6,25S14.5,6,25,6  s19,8.5,19,19S35.5,44,25,44z" fill="#54769B"/></svg>--}}
{{--                            </div>--}}

{{--                        </div>--}}

{{--                        <div style="display: none" id="commentForm">--}}

{{--                            <form action="{{route('lander.add.comment')}}" method="post">--}}
{{--                                @csrf--}}
{{--                                <input type="hidden" name="name" id="name" value="">--}}
{{--                                <input type="hidden" name="network" id="network" value="">--}}
{{--                                <input type="hidden" name="profile" id="profile" value="">--}}
{{--                                <input type="hidden" name="photo" id="photo" value="">--}}
{{--                                <input type="hidden" name="link" id="link" value="">--}}
{{--                                <input type="hidden" name="page" id="page" value="wakeup">--}}
{{--                                <input type="hidden" name="page" id="page" value="wakeup">--}}
{{--                                <input type="hidden" name="rating" id="rating" value="5">--}}
{{--                                <input type="hidden" name="token" id="token" value="">--}}


{{--                                <div class="col-12 col-md-8  offset-md-2  p-3">--}}
{{--                                    <div class="row">--}}

{{--                                        <div class="col-md-2 col-12 text-center">--}}
{{--                                            <img class="comment-img img-fluid img-circle" id="new-comment-photo"--}}
{{--                                                 src="" alt="">--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-9 col-12">--}}
{{--                                            <div class="comment-name text-center-mobile">--}}
{{--                                                <h5 class="text-center" id="new-comment-name"></h5>--}}
{{--                                            </div>--}}
{{--                                            <textarea class="form-control comment-textarea m-3" name="comment" id="comment"--}}
{{--                                                      rows="5"></textarea>--}}
{{--                                            <div class="rating-block text-center-mobile">--}}
{{--                                                @for( $i=1; $i<=5; $i++ )--}}
{{--                                                    <img src="{{asset('img/cms/star-full@2x.png')}}"--}}
{{--                                                         title="{{$i}}" data-rating="{{$i}}"--}}
{{--                                                         class="comment-star comment-star-full comment-star-select"--}}
{{--                                                         alt="{{$i}}">--}}
{{--                                                @endfor--}}
{{--                                            </div>--}}
{{--                                            <div class="mobile-text-center mt-3">--}}
{{--                                                <button type="submit"--}}
{{--                                                        class="btn btn-mint pt-2 pl-2 pr-5 pl-5 button-shadow fz24 hvr-fade hvr-bob">--}}
{{--                                                    опубликовать--}}
{{--                                                </button>--}}
{{--                                            </div>--}}


{{--                                        </div>--}}

{{--                                    </div>--}}

{{--                                </div>--}}


{{--                                --}}{{--                            <div class="row">--}}
{{--                                --}}{{--                                <div class="col-8 offset-2 col-md-1">--}}
{{--                                --}}{{--                                    <img class="comment-img img-fluid img-circle"--}}
{{--                                --}}{{--                                         id="photo-Img" src="" width="50" height="50" alt="">--}}
{{--                                --}}{{--                                </div>--}}
{{--                                --}}{{--                                <div class="col-12 col-md-8">--}}
{{--                                --}}{{--                                    <div id="commentName"  class="comment-name"></div>--}}
{{--                                --}}{{--                                    <div class="form-group">--}}
{{--                                --}}{{--                                        <label for="score">Рейтинг</label>--}}
{{--                                --}}{{--                                        <select name="rating" id="rating">--}}
{{--                                --}}{{--                                            <option value="1">1</option>--}}
{{--                                --}}{{--                                            <option value="2">2</option>--}}
{{--                                --}}{{--                                            <option value="3">3</option>--}}
{{--                                --}}{{--                                            <option value="4">4</option>--}}
{{--                                --}}{{--                                            <option selected value="5">5</option>--}}
{{--                                --}}{{--                                        </select>--}}
{{--                                --}}{{--                                    </div>--}}

{{--                                --}}{{--                                    <div class="form-group">--}}
{{--                                --}}{{--                                        <label for="text">Текст</label>--}}
{{--                                --}}{{--                                        <textarea class="form-control"  name="comment" id="comment" rows="5"></textarea>--}}
{{--                                --}}{{--                                    </div>--}}
{{--                                --}}{{--                                    <button type="submit" class="btn btn-primary">Submit</button>--}}

{{--                                --}}{{--                                </div>--}}
{{--                                --}}{{--                            </div>--}}


{{--                            </form>--}}

{{--                        </div>--}}

{{--                        @if (Session::has('success'))--}}
{{--                            <div>--}}
{{--                                <script>--}}
{{--                                    swal("Спасибо", '{!! Session::get('success') !!}', "success");--}}
{{--                                </script>--}}
{{--                            </div>--}}
{{--                        @endif--}}

{{--                        @if($errors->any())--}}
{{--                            --}}{{--                   PLS  DON`T KILL ME --}}
{{--                            --}}{{--                    <div class="col-12">--}}
{{--                            --}}{{--                        <div class="box-body">--}}
{{--                            --}}{{--                            <div class="alert alert-danger alert-dismissible">--}}
{{--                            --}}{{--                               --}}
{{--                            --}}{{--                            </div>--}}
{{--                            --}}{{--                        </div>--}}
{{--                            --}}{{--                    </div>--}}
{{--                            <script>--}}
{{--                                swal("Ошибка", '{!! implode('', $errors->all('<div>:message</div>')) !!}', "error");--}}
{{--                            </script>--}}
{{--                        @endif--}}


{{--                    </div>--}}


{{--                    <div id="all-comments">--}}

{{--                        <div class="comment-block">--}}


{{--                            @foreach($landerComments as $landerComment)--}}

{{--                                <div class="col-12 col-md-10  offset-md-1 mb-4  mt-4  p-2">--}}
{{--                                    <div class="row">--}}

{{--                                        <div class="col-md-2 col-12 text-center">--}}
{{--                                            <a href="{{$landerComment->link}}" target="_blank">--}}
{{--                                                <img class="comment-img img-fluid img-circle"--}}
{{--                                                     src={{ asset('img/comments/'.$landerComment->avatar)  }}--}}
{{--                                                         alt="{{$landerComment->name}}">--}}

{{--                                                --}}{{--<img class="comment-img img-fluid img-circle" --}}
{{--                                                --}}{{--     src={{ asset('img/comments/no-ava.jpg')  }} --}}
{{--                                                --}}{{--         alt="{{$landerComment->name}}"> --}}


{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-9 col-12">--}}
{{--                                            <div class="comment-name text-center-mobile">--}}
{{--                                                <h5 class="text-center-mobile">--}}
{{--                                                    <a href="{{$landerComment->link}}" target="_blank">--}}
{{--                                                        {{$landerComment->name}}--}}
{{--                                                    </a>--}}
{{--                                                </h5>--}}
{{--                                                <div class="m-2">--}}
{{--                                                    @for( $i=1; $i<=5; $i++ )--}}
{{--                                                        @if($landerComment->rating>=$i)--}}
{{--                                                            <img src="{{asset('img/cms/star-full@2x.png')}}"--}}
{{--                                                                 class="comment-star comment-star-full" alt="">--}}
{{--                                                        @else--}}
{{--                                                            <img src="{{asset('img/cms/star-empty@2x.png')}}"--}}
{{--                                                                 class="comment-star comment-star-empty" alt="">--}}
{{--                                                        @endif--}}

{{--                                                    @endfor--}}

{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="comment-text">--}}
{{--                                                {{$landerComment->comment}}--}}
{{--                                            </div>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}

{{--                                </div>--}}
{{--                                --}}{{--                    </div>--}}


{{--                            @endforeach--}}


{{--                        </div>--}}

{{--                    </div>--}}


{{--                </div>--}}
{{--            </div>--}}



{{--            <div class="review-wrapper">--}}
{{--                <div class="review-wrapper-inner">--}}

{{--                    <!-- <div id="mc-review"></div>--}}
{{--                    <script type="text/javascript">--}}
{{--                    cackle_widget = window.cackle_widget || [];--}}
{{--                    cackle_widget.push({widget: 'Review', id: 72137});--}}
{{--                    (function() {--}}
{{--                        var mc = document.createElement('script');--}}
{{--                        mc.type = 'text/javascript';--}}
{{--                        mc.async = true;--}}
{{--                        mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';--}}
{{--                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);--}}
{{--                    })();--}}
{{--                    </script>--}}
{{--                    <a id="mc-link" href="http://cackle.me">Отзывы для сайта <b style="color:#4FA3DA">Cackl</b><b style="color:#F65077">e</b></a> -->--}}

{{--                </div> <!-- review-wrapper-inner -->--}}
{{--            </div> <!-- review-wrapper -->--}}

{{--        </div>--}}
    </section>

</div>


<div style="color:#fff; background: #1fa7b5; padding:10px 0;">
    <p style="text-align: center;">Остались вопросы? Пишите на
        <a href="mailto:info@iwanttobeawoman.ru">info@iwanttobeawoman.ru</a>
    </p>
</div>

<div id="modal1" class="modal_div">
    <div class="modal_close">

    </div>
    <div class="modal-data">
        15-17 мая
    </div>
    <div class="modal-title">
        Самоценность
    </div>
    <div class="modal-wrapper__container">
        <div class="nav-modal-one"></div>
        <div class="modal-wrapper owl-carousel owl-theme slide-two-one-modal">
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    15 мая
                </div>
                <div class="modal-data-detail__text">
                    Введение, описание курса.
                    Первый шаг - возвращение себя в себя.<br>
                    2 практики:
                    - фокусировка
                    - вспоминаем свое имя по авторской методике Тимура Гильманова
                </div>
            </div>
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    16 мая
                </div>
                <div class="modal-data-detail__text">
                    Успокаиваем нервную систему. Война закончена, можно сложить оружие.<br>Практика:- Включаем
                    нервные
                    центры и ощущаем свою психическую силу.
                </div>
            </div>
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    17 мая
                </div>
                <div class="modal-data-detail__text">
                    ВЕРА.<br>
                    Осознаем, где мы не верим в себя во всех сферах жизни. Гармонизация состояния ВЕРЫ.
                </div>
            </div>
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    15 мая
                </div>
                <div class="modal-data-detail__text">
                    Введение, описание курса.
                    Первый шаг - возвращение себя в себя.<br>
                    2 практики:
                    - фокусировка
                    - вспоминаем свое имя по авторской методике Тимура Гильманова
                </div>
            </div>
        </div>
    </div>
    <div class="button-wrapper">
        <a class="button close-in-popup" href="<?=$paylink?>">Принять участие</a>
    </div>
</div>
<div id="modal2" class="modal_div">
    <div class="modal_close">

    </div>
    <div class="modal-data">
        18-20 мая
    </div>
    <div class="modal-title">
        Самобытность
    </div>
    <div class="modal-wrapper__container">
        <div class="nav-modal-two"></div>
        <div class="modal-wrapper owl-carousel owl-theme slide-two-two-modal">
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    18 мая
                </div>
                <div class="modal-data-detail__text">
                    Отказываемся от попыток получить желаемое, когда на это нет энергии.<br>
                    Как выйти из повторяющихся циклических событий. Практика:
                    - Убираем старые обиды и переживания.
                </div>
            </div>
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    19 мая
                </div>
                <div class="modal-data-detail__text">
                    Ключевой день: учимся включать счастье в здесь и теперь.<br>
                    Практика:
                    - Ощущаем истинное счастье внутри нас и сохраняем это чувство до конца курса.
                </div>
            </div>
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    20 мая
                </div>
                <div class="modal-data-detail__text">
                    Правда.
                    Психика деградирует, когда мы врём и обманываем себя, а значит и мир. Это трудно увидеть
                    сразу.<br>
                    Как перестать врать и научиться говорить правду?
                    Он-лайн сессия "Маски и субличности, как с ними работать".
                </div>
            </div>
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    18 мая
                </div>
                <div class="modal-data-detail__text">
                    Отказываемся от попыток получить желаемое, когда на это нет энергии.<br>
                    Как выйти из повторяющихся циклических событий. Практика:
                    - Убираем старые обиды и переживания.
                </div>
            </div>
        </div>
    </div>
    <div class="button-wrapper">
        <a class="button close-in-popup" href="<?=$paylink?>">Принять участие</a>
    </div>
</div>
<div id="modal3" class="modal_div">
    <div class="modal_close">

    </div>
    <div class="modal-data">
        21-24 мая
    </div>
    <div class="modal-title">
        Самосознание
    </div>
    <div class="modal-wrapper__container">
        <div class="nav-modal-three"></div>
        <div class="modal-wrapper owl-carousel owl-theme slide-two-three-modal">
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    21 мая
                </div>
                <div class="modal-data-detail__text">
                    Авторская практика на работу с прошлым.<br>
                    Не "стираем" прошлые переживания, а перерабатываем и осознаем их через любовь:
                    оставаться в старом и копаться там - это не только невыгодно нам, но ещё и очень вредно.
                </div>
            </div>
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    22 мая
                </div>
                <div class="modal-data-detail__text">
                    Закрепляем состояние "Кто я на самом деле?"<br>
                    Практика:- выявляем истинные желания. Начинаем видеть, где мы действуем от своего состояния, а
                    где от ложного, когда нами руководят наши субличности.
                </div>
            </div>
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    23 мая
                </div>
                <div class="modal-data-detail__text">
                    Закладываем фундамент будущего.<br>
                    Теперь ты - женщина, которая управляет своим психическим состоянием и может работать с будущим,
                    а не сидеть в прошлом. Усиливаем истинные желания изнутри.
                </div>
            </div>
            <div class="modal-data-detail">
                <div class="modal-data-detail__data">
                    24 мая
                </div>
                <div class="modal-data-detail__text">
                    Итоги курса "Я хочу быть женщиной, но забыла как".<br>
                    Мои личные пожелания всем участницам.
                    Ответы на вопросы.
                </div>
            </div>
        </div>
    </div>
    <div class="button-wrapper">
        <a class="button close-in-popup" href="<?=$paylink?>">Принять участие</a>
    </div>
</div>

<div id="overlay"></div> <!-- Пoдлoжкa, oднa нa всю стрaницу -->
<script src="{{asset('js/all/jquery.min.js')}}"></script>
<script src="./js/index/slick.min.js"></script>
<script src="./js/index/owl.carousel.min.js"></script>
<script src="./js/index/script.js"></script>

<script src="{{asset('js/all/scripts.js') }}"></script>


<script>

    $('#vkontakte-button').on('click',function (e) {
        e.preventDefault();
        window.open('{{ route('auth.login',['auth'=>'vkontakte'])  }}','Checking Auth',"height=380,width=650");
        return -1;
    })
    $('#facebook-button').on('click',function (e) {
        console.log('clicked on facebook...')
        window.open('{{ route('auth.login',['auth'=>'facebook'])  }}','Checking Auth',"height=600,width=800");
        return -1;
    })


    $(function () {




    })
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(55714759, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/55714759" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
