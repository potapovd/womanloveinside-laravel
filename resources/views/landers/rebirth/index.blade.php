
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <meta name="robots" content="noindex, nofollow, noodp, noarchive, nosnippet, noimageindex" />

    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>Rebirth! Годовая трансформационная программа "ВОЗРОЖДЕНИЕ ЖЕНЩИНЫ"</title>
    <!--    <link rel="icon" type="image/png" sizes="192x192" href="/src/favicon/android-icon-192x192.png">-->
    <!--    <link rel="icon" type="image/png" sizes="96x96" href="/src/favicon/favicon-96x96.png">-->
    <!--    <link rel="icon" type="image/png" sizes="32x32" href="/src/favicon/favicon-32x32.png">-->
    <!--    <link rel="icon" type="image/png" sizes="16x16" href="/src/favicon/favicon-16x16.png">-->
    <!--    <link id="favicon" rel="icon" type="image/x-icon" href="/src/favicon/favicon.ico">-->
    <meta content="#673ba7" name="msapplication-TileColor">
    <meta content="#673ba7" name="theme-color">

    <link href="./css/rebirth/bootstrap.min.css" rel="stylesheet" />
    <link href="./css/rebirth/aos.css" rel="stylesheet" />
    <link href="./css/rebirth/swiper.min.css" rel="stylesheet" />
    <link href="./css/rebirth/style.css" rel="stylesheet" />

    <link rel="canonical" href="{{route('lander.rebirth')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="196x196" href="{{asset('img/icons/favicon-196.png')}}"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/icons/apple-icon-180.png')}}"/>
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset('img/icons/apple-icon-167.png')}}"/>
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/icons/apple-icon-152.png')}}"/>
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/icons/apple-icon-120.png')}}"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>


{{--    <link rel="canonical" href="https://iwanttobeawoman.ru/ikh/" />--}}
{{--    <meta property="og:locale" content="ru_RU" />--}}
{{--    <meta property="og:type" content="article" />--}}
{{--    <meta property="og:title" content="Знаю как - Хочу быть женщиной" />--}}
{{--    <meta property="og:url" content="https://iwanttobeawoman.ru/ikh/" />--}}
{{--    <meta property="og:site_name" content="Хочу быть женщиной" />--}}
{{--    <meta property="og:image" content="https://iwanttobeawoman.ru/wp-content/uploads/2018/12/ikh-fb.jpg" />--}}
{{--    <meta property="og:image:secure_url" content="https://iwanttobeawoman.ru/wp-content/uploads/2018/12/ikh-fb.jpg" />--}}
{{--    <meta property="og:image:width" content="1250" />--}}
{{--    <meta property="og:image:height" content="691" />--}}
{{--    <meta name="twitter:card" content="summary_large_image" />--}}
{{--    <meta name="twitter:title" content="Знаю как - Хочу быть женщиной" />--}}
{{--    <meta name="twitter:image" content="https://iwanttobeawoman.ru/wp-content/uploads/2018/12/ikh-fb.jpg" />--}}

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{asset('js/all/sweetalert.min.js') }}"></script>

    <style>
        @media (max-width: 768px){
            .reviews {
                width: 100%;
                margin: 0 auto;
            }
        }
        .providericon svg{
            filter: grayscale(.7);
            opacity: .65;
            transition: all .5s;
            cursor: pointer;
        }
        .providericon svg:hover,
        .providericon svg.providericon-active{
            opacity: 1;
            filter: grayscale(0);
        }
    </style>

</head>

<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () { (m[i].a = m[i].a || []).push(arguments) };
        m[i].l = 1 * new Date(); k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(59642248, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/59642248" style="position:absolute; left:-9999px;" alt="" /></div>
</noscript>
<!-- /Yandex.Metrika counter -->
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return; n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
        n.queue = []; t = b.createElement(e); t.async = !0;
        t.src = v; s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '460025111365219');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=460025111365219&ev=PageView&noscript=1" /></noscript>
<!-- End Facebook Pixel Code -->

<section id="header">
    <div class="container">
        <div class="col-md-12 position-relative">
            <img alt="" class="position-absolute round-bg" src="./img/rebirth/Untitled-6@2x.png" srcset="./img/rebirth/Untitled-6@2x.png,
                     ./img/rebirth/Untitled-6-mob.png 991w">
            <div class="row">
                <div class="col-lg-5 offset-lg-4 col-md-10 ">
                    <nav class="mobile-hidden">
                        <ul class="d-flex flex-row top-menu">
                            <li><a class="goto" href="#goals">О курсе</a></li>
                            <li><a class="goto" href="#programs">Программа</a></li>
                            <li><a class="goto" href="#plans">Варианты</a></li>
                            <!-- <li><a class="goto" href="#comments">FAQ</a></li> -->
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-1 col-12 col-md-2">
                    <div class="row">
                        <div class="col-md-6 col-2 offset-10">
                            <a class="d-flex justify-content-center align-items-center rounded-circle brand-bg linktochat "
                               href="https://lk.timurgilmanov.com">
                                <svg data-name="Layer 1" id="Layer_1" viewBox="0 0 510.29 560.58"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <defs></defs>
                                    <title>Vector Smart Object1</title>
                                    <path class="st0"
                                          d="M300.17,134.89l16,.05c3.5.66,7,1.47,10.48,2,30.19,4.41,50.42,21.86,61,49.59,7.49,19.68,12.45,40.35,18.21,60.67,13,45.84,31.69,88.45,67,122.05,9.8,9.33,6.85,21.16-6.25,25-5,1.45-10.45,1.61-15.7,1.63-22,.11-44,.2-66-.22-14.07-.26-18.55,7.67-20.33,16.09-4.56,21.57,4,43.68,22.68,55.1,10.41,6.38,22.24,10.47,33.5,15.41,23.7,10.39,47.19,21.15,68.48,36.11,19.46,13.67,34.49,30.83,42.44,53.59,6.34,18.16,7,37,6.11,55.88-.42,9.32-6.5,16.06-15.38,18.43-1.52.4-3,.82-4.55,1.23l-422-1.1a6.14,6.14,0,0,0-1.69-.93c-10.89-1.82-17.52-7.76-18-18.42-.77-15.7-.43-31.41,3.45-46.9C86.85,551,105,530.38,129.5,514.35s51.38-27.62,78.44-38.59c12.68-5.13,25-10.47,33.18-22.24,9.69-13.93,13.14-29,8.2-45.46-2.42-8.1-8-12.24-16.44-12.61-6.49-.28-13-.1-19.5-.18-20.31-.24-40.63-.31-60.94-.89-7-.2-12.65-3.68-15.19-10.74-2.29-6.35.6-11,5-15.39,6.12-6.1,12.49-12.07,17.83-18.83,23.44-29.61,38.1-63.62,48.83-99.52,6.33-21.2,11.72-42.74,19.39-63.44,9.17-24.77,26.78-41.69,53.12-48.13C287.58,136.82,293.92,136,300.17,134.89Z"
                                          transform="translate(-51.78 -110.89)" />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row header-top-text">
                <div class="col-lg-6 mt-5 mb-5">
                    <div class="text-center" id="mainheader">
                        <span class="mainheader-first d-block text-center">стартуем</span>
                        <span class="mainheader-second d-inline-block text-center">1 сентября</span>
                        <img alt="" class="position-absolute mainheader-third-img"
                             src="./img/rebirth/Untitled-10.png" srcset="./img/rebirth/Untitled-10@2x.png 2x">


                        <div class="mainheader-third d-block text-center position-relative">
                            <img
                                src="./img/rebirth/VectorSmartObject-rebith@2x.png"
                                class="img-fluid rebith-logo"
                                alt=""><br>

                                <div class="position-relative animated-title-out">
                                    <div class="animated-title">
                                        <div class="text-top">
                                            <div>
                                                <span class="fz-28 ">
                                                   Годовая трансформационная программа
                                                </span>
                                                <span class="fz-80 mt-lg-2">
                                                    Возрождение
                                                </span>
                                            </div>
                                        </div>
                                        <div class="text-bottom">
                                            <div>
                                                <span class="fz-100 ">
                                                    Женщины
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!--                                Я хочу быть  <br>-->
                            <!--                                 женщиной <br> и знаю как!-->

                            <!--                                <div class="position-relative animated-title-out">-->
                            <!--                                    <div class="animated-title">-->
                            <!--                                        <div class="text-top">-->
                            <!--                                            <div>-->
                            <!--                                                <span>Я хочу быть </span>-->
                            <!--                                                <span>женщиной</span>-->
                            <!--                                            </div>-->
                            <!--                                        </div>-->
                            <!--                                        <div class="text-bottom">-->
                            <!--                                            <div>и знаю как!</div>-->
                            <!--                                        </div>-->
                            <!--                                    </div>-->
                            <!--                                </div>-->


                        </div>
                        <!-- <div class="desktop-hidden">
                            <div class="d-flex flex-row allpoints">
                                <div class="onepoint d-flex flex-column bg-pink">
                                    <span class="onepoint-m text-color-white" style="margin-left:-4px">12</span>
                                    <span class="onepoint-t">
                                            месяцев <br> &nbsp;
                                        </span>
                                </div>
                                <div class="onepoint d-flex flex-column bg-pink">
                                    <span class="onepoint-m text-color-white">20</span>
                                    <span class="onepoint-t text-color-white">
                                            человек <br> в группе
                                        </span>
                                </div>
                                <div class="onepoint d-flex flex-column bg-pink">
                                    <span class="onepoint-m text-color-white" style="margin-left:-4px">12</span>
                                    <span class="onepoint-t text-color-white">
                                            вебинаров <br> в месяц
                                        </span>
                                </div>
                                <div class="onepoint d-flex flex-column bg-pink">
                                    <span class="onepoint-m text-color-white">6</span>
                                    <span class="onepoint-t text-color-white">
                                            консультаций <br> &nbsp;
                                        </span>
                                </div>
                            </div>
                        </div> -->
                        <span class="mainheader-fourth d-block mt-4 text-center">
                                <a class="d-inline-block buttonlink text-center goto" href="#plans">Записаться на
                                    курс</a>
                            </span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="goals">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 pt-5 mobile-text-center">
                <img alt="" class="mb-4" src="./img/rebirth/Untitled-18.png"
                     srcset="./img/rebirth/Untitled-18@2x.png 2x">
                <h2 class="assets-title">
                    Цели курса
                </h2>
            </div>
            <div class="col-lg-3 d-flex align-content-end justify-content-end flex-column pb-3  ">
                    <span class="mainheader-fourth d-block mt-4 text-center mobile-hidden">
                        <a class="d-inline-block buttonlink text-center goto" href="#programs">Программа курса</a>
                    </span>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <!-- Slider main container -->
                <div class="swiper-container" id="swiper-container-goals">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper" data-aos="fade">
                        <!-- Slides -->
                        <div class="swiper-slide swiper-slide-slide1">
                            <div class="slider-object">
                                <h3 class="slider-object-title">
                                    ТВОЁ ПРЕОБРАЖЕНИЕ
                                    В ЖЕНЩИНУ
                                </h3>
                                <p class="slider-object-text">
                                    - ту, которой восторгаются мужчины, которой благоволит сама жизнь.
                                </p>
                                <div class="slider-object-img-out text-center">
                                    <img alt="" class="slider-object-img text-center"
                                         src="./img/rebirth/Untitled-7.png"
                                         srcset="./img/rebirth/Untitled-7@2x.png 2x">
                                </div>

                            </div>
                        </div>

                        <div class="swiper-slide swiper-slide-slide2">
                            <div class="slider-object">
                                <h3 class="slider-object-title">
                                    ТЫ МЕНЯЕШЬСЯ И СТАНОВИШЬСЯ ЗАМЕТНОЙ
                                </h3>
                                <p class="slider-object-text">
                                    Ты формируешь события и окружающую действительность своим внутренним состоянием.
                                </p>
                                <div class="slider-object-img-out text-center">
                                    <img alt="" class="slider-object-img text-center"
                                         src="./img/rebirth/Untitled-8.png"
                                         srcset="./img/rebirth/Untitled-8@2x.png 2x">
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide swiper-slide-slide3">
                            <div class="slider-object">
                                <h3 class="slider-object-title">
                                    ПЕРЕЙТИ НА НОВЫЙ УРОВЕНЬ И ОСТАТЬСЯ ТАМ
                                </h3>
                                <p class="slider-object-text">
                                    Удержаться в новой реальности, не возвращаясь в прежнее состояние.
                                </p>
                                <div class="slider-object-img-out text-center">
                                    <img alt="" class="slider-object-img text-center"
                                         src="./img/rebirth/Untitled-1.png"
                                         srcset="./img/rebirth/Untitled-1@2x.png 2x">
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide swiper-slide-slide4">
                            <div class="slider-object">
                                <h3 class="slider-object-title">
                                    12 МЕСЯЦЕВ - ЭТО МНОГО ИЛИ МАЛО?
                                </h3>
                                <p class="slider-object-text mt-2">
                                    Многие идут к этому годами. Доходят единицы. Мы сумеем за полгода — и точно
                                    получим
                                    результат.
                                </p>
                                <div class="slider-object-img-out text-center">
                                    <img alt="" class="slider-object-img text-center"
                                         src="./img/rebirth/Untitled-2.png"
                                         srcset="./img/rebirth/Untitled-2@2x.png 2x">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-scrollbar"></div>
                </div>

            </div>
        </div>
    </div>
</section>

<section id="tasks">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 pt-5 pb-5">
                <h2 class="assets-title mobile-text-center">
                    Задачи <br>
                    курса
                </h2>
                <h3 class="slider-object-title">
                    ПОСМОТРИ, ЧТО ТЫ БУДЕШЬ УМЕТЬ ДЕЛАТЬ ТАК, КАК БУДТО ЗНАЛА И УМЕЛА ЭТО С РОЖДЕНИЯ:
                </h3>
                <div class="all-tasks mt-5">
                    <div class="task mb-4" data-aos="fade" data-aos-delay="100">
                        <div class="task-img text-center mb-2">
                            <img alt="" src="./img/rebirth/Untitled-11.png"
                                 srcset="./img/rebirth/Untitled-11@2x.png 2x">
                        </div>
                        <div class="task-text">
                            Ты сможешь закрыть программы бедности и включить состояние богатства по-женски: границ
                            изобилия не существует - долой рамки и страхи. Ты почувствуешь, что достойна жить лучше.
                        </div>
                    </div>
                    <div class="task mb-4" data-aos="fade" data-aos-delay="200">
                        <div class="task-img text-center mb-2">
                            <img alt="" src="./img/rebirth/Untitled-12.png"
                                 srcset="./img/rebirth/Untitled-12@2x.png 2x">
                        </div>
                        <div class="task-text">
                            Ты будешь ощущать время и пространство по-женски. Перестанешь опаздывать, но и спешить
                            никуда не будешь. Ты начнешь успевать, легко и играючи.
                        </div>
                    </div>
                    <div class="task mb-4" data-aos="fade" data-aos-delay="300">
                        <div class="task-img text-center mb-2">
                            <img alt="" src="./img/rebirth/Untitled-13.png"
                                 srcset="./img/rebirth/Untitled-13@2x.png 2x">
                        </div>
                        <div class="task-text">
                            Секс обретёт невероятную глубину, станет моментом любви и единения глаза в глаза.
                        </div>
                    </div>


                    <div class="task mb-4" data-aos="fade" data-aos-delay="400">
                        <div class="task-img text-center mb-2">
                            <img alt="" src="./img/rebirth/change.png" srcset="./img/rebirth/change@2x.png 2x">
                        </div>
                        <div class="task-text">
                            Ты наполнишься женской энергией и раскроешь в себе женственность,  осознаешь свою
                            ценность и самодостаточность. Ты отпустишь тотальный контроль и напряжение из своей
                            жизни.
                        </div>
                    </div>
                    <div class="task mb-4" data-aos="fade" data-aos-delay="450">
                        <div class="task-img text-center mb-2">
                            <img alt="" src="./img/rebirth/family.png" srcset="./img/rebirth/family@2x.png 2x">
                        </div>
                        <div class="task-text">
                            Ты создашь своим новым состоянием гармонию в отношениях: с мужчиной, детьми, близкими
                            людьми и всем миром.
                        </div>
                    </div>


                    <div class="book-place mt-5 text-center">
                        <a class="d-inline-block buttonlink text-center goto" href="#plans">Забронировать место</a>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 pt-5 mt-3 mobile-hidden">
                <img alt="" class="img-fluid " data-aos="fade" data-aos-delay="350"
                     src="./img/rebirth/Untitled-6-2.png" srcset="./img/rebirth/Untitled-6-2@2x.png 2x">
            </div>
        </div>
    </div>
</section>

<section id="programs">
    <div class="container position-relative overflow-hidden">

        <div class="row">
            <div class="col-lg-12">
                <h2 class="assets-title mobile-text-center">
                    Программа
                    курса
                    <!--                    <span class="indent-50"></span>-->
                </h2>
                <div class="position-relative">
                    <div class="program-date text-center">
                        1 сентября 2020 - 31 августа 2021
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 position-relative">

                <!-- Slider main container -->
                <div class="swiper-container" id="swiper-container-programs">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper align-items-stretch ">
                        <!-- Slides -->

                        <div class="swiper-slide" data-aos="fade" data-aos-delay="100">
                            <div
                                class="slider-object-month d-flex flex-column justify-content-end align-content-end">
                                <div class="month-number text-center">1</div>
                                <div class="month-column column60">
                                    <div class="text-center month-column-title pt-2">месяц <br> эмоциональный баланс</div>
                                    <div class="month-column-text pt-2 pl-3 pr-3 pb-3">
                                        <ul>
                                            <li> Самооценка</li>
                                            <li> Самоценность</li>
                                            <li> Полная балансировка психики</li>
                                            <li> Полный отказ от женЧИНского состояния</li>
                                            <li> Полное принятие всех своих женских качеств</li>
                                            <li> Умение управлять своими эмоциями: более десятка инструментов</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" data-aos="fade" data-aos-delay="200">
                            <div
                                class="slider-object-month d-flex flex-column justify-content-end align-content-end">
                                <div class="month-number text-center">2</div>
                                <div class="month-column column60">
                                    <div class="text-center month-column-title pt-2">месяц <br> доверие</div>
                                    <div class="month-column-text pt-2 pl-3 pr-3 pb-3">
                                        <ul>

                                            <li> Как научиться доверять сильнейшему качеству - женской интуиции</li>
                                            <li> Учимся принимать в жизни всё - и хорошее, и плохое: практики и
                                                техники
                                            </li>
                                            <li> Доверяем своему любимому управление теми процессами, которые ему
                                                нужны
                                                от природы
                                            </li>
                                            <li> Доверяем себе любимой управление теми процессами, которые нужны
                                                женщине
                                                от природы
                                            </li>
                                            <li> Плохое и хорошее - это всегда вопрос восприятия, как сфомировать
                                                это
                                                состояние и следовать ему всю жизнь
                                            </li>
                                            <li> Как довериться ресурсному женскому состоянию и больше не пытаться
                                                его
                                                удержать, а просто жить этим
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" data-aos="fade" data-aos-delay="300">
                            <div
                                class="slider-object-month d-flex flex-column justify-content-end align-content-end">
                                <div class="month-number text-center">3</div>
                                <div class="month-column column60">
                                    <div class="text-center month-column-title pt-2">месяц <br> секс</div>
                                    <div class="month-column-text pt-2 pl-3 pr-3 pb-3">
                                        <ul>
                                            <li> Как настроить себя на открытость в отношениях без вульгарности. Как
                                                стать сексуальной по-настоящему
                                            </li>
                                            <li> Как проработать свои блоки и зажимы, и зажимы своего любимого</li>
                                            <li> Секс, как важнейший элемент энергетического обмена между мужчиной и
                                                женщиной
                                            </li>
                                            <li> Открываем состояние секса, чтобы не надо было изучать никакие
                                                методики,
                                                как "правильно" заниматься сексом. Естественность нам в помощь
                                            </li>
                                            <li> Глубинные, инстинктивные проработки себя через сексуальные
                                                отношения с
                                                любимым
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" data-aos="fade" data-aos-delay="500">
                            <div
                                class="slider-object-month d-flex flex-column justify-content-end align-content-end">
                                <div class="month-number text-center">4</div>
                                <div class="month-column column60">
                                    <div class="text-center month-column-title pt-2">месяц <br> дисциплина</div>
                                    <div class="month-column-text pt-2 pl-3 pr-3 pb-3">
                                        <ul>

                                            <li> Вычленяем и прорабатываем все важные и неважные женские дела</li>
                                            <li> Основа именно женской дисциплины и чем она отличается от мужской
                                            </li>
                                            <li> Как всё успевать при наличии детей, дел, семейного быта, работы, но
                                                при
                                                этом быть в ресурсе и сохранять энергию
                                            </li>
                                            <li> Самореализация для женщины, хобби, творчество</li>
                                            <li> Как настраивать детей на дисциплину, порядок и делать так, чтобы
                                                они
                                                помогали в быту + автоматически учились и получали от этого
                                                удовольствие
                                            </li>
                                            <li> Методы управления вниманием, сохранения энергии, когда идет
                                                семейный
                                                переход на новый уровень
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" data-aos="fade" data-aos-delay="600">
                            <div
                                class="slider-object-month d-flex flex-column justify-content-end align-content-end">
                                <div class="month-number text-center">5</div>
                                <div class="month-column column60">
                                    <div class="text-center month-column-title pt-2">месяц <br> богатство</div>
                                    <div class="month-column-text pl-2 pr-2 pb-2">
                                        <ul>

                                            <li> Богатство, как норма жизни</li>
                                            <li> Как женщине формировать богатство: основные столпы, на которых
                                                строится
                                                это состояние
                                            </li>
                                            <li> Как помочь настроить мужчину на богатство</li>
                                            <li> Как заложить состояние, мышление богатства в детей</li>
                                            <li> Проработка всех родовых программ бедности</li>
                                            <li> Денежное планирование, семейное планирование. Как живут, мыслят и
                                                чувствуют деньги успешные семьи, какими критериями они
                                                руководствуются
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" data-aos="fade" data-aos-delay="700">
                            <div
                                class="slider-object-month d-flex flex-column justify-content-end align-content-end">
                                <div class="month-number text-center">6</div>
                                <div class="month-column column60">
                                    <div class="text-center month-column-title pt-2">месяц <br> итоговый</div>
                                    <div class="month-column-text pt-2 pl-3 pr-3 pb-3">
                                        <ul>
                                            <li> Экзамен на состояние "Я ЖЕНЩИНА" с чёткими критериями</li>
                                            <li> "Докрутка" каждой участницы</li>
                                            <li> Написание плана от души, от сердца на ближайшие 5 лет жизни</li>
                                            <li> Экзамен на состояние "Я ВОЛШЕБНИЦА" с чёткими критериями</li>
                                            <li> Закрытые, уникальные практики на работу с женскими энергиями,
                                                душой,
                                                инициация своего внутреннего ЖЕНСКОГО СОСТОЯНИЯ. Это полный
                                                эксклюзив,
                                                который можно получить только на 6-ой месяц тренинга
                                            </li>
                                            <li> Мои личные пожелания, напутствия всем участницам тренинга</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" data-aos="fade" data-aos-delay="700">
                            <div
                                class="slider-object-month d-flex flex-column justify-content-end align-content-end">
                                <div class="month-number text-center">7-12</div>
                                <div class="month-column column60">
                                    <div class="text-center month-column-title pt-2">месяц <br> доведение до идеала </div>
                                    <div class="month-column-text pt-2 pl-3 pr-3 pb-3">
                                        <ul>
                                            <li>Ежемесячные вебинары</li>
                                            <li>Самостоятельное обучение</li>

                                            <li>Проработка проблемных вопросов</li>
                                            <li>Нетворкинг и общение с другими участниками курса </li>
                                            <li>Доступ к эксклюзивным материала</li>
                                            <li>Специальные предложения для участников курса</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <!--                    <div class="swiper-scrollbar"></div>-->

                </div>
                <div class="book-place mt-5 mobile-text-center ">
                    <a class="d-inline-block buttonlink goto" href="#plans">Забронировать место</a>
                </div>

                <!--                <img src="./img/rebirth/Untitled-22@2x.png" data-aos="fade-up" data-aos-delay="450"-->
                <!--                     class="img-fluid graph position-absolute" alt="">-->
            </div>
        </div>

        <img src="./img/rebirth/animation-site-final-v2.gif" class="position-absolute running-girl " alt="">

    </div>
</section>

<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-12 pt-5">
                <h2 class="assets-title mobile-text-center">
                    ТИМУР ГИЛЬМАНОВ
                </h2>
            </div>
        </div>
        <div class="row position-relative pt-2 pb-5">
            <img alt="" class="about-bg" src="./img/rebirth/Untitled-15.png"
                 srcset="./img/rebirth/Untitled-15@2x.png 2x">
            <div class="col-md-4 offset-md-1 no-padding-mobile" data-aos="fade" data-aos-delay="100">
                <div class="text-center mt-3 mb-3">
                    <img alt="" src="./img/rebirth/Untitled-14-3.png" class="about-photo"
                         srcset="./img/rebirth/Untitled-14-3@2x.png 2x">
                </div>
                <h3 class="bg-pink text-color-white font-weight-bold p-3">
                    ЭКСКЛЮЗИВНЫЙ ПОДАРОК ОТ ТИМУРА ГИЛЬМАНОВА ДЛЯ ТЕХ, КТО ПРОШЕЛ ПОЛНЫЙ КУРС - ЛИЧНЫЕ ПОЖЕЛАНИЯ,
                    НАПУТСТВИЯ И БОНУСНЫЕ СЕКРЕТЫ НА БЛИЖАЙШИЕ 5 ЛЕТ ЖИЗНИ.
                </h3>
            </div>
            <div class="col-md-6">
                <p>Это труд. Но какая радость от каждой маленькой победы, от каждого пойманного осознания.
                    <br><br>
                    Это ежедневные практики, но что может сравниться с испытанным в первый раз состоянием любви и
                    теплоты в душе, пробивающим до слез?
                    <br><br>
                    Будет все: и радость предвкушения и пропасть отчаяния, но без этого не бывает развития. И
                    результат,
                    полученный не шаманскими танцами с бубном, а собственной, пусть и мучительной, трансформацией,
                    останется с тобой навсегда.
                    <br><br>
                    У тебя все получится. Я не знаю ни одного человека, который бы работал над собой, и у которого
                    бы не
                    получилось.
                    Я поддержу тебя. Я проведу тебя за руку.<br>
                    Через страхи, сомнения и откаты.<br>
                    Я буду рядом.</p>

                <div class="signature-out">
                    <div class="signature">


                        <svg version="1.1" x="0px" y="0px" viewBox="0 0 548.5 159.9" xml:space="preserve">

                                <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                      stroke-linejoin="round" d="M64.5,146.4c-0.1,0-0.1,0-0.3-0.1c-0.1-0.1-0.2-0.1-0.2-0.2c-0.2-0.1-0.3-0.3-0.4-0.5c-0.1-0.2-0.1-0.7-0.1-1.6
                                    c0-0.9,0.1-2.1,0.3-3.5c0.2-1.5,0.6-3.7,1.1-6.6s1.3-6.4,2.2-10.5c2.6-11.9,6.2-27,10.9-45.3c5.7-22.8,10.5-41.6,14.4-56.5
                                    c-16.8,1.5-32.2,3.8-46.2,6.8c-14,3-24,6.6-30.1,10.5c-3.2,2-4.9,4.2-5.3,6.4c-0.1,0.2-0.2,0.4-0.4,0.5C10.3,46,10.1,46,9.9,46
                                    c-0.2-0.1-0.4-0.2-0.6-0.4c-0.2-0.2-0.2-0.4-0.2-0.6c0.5-2.7,2.5-5.2,6.1-7.5c5.4-3.6,14.3-6.9,26.6-9.9c12.3-3,26.6-5.4,42.9-7.1
                                    c0.9-0.1,2.2-0.2,4.1-0.3c1.9-0.1,3.2-0.3,4.2-0.3c1.2-4.2,1.7-6.5,1.7-6.7c0-0.2,0.2-0.4,0.4-0.5c0.2-0.2,0.5-0.2,0.7-0.1
                                    c0.2,0,0.4,0.2,0.5,0.4c0.1,0.2,0.2,0.5,0.1,0.7c0,0.1-0.5,2.1-1.6,6.1c13.5-1.1,26-1.6,37.6-1.3c11.6,0.2,20.7,1.2,27.1,2.8
                                    c0.2,0,0.4,0.2,0.5,0.4c0.2,0.2,0.2,0.5,0.1,0.7c0,0.2-0.2,0.4-0.4,0.5c-0.3,0.1-0.5,0.2-0.7,0.1c-6.6-1.6-15.9-2.5-27.8-2.7
                                    c-12-0.2-24.3,0.3-37,1.4C87.8,46.7,83,65.3,80,77.1C69.4,119.4,64.5,142,65.2,145c0.3,0.3,0.3,0.7,0,1
                                    C64.9,146.2,64.7,146.4,64.5,146.4z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round" d="M96,91.8c-1.4,0-2.6-0.4-3.6-1.1c-2.5-2.1-2.3-7.5,0.6-16.2c0.1-0.2,0.2-0.4,0.5-0.5c0.2-0.1,0.4-0.1,0.6-0.1
                                    c0.2,0.1,0.4,0.2,0.5,0.5c0.1,0.2,0.1,0.4,0,0.6c-2.7,7.8-3.1,12.6-1.1,14.3c0.6,0.5,1.4,0.8,2.5,0.8c1.1,0,2.4-0.2,3.8-0.6
                                    c1.4-0.4,3.3-1.2,5.7-2.3c2.4-1.1,4.1-2,5.2-2.5c1-0.5,3.2-1.7,6.4-3.4l1.7-0.9c0.2-0.1,0.4-0.1,0.7,0c0.3,0.1,0.4,0.2,0.5,0.4
                                    c0.1,0.2,0.1,0.5,0,0.7c-0.1,0.3-0.2,0.4-0.4,0.5l-1.7,0.8c-2.9,1.6-5.1,2.7-6.7,3.5c-1.6,0.8-3.4,1.7-5.6,2.7
                                    c-2.2,1-4,1.7-5.6,2.2C98.6,91.6,97.2,91.8,96,91.8z M95.3,69.2c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.1,0c-0.2,0-0.4-0.2-0.5-0.4
                                    c-0.1-0.2-0.2-0.5-0.1-0.7c0.7-2.8,1.7-4.4,3-4.8c0.7-0.3,1.4-0.2,2.1,0.3c0.2,0.1,0.3,0.3,0.3,0.6c0,0.3,0,0.5-0.1,0.7
                                    c-0.1,0.2-0.3,0.3-0.6,0.3c-0.3,0-0.5,0-0.6-0.1L98,64.9c-0.3,0.1-0.7,0.6-1,1.3c-0.4,0.8-0.7,1.5-0.9,2.3
                                    C95.9,69,95.7,69.2,95.3,69.2z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round" d="M118.6,91.3c0,0-0.1,0-0.2,0c-0.1,0-0.1,0-0.2,0c-0.4-0.1-0.6-0.5-0.7-1c-0.1-0.5-0.1-1.5,0.1-2.9
                                    c0.1-1.2,0.5-3.3,1.1-6.2l1.8-9c0.1-0.2,0.2-0.4,0.4-0.6s0.4-0.2,0.6-0.2c0.2,0,0.4,0.2,0.6,0.4c0.2,0.3,0.2,0.5,0.2,0.7
                                    c-1.6,6.9-2.5,11.8-2.9,14.9c1.4-3.2,2.2-5,2.2-5.2c1.8-4.2,3-6.9,3.7-8.1c0.7-1.2,1.3-1.8,1.9-1.9c0.5,0,0.9,0.2,1.1,0.5
                                    c0.7,1.2,0.8,5.1,0.3,11.8c2-3.1,3.7-5.1,5.4-5.8c0.5-0.3,1.1-0.4,1.6-0.5c0.5-0.1,1.1,0,1.8,0.2c0.7,0.2,1.3,0.3,1.7,0.5
                                    c0.4,0.1,1.1,0.4,2.1,0.8c1.5,0.6,2.8,1.1,3.7,1.4c0.9,0.3,2.5,0.7,4.7,1.1c2.2,0.4,4.3,0.7,6.4,0.7c2.1,0,4.8-0.2,8.1-0.6
                                    c3.3-0.4,6.8-1.1,10.5-2.1c0.2-0.1,0.4,0,0.7,0.1c0.2,0.1,0.3,0.3,0.4,0.5c0.1,0.6-0.1,1-0.5,1.1c-3.8,1-7.4,1.7-10.8,2.2
                                    c-3.4,0.4-6.2,0.7-8.3,0.7c-2.2,0-4.4-0.2-6.6-0.7c-2.2-0.4-3.9-0.8-4.9-1.2c-1-0.3-2.3-0.8-3.8-1.4c-0.8-0.3-1.4-0.5-1.8-0.7
                                    s-0.9-0.3-1.4-0.4c-0.5-0.1-1-0.2-1.4-0.1c-0.4,0-0.8,0.1-1.2,0.3c-1.6,0.7-3.8,3.5-6.6,8.4c-0.2,0.4-0.6,0.5-1.1,0.4
                                    c-0.4-0.1-0.6-0.4-0.6-1c0.6-5.6,0.7-10.1,0.5-13.4c-0.2,0.5-1.4,3.2-3.7,8.1c-0.9,2.2-1.7,4.1-2.4,5.6
                                    C120,90.4,119.3,91.3,118.6,91.3z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round" d="M174.2,92.8h-0.4c-0.7-0.1-1.1-0.4-1.4-1.1c-0.9-1.6-0.3-5.7,1.9-12.2c0.5-1.4,1-2.8,1.5-4.4c0.6-1.5,1-2.7,1.4-3.5
                                    l0.5-1.2c0.1-0.2,0.2-0.3,0.5-0.4c0.2-0.1,0.5-0.1,0.7,0c0.2,0.1,0.3,0.2,0.4,0.5c0.1,0.2,0.1,0.5,0,0.7c-1.2,2.8-2.3,5.8-3.4,8.9
                                    c-2,5.9-2.6,9.6-2,10.9l0.2,0.2c1.4,0,3.8-2.7,7-8.1c0.8-1.3,1.5-2.7,2.3-4.2c0.8-1.5,1.4-2.7,1.8-3.5l0.6-1.3
                                    c0.2-0.4,0.6-0.6,1.1-0.5c0.4,0.1,0.6,0.4,0.6,1c-0.8,5.9-0.3,9.5,1.5,11c0.4,0.3,1,0.6,1.6,0.7c0.6,0.1,1.4,0.1,2.4-0.1
                                    c0.9-0.2,1.8-0.3,2.7-0.6c0.8-0.2,2-0.6,3.4-1.1s2.6-1,3.6-1.4c1-0.4,2.4-0.9,4.1-1.7c0.3-0.1,0.7-0.3,1.3-0.5
                                    c0.6-0.2,1-0.4,1.3-0.5c0.2-0.1,0.4-0.1,0.7,0c0.2,0.1,0.4,0.3,0.5,0.5c0.1,0.2,0.1,0.4,0,0.7s-0.2,0.4-0.4,0.5
                                    c-0.3,0.1-0.7,0.3-1.3,0.5s-1,0.4-1.3,0.5c-2.1,0.8-3.6,1.5-4.6,1.8c-1,0.4-2.3,0.9-3.9,1.4c-1.6,0.6-2.8,1-3.7,1.1
                                    c-0.8,0.2-1.8,0.3-2.8,0.4c-1,0.1-1.9,0.1-2.6-0.1c-0.7-0.2-1.4-0.5-2-0.9c-1.7-1.3-2.5-4-2.4-8c-0.2,0.3-1.1,2-2.9,5.1
                                    C179,89.8,176.2,92.8,174.2,92.8z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round"
                                  d="M218.3,91.2c-0.5,0-0.8-0.1-1-0.3c-0.2-0.2-0.4-0.5-0.4-0.8c-0.1-0.3,0-0.8,0.1-1.4c0.1-0.6,0.3-1.2,0.4-1.8
                                    c0.2-0.5,0.4-1.4,0.8-2.5c0.9-2.8,1.4-4.2,1.4-4.4c0.1-0.5,0.2-0.9,0.2-1.3s0-0.6-0.1-0.7l-0.1-0.1h-0.2c-2.8,0.1-5.1-0.2-6.9-0.8
                                    c-1.1,1.9-1.9,3.3-2.3,4.2c-0.1,0.2-0.2,0.3-0.5,0.4c-0.2,0.1-0.5,0.1-0.7,0c-0.2-0.1-0.3-0.3-0.4-0.5c-0.1-0.3-0.1-0.5,0-0.7
                                    c0.3-0.7,1.1-2.1,2.3-4.2c-0.9-0.6-1.5-1.3-1.7-2.2c-0.5-1.5-0.3-3.1,0.7-4.8c1-1.8,2.2-3.2,3.5-4.2c1.4-1.1,2.5-1.3,3.3-0.6
                                    c0.9,0.6,0.9,2.1,0.1,4.5c-0.5,1.2-1.6,3.4-3.5,6.6c1.5,0.5,3.5,0.7,5.9,0.6c0.7,0,1.3,0.2,1.6,0.7c0.3,0.3,0.4,0.8,0.5,1.3
                                    c0.1,0.5,0,1.1-0.1,1.9c-0.1,0.7-0.3,1.5-0.6,2.2c-0.2,0.8-0.5,1.7-0.9,2.8c-0.1,0.3-0.2,0.8-0.5,1.4c-0.2,0.7-0.4,1.2-0.5,1.7
                                    c-0.1,0.5-0.2,0.8-0.2,1.1c1.1-0.5,2.5-1.7,4.3-3.6c0.7-0.8,1.3-1.4,1.7-1.8s1-0.9,1.7-1.5s1.5-1.1,2.2-1.4
                                    c0.8-0.3,1.6-0.6,2.4-0.7c0.2,0,0.4,0,0.6,0.2c0.2,0.2,0.4,0.4,0.4,0.6c0,0.2,0,0.4-0.2,0.6c-0.2,0.2-0.4,0.3-0.6,0.4
                                    c-0.9,0.1-1.9,0.5-2.8,1.1c-0.9,0.6-1.7,1.1-2.2,1.6c-0.6,0.5-1.3,1.3-2.1,2.2c-0.7,0.8-1.3,1.4-1.6,1.7c-0.3,0.3-0.8,0.7-1.4,1.3
                                    c-0.6,0.5-1.2,0.9-1.8,1.1C219.1,91.2,218.8,91.2,218.3,91.2z M215.8,66c-0.3,0-0.8,0.3-1.5,0.8c-0.7,0.6-1.4,1.3-2,2.1
                                    c-1.5,2-1.9,3.7-1.2,5.1c0.3,0.4,0.5,0.7,0.8,0.8c1.9-3.3,3-5.4,3.4-6.4C215.6,67.5,215.8,66.7,215.8,66z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round"
                                  d="M271.3,152.8c-1,0-1.9-0.1-2.7-0.3c-3-0.7-5.1-2-6.3-3.9c-1.2-2-1.5-4.4-0.8-7.5c1.1-5,4.6-11.3,10.7-18.9
                                    c5.1-6.5,11.8-13.3,20.1-20.6c6.3-5.5,12.2-10,17.7-13.6c0.7-2.7,1.1-4.3,1.2-4.7c0.2-1,0.6-3.1,1.2-6.2
                                    c-11,10.6-20.6,17.7-28.9,21.5c-8,3.7-14.6,4.1-19.8,1.4c-4.2-2.2-7.1-6.4-8.5-12.6c-1.4-5.8-1.3-12.4,0.1-19.8
                                    c1.1-5.9,2.9-11.7,5.5-17.4c2.6-5.7,5.7-10.8,9.3-15.3c4.2-5.3,9-9.5,14.2-12.7c5.2-3.1,10.7-5,16.5-5.8
                                    c12.3-1.5,20.5,0.4,24.5,5.6c2.4,3.1,3.4,7.4,2.9,12.9c-0.5,5.4-2,11.5-4.8,18.5c-0.1,0.2-0.2,0.4-0.5,0.5s-0.5,0.1-0.7,0
                                    c-0.2-0.1-0.4-0.2-0.5-0.5c-0.1-0.2-0.1-0.5,0-0.7c5.5-14.5,6.2-24.4,2.1-29.8c-3.5-4.6-11.2-6.3-22.9-4.9
                                    c-4.5,0.5-8.9,1.9-13.1,4.1c-4.2,2.2-8,4.9-11.3,8.1c-3.3,3.3-6.3,6.9-9,11c-2.7,4.1-4.9,8.4-6.7,12.9c-1.8,4.5-3.1,9-3.9,13.6
                                    c-1.4,7.4-1.4,13.9,0,19.4s3.9,9.2,7.6,11.1c5,2.7,11.7,1.9,20-2.3c8.3-4.2,17.8-11.5,28.4-22c0.7-4.5,0.9-8,0.7-10.5
                                    c-0.2-2.5-0.6-4.1-1.4-4.7c-0.5-0.4-1.3-0.2-2.6,0.7c-0.2,0.1-0.4,0.1-0.6,0.1c-0.3-0.1-0.5-0.2-0.6-0.3c-0.1-0.2-0.2-0.4-0.2-0.7
                                    c0-0.3,0.2-0.5,0.3-0.6c1.9-1.4,3.5-1.5,4.8-0.5c2.2,1.8,2.6,7.5,1.1,16.9v0.2c-0.4,2.6-0.9,5.6-1.7,8.9c-0.1,0.6-0.4,1.6-0.7,3.1
                                    c8.7-5.3,15-7.5,18.9-6.6c0.2,0,0.4,0.2,0.5,0.4s0.2,0.5,0.1,0.7c0,0.2-0.2,0.4-0.4,0.5c-0.2,0.2-0.5,0.2-0.7,0.1
                                    c-3.8-0.8-10.1,1.7-19,7.3c-3.4,13.2-7.8,25.3-13.2,36.2c-4.2,8.6-8.8,15.5-13.8,20.6C279.9,150.5,275.5,152.8,271.3,152.8z
                                     M309.3,90.6c-5.8,4-11.2,8.1-16,12.4c-8.2,7.2-15,14.3-20.5,21.3s-8.7,12.8-9.7,17.3c-0.6,2.6-0.4,4.6,0.6,6.1s2.7,2.5,5.2,3.1
                                    c4.3,1,8.8-0.8,13.6-5.3c4.8-4.5,9.5-11.5,14.2-20.9C301.6,114.7,305.8,103.4,309.3,90.6z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round"
                                  d="M334.6,91.8c-1.4,0-2.6-0.4-3.6-1.1c-2.5-2.1-2.3-7.5,0.6-16.2c0.1-0.2,0.2-0.4,0.5-0.5c0.2-0.1,0.4-0.1,0.6-0.1
                                    c0.2,0.1,0.4,0.2,0.5,0.5s0.1,0.4,0,0.6c-2.7,7.8-3.1,12.6-1.1,14.3c0.6,0.5,1.4,0.8,2.5,0.8c1.1,0,2.4-0.2,3.8-0.6
                                    s3.3-1.2,5.7-2.3c2.4-1.1,4.1-2,5.2-2.5c1-0.5,3.2-1.7,6.4-3.4l1.7-0.9c0.2-0.1,0.4-0.1,0.7,0s0.4,0.2,0.5,0.4
                                    c0.1,0.2,0.1,0.5,0,0.7c-0.1,0.3-0.2,0.4-0.4,0.5l-1.7,0.8c-2.9,1.6-5.1,2.7-6.7,3.5c-1.6,0.8-3.4,1.7-5.6,2.7s-4,1.7-5.6,2.2
                                    C337.2,91.6,335.8,91.8,334.6,91.8z M333.9,69.2c0,0-0.1,0-0.1,0s-0.1,0-0.1,0c-0.2,0-0.4-0.2-0.5-0.4s-0.2-0.5-0.1-0.7
                                    c0.7-2.8,1.7-4.4,3-4.8c0.7-0.3,1.4-0.2,2.1,0.3c0.2,0.1,0.3,0.3,0.3,0.6c0,0.3,0,0.5-0.1,0.7c-0.1,0.2-0.3,0.3-0.6,0.3
                                    c-0.3,0-0.5,0-0.6-0.1l-0.5-0.1c-0.3,0.1-0.7,0.6-1,1.3s-0.7,1.5-0.9,2.3C334.6,69,334.3,69.2,333.9,69.2z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round" d="M352.5,115.8h-0.3c-0.4-0.1-0.8-0.4-1-0.9c-1.4-3,1.2-17.1,8-42.4c1.5-5.5,3.1-11.4,4.9-17.6c1.8-6.2,3.2-11,4.2-14.5
                                    c1-3.4,1.5-5.2,1.5-5.2c0-0.2,0.2-0.4,0.4-0.5c0.2-0.1,0.5-0.2,0.7-0.1c0.2,0,0.4,0.2,0.5,0.4c0.1,0.2,0.2,0.5,0.1,0.7
                                    c0,0-0.5,1.8-1.6,5.3c-1,3.5-2.5,8.4-4.3,14.7c-1.8,6.3-3.5,12.2-4.9,17.8c-3.1,11.6-5.3,20.6-6.6,27c-1.6,7.8-2.1,12.4-1.4,13.7
                                    c0.4,0.2,0.6,0.5,0.5,1C353.2,115.5,352.9,115.8,352.5,115.8z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round" d="M366.8,91.3c0,0-0.1,0-0.2,0c-0.1,0-0.1,0-0.2,0c-0.4-0.1-0.6-0.5-0.7-1c-0.1-0.5-0.1-1.5,0.1-2.9
                                    c0.1-1.2,0.5-3.3,1.1-6.2l1.8-9c0.1-0.2,0.2-0.4,0.4-0.6c0.2-0.1,0.4-0.2,0.6-0.2c0.2,0,0.4,0.2,0.6,0.4c0.2,0.3,0.2,0.5,0.2,0.7
                                    c-1.6,6.9-2.5,11.8-2.9,14.9c1.4-3.2,2.2-5,2.2-5.2c1.8-4.2,3-6.9,3.7-8.1c0.7-1.2,1.3-1.8,1.9-1.9c0.5,0,0.9,0.2,1.1,0.5
                                    c0.7,1.2,0.8,5.1,0.3,11.8c2-3.1,3.7-5.1,5.4-5.8c0.5-0.3,1.1-0.4,1.6-0.5c0.5-0.1,1.1,0,1.8,0.2c0.7,0.2,1.3,0.3,1.7,0.5
                                    s1.1,0.4,2.1,0.8c1.5,0.6,2.8,1.1,3.7,1.4c0.9,0.3,2.5,0.7,4.7,1.1c2.2,0.4,4.3,0.7,6.4,0.7c2.1,0,4.8-0.2,8.1-0.6
                                    c3.3-0.4,6.8-1.1,10.5-2.1c0.2-0.1,0.4,0,0.7,0.1c0.2,0.1,0.3,0.3,0.4,0.5c0.1,0.6-0.1,1-0.5,1.1c-3.8,1-7.4,1.7-10.8,2.2
                                    c-3.4,0.4-6.2,0.7-8.3,0.7c-2.2,0-4.4-0.2-6.6-0.7c-2.2-0.4-3.9-0.8-4.9-1.2s-2.3-0.8-3.8-1.4c-0.8-0.3-1.4-0.5-1.8-0.7
                                    c-0.4-0.2-0.9-0.3-1.4-0.4c-0.5-0.1-1-0.2-1.4-0.1s-0.8,0.1-1.2,0.3c-1.6,0.7-3.8,3.5-6.6,8.4c-0.2,0.4-0.6,0.5-1.1,0.4
                                    c-0.4-0.1-0.6-0.4-0.6-1c0.6-5.6,0.7-10.1,0.5-13.4c-0.2,0.5-1.4,3.2-3.7,8.1c-0.9,2.2-1.7,4.1-2.4,5.6
                                    C368.2,90.4,367.5,91.3,366.8,91.3z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round" d="M433.4,94.6c-1.2,0-2.3-0.4-3.1-1.1c-0.9-0.8-1.3-2-1.4-3.6s0.4-3.7,1.2-6.4c-1.9,2-3.8,3.7-5.9,5c-2,1.3-3.8,1.6-5.3,0.8
                                    c-0.5-0.3-0.7-0.8-0.6-1.7c0.1-0.9,0.7-2.2,1.6-3.9c0.9-1.7,2.1-3.2,3.5-4.7c1.4-1.5,2.6-2.3,3.8-2.4c1.3-0.2,2.2,0.5,2.8,1.9
                                    c0.1,0.2,0.1,0.4,0,0.7s-0.2,0.4-0.4,0.5c-0.2,0.1-0.5,0.1-0.7,0c-0.2-0.1-0.4-0.2-0.5-0.4c-0.2-0.4-0.4-0.7-0.6-0.8
                                    c-0.2-0.1-0.4-0.1-0.6,0c-1.2,0.3-2.8,1.9-4.7,4.6c-1.5,2.2-2.3,3.8-2.4,4.8c0.9,0.2,2.1-0.2,3.6-1.1c2.7-1.7,5.4-4.5,8.2-8.4
                                    c0.2-0.5,0.5-1.1,0.8-2c0.4-0.8,0.7-1.4,0.8-1.7c0.1-0.3,0.3-0.5,0.7-0.5c0.4,0,0.6,0.1,0.8,0.3c0.7,0.7,0.2,2.3-1.6,4.8
                                    c-2.9,7.3-3.6,11.7-2,13.2c0.3,0.3,0.7,0.5,1.2,0.6c0.5,0.1,1.2,0,2-0.1c0.8-0.2,1.5-0.4,2.2-0.6c0.7-0.2,1.6-0.6,2.8-1.1
                                    c1.2-0.5,2.1-1,2.9-1.4c0.8-0.4,1.9-0.9,3.3-1.7c1.4-0.7,2.5-1.3,3.2-1.7c0.7-0.4,1.8-1,3.3-1.8c0.7-0.4,1.5-0.9,2.4-1.3
                                    s1.9-1,2.9-1.6c1.1-0.6,1.9-1,2.5-1.3c0.2-0.1,0.4-0.1,0.6,0c0.2,0.1,0.4,0.2,0.5,0.4c0.1,0.2,0.1,0.4,0,0.7
                                    c-0.1,0.3-0.2,0.4-0.4,0.5c-3.2,1.7-5.7,3.1-7.7,4.2c-3.6,2-6.3,3.4-8.1,4.4c-1.8,0.9-3.8,1.9-6.2,2.8
                                    C436.7,94.2,434.8,94.6,433.4,94.6z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round"
                                  d="M458.2,93.9H458l-0.2-0.1l-0.2-0.1c0,0-0.1-0.1-0.1-0.1c-0.1,0-0.1-0.1-0.1-0.1c0,0,0-0.1,0-0.2c0-0.1,0-0.1,0-0.2V93
                                    c-0.1-0.8,0.2-2.7,0.8-5.6c0.7-2.9,1.3-5.6,2-8c1-3.7,1.8-6.4,2.5-8.3c0.9-2.6,1.7-4.1,2.4-4.5c0.5-0.3,0.8-0.1,1.1,0.3
                                    c0.3,0.4,0.2,0.8-0.3,1.1c-0.2,0.2-0.4,0.7-0.8,1.5c-0.3,0.8-0.8,2.2-1.4,4.1c-0.6,1.9-1.2,4-1.9,6.5c-1.2,4.5-2,7.5-2.2,8.9
                                    c0.1-0.2,0.3-0.5,0.5-0.9s0.4-0.7,0.5-0.9c2.3-3.7,4-6.5,5.3-8.2c1.3-1.7,2.4-2.6,3.4-2.7c0.6-0.1,1.1,0.1,1.6,0.4
                                    c0.5,0.5,0.9,1.1,1.1,1.8s0.4,1.8,0.6,3c0.1,0.7,0.2,1.3,0.3,1.7c0.1,0.4,0.2,0.9,0.4,1.5c0.2,0.6,0.5,1,0.8,1.4
                                    c0.3,0.4,0.7,0.7,1.1,0.8c2.9,1.4,9.4-0.9,19.4-6.9c0.2-0.1,0.5-0.1,0.7-0.1c0.2,0.1,0.4,0.2,0.6,0.4c0.3,0.5,0.2,0.9-0.4,1.2
                                    c-5.4,3.2-9.8,5.3-13.2,6.5c-3.4,1.1-6,1.2-7.8,0.4c-0.6-0.3-1.2-0.7-1.6-1.2c-0.5-0.5-0.8-1.1-1.1-1.8c-0.3-0.7-0.4-1.3-0.6-1.8
                                    c-0.1-0.5-0.3-1.2-0.4-2c-0.3-2-0.7-3.3-1.2-3.8h-0.1c-0.5,0-1.6,1-3.1,3.1c-0.7,0.9-2.2,3.2-4.4,6.9c-1.9,3-3,4.8-3.3,5.2
                                    c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0.1-0.1,0.1s-0.1,0-0.1,0c-0.1,0-0.1,0-0.1,0H458.2z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round"
                                  d="M492.8,94.9c-0.6,0-1.1-0.2-1.6-0.7c-0.4-0.5-0.7-1.1-0.8-2c-0.1-0.8,0-1.9,0.3-3.1c0.3-1.2,0.8-2.8,1.6-4.5
                                    c0.8-1.8,1.9-3.7,3.2-5.9c0.1-0.2,0.3-0.4,0.6-0.4c0.2-0.1,0.5,0,0.7,0.1c0.5,0.3,0.6,0.7,0.3,1.2c-1,1.7-1.9,3.2-2.6,4.7
                                    c-0.7,1.5-1.2,2.7-1.5,3.6c-0.3,1-0.5,1.8-0.6,2.6c-0.1,0.8-0.1,1.3,0,1.7c0.1,0.4,0.2,0.6,0.3,0.8c0,0.2,0.2,0.2,0.6,0
                                    c0.5-0.2,1-0.7,1.6-1.5c0.6-0.8,1.1-1.9,1.6-3.4c0.5-1.5,0.9-3.3,1-5.5c0.1-2.2,0.1-4.7-0.2-7.6c0-0.2,0.1-0.4,0.3-0.6
                                    c0.2-0.2,0.4-0.3,0.6-0.3c0.2,0,0.4,0.1,0.6,0.2c0.2,0.2,0.3,0.3,0.3,0.5c0.3,3.1,0.4,5.8,0.2,8.1c-0.2,2.3-0.6,4.3-1.2,6
                                    c-0.6,1.7-1.3,3-2,3.9c-0.7,0.9-1.4,1.6-2.1,1.9C493.5,94.8,493.2,94.9,492.8,94.9z" />
                            <path fill="none" stroke="#020202" stroke-width="3.5" stroke-linecap="round"
                                  stroke-linejoin="round" d="M508.4,100H508c-1.8-0.3-2.7-2.8-2.8-7.3c-0.1-3.3,0.2-7.3,0.9-12.1c0.2-1.3,0.4-2.5,0.6-3.4c0.2-1,0.4-1.7,0.6-2.2
                                    c0.2-0.5,0.3-0.9,0.5-1.1c0.2-0.3,0.3-0.5,0.4-0.5c0.1-0.1,0.2-0.1,0.4-0.1c0.2-0.1,0.5,0,0.7,0.1c0.2,0.1,0.4,0.3,0.4,0.5
                                    c0.1,0.5,0,0.8-0.5,1c-0.3,0.4-0.6,1.5-1,3.4c-0.4,1.8-0.7,3.9-0.9,6.4c-0.6,6.1-0.4,10.3,0.4,12.6c0.3,0.7,0.5,1.1,0.7,1.1
                                    c0.2,0,0.4-0.1,0.7-0.4c0.3-0.2,1.1-1.4,2.4-3.5s2.7-5,4.3-8.8c2.6-6.1,4.8-11.5,6.5-16.3l0.1-0.3c0.2-0.5,0.6-0.7,1.1-0.5
                                    c0.2,0.1,0.4,0.2,0.5,0.5c0.1,0.2,0.1,0.4,0,0.6l-0.1,0.3c-2.2,5.9-4.4,11.4-6.6,16.6c-1.6,3.6-3,6.4-4.2,8.5
                                    c-1.2,2.1-2.2,3.4-2.8,4C509.6,99.7,509,100,508.4,100z" />

                            </svg>
                    </div>
                    <div id="inViewport"></div>
                </div>

            </div>
        </div>
    </div>
</section>

<section id="plans">
    <div class="container">
        <div class="row">
            <div class="col-md-12 pt-5 ">
                <!-- <h2 class="assets-title mobile-text-center">Варианты участия</h2> -->
                <h2 class="assets-title mobile-text-center">Стоимость участия</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12 pt-2 pb-5">

                <!-- Slider main container -->
                <div class="swiper-container" id="swiper-container-plan">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">

                        <!-- Slides -->
                        <div class="swiper-slide" data-aos="fade">
                            <div class="slider-object-plan ">
                                <!-- <img src="./img/rebirth/sale-31.png" srcset="./img/rebirth/sale-31@2x.png 2x"
                                    class="sale-block sale-31" alt=""> -->
                                <div class="pt-3 pr-3 pb-0 pl-3">
                                    <h3 class="slider-object-plan-title text-center">
                                        Акционная цена
                                    </h3>
                                    <!--                                    <p class="slider-object-plan-text-oldprice text-center">-->
                                    <!--                                        &nbsp;&nbsp; 66000 &nbsp;&nbsp;<br><br>-->
                                    <!--                                    </p>-->
                                    <!--                                    <p class="slider-object-plan-text-oldprice-free text-center">-->
                                    <!--                                        Экономия - 16 тыс рублей-->
                                    <!--                                    </p>-->
                                    <p class="slider-object-plan-tex text-center line-through">
                                        70.000 ₽ <br><br>
                                    </p>
                                    <!-- <p class="slider-object-plan-text-oldprice-free text-center">
                                        &nbsp; <b>Экономия - <span class="price">9.000 ₽</span></b>
                                    </p> -->
                                    <p class="slider-object-plan-text-oldprice-free text-center">
                                        &nbsp; <b>Акционная цена</b> до 26 августа включительно
                                    </p>
                                </div>
                                <div>
                                    <p class="slider-object-plan-text-newprice text-center">
                                        49.000 ₽
                                    </p>
                                </div>
                                <div class="pt-1 pr-3 pb-3 pl-3 text-center">
                                    <a class="d-inline-block buttonlink text-center link-button"
                                       href="https://lk.timurgilmanov.com/product/rebirth-sale/">Заказать</a>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide" data-aos="fade" data-aos-delay="200">
                            <div class="slider-object-plan">
                                <div class="pt-3 pr-3 pb-0 pl-3">
                                    <h3 class="slider-object-plan-title text-center">
                                        Полная цена
                                    </h3>
                                    <p class="slider-object-plan-tex text-center" style="line-height: 21px;">
                                        {{--12.000 рублей/месяц.<br><br>--}} &nbsp; <br><br>
                                    </p>
                                    <p class="slider-object-plan-text-oldprice-free text-center">
                                        <b>Экономия - <span class="price">0 ₽</span>&nbsp; <b>
                                    </p>
                                </div>
                                <div>
                                    <p class="slider-object-plan-text-newprice text-center">
                                        70.000 ₽
                                    </p>
                                </div>
                                <div class="pt-1 pr-3 pb-3 pl-3 text-center">
                                    <a class="d-inline-block buttonlink text-center link-button"
                                        href="https://lk.timurgilmanov.com/product/rebirth-full/">Заказать</a>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="swiper-slide " data-aos="fade" data-aos-delay="100">
                            <div class="slider-object-plan ">
                                <div class="pt-3 pr-3 pb-0 pl-3">
                                    <img src="./img/rebirth/sale-17.png" srcset="./img/rebirth/sale-17@2x.png 2x"
                                        class="sale-block sale-17" alt="">
                                    <h3 class="slider-object-plan-title text-center">
                                        Рассрочка
                                    </h3>
                                    <p class="slider-object-plan-tex text-center">
                                        3 месяца - по 20.000 рублей/месяц. <br><br>
                                    </p>
                                    <p class="slider-object-plan-text-oldprice-free text-center">
                                        &nbsp; <b>Экономия - <span class="price">12.000 ₽</span></b>
                                    </p>
                                </div>
                                <div>
                                    <p class="slider-object-plan-text-newprice text-center">
                                        20.000 ₽
                                    </p>
                                </div>
                                <div class="pt-1 pr-3 pb-3 pl-3 text-center">
                                    <a class="d-inline-block buttonlink text-center link-button"
                                        href="https://lk.timurgilmanov.com/checkout/?add-to-cart=5953&quantity=1">Заказать</a>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide " data-aos="fade" data-aos-delay="200">
                            <div class="slider-object-plan">
                                <div class="pt-3 pr-3 pb-0 pl-3">
                                    <h3 class="slider-object-plan-title text-center">
                                        Помесячная оплата
                                    </h3>
                                    <p class="slider-object-plan-tex text-center">
                                        12.000 рублей/месяц.<br><br>
                                    </p>
                                    <p class="slider-object-plan-text-oldprice-free text-center">
                                        <b>Экономия - <span class="price">0 ₽</span>&nbsp; <b>
                                    </p>
                                </div>
                                <div>
                                    <p class="slider-object-plan-text-newprice text-center">
                                        12.000 ₽
                                    </p>
                                </div>
                                <div class="pt-1 pr-3 pb-3 pl-3 text-center">
                                    <a class="d-inline-block buttonlink text-center link-button"
                                        href="https://lk.timurgilmanov.com/checkout/?add-to-cart=5954&quantity=1">Заказать</a>
                                </div>
                            </div>
                        </div> -->


                    </div>
                    <div class="swiper-button-next "></div>
                    <div class="swiper-button-prev "></div>
                    <div class="swiper-scrollbar"></div>
                </div>

            </div>
        </div>
    </div>
</section>

<section id="comments">
    <div class="container">
        <div class="row">
            <div class="col-md-12 pt-5 ">
                <h2 class="assets-title mobile-text-center">ОТЗЫВЫ</h2>
            </div>
        </div>
        @include('blocks.comments',['page'=>'rebirth'])
    </div>
</section>



{{--<section id="comments">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12 pt-5 ">--}}
{{--                <h2 class="assets-title mobile-text-center">ОТЗЫВЫ</h2>--}}
{{--            </div>--}}
{{--        </div>--}}


{{--        <div class="row">--}}
{{--            <div class="col-12">--}}

{{--                <div class="d-flex flex-row justify-content-center align-content-center">--}}
{{--                    <div--}}
{{--                        class="d-flex flex-column justify-content-center align-content-center mr-3 comments-number bebas-font text-center">--}}
{{--                        {{ $landerComments->count()  }}--}}
{{--                    </div>--}}
{{--                    <img height="36" class="w-auto" src="{{asset('img/cms/comments-icon@2x.png')}}" alt="">--}}
{{--                </div>--}}
{{--                <div class="text-center total-rating  bebas-font">--}}


{{--                    @if ($landerComments->count()>0)--}}
{{--                        {{ round($landerCommentsSum/$landerComments->count(),1)  }}--}}
{{--                        @for( $i=1; $i<=5; $i++ )--}}
{{--                            @if(round($landerCommentsSum/$landerComments->count())>=$i)--}}
{{--                                <img src="{{asset('img/cms/star-full@2x.png')}}"--}}
{{--                                     title="{{round($landerCommentsSum/$landerComments->count(),1) }}"--}}
{{--                                     class="comment-star comment-star-full" alt="">--}}
{{--                            @else--}}
{{--                                <img src="{{asset('img/cms/star-empty@2x.png')}}"--}}
{{--                                     title="round($landerCommentsSum/$landerComments->count(),1) "--}}
{{--                                     class="comment-star comment-star-empty" alt="">--}}
{{--                            @endif--}}
{{--                        @endfor--}}
{{--                    @endif--}}


{{--                </div>--}}
{{--            </div>--}}

{{--        </div>--}}


{{--        <div class="row">--}}
{{--            <div class="col-12">--}}
{{--                <div id="mc-review"></div>--}}

{{--                <a href="http://cackle.me" id="mc-link">--}}
{{--                    Отзывы для сайта--}}
{{--                    <b style="color:#4FA3DA">Cackl</b>--}}
{{--                    <b style="color:#F65077">e</b>--}}
{{--                </a>--}}


{{--                <div class="row">--}}
{{--                    <div class="col-12">--}}

{{--                        <div class="text-center p-3 leave-comment-txt-block">--}}

{{--                            <div class="leave-comment-txt mb-3">--}}
{{--                                оставьте отзыв, <br> нажав на иконку:--}}
{{--                            </div>--}}

{{--                            <div id="uLoginc513b193"--}}
{{--                                 data-ulogin="display=panel;fields=first_name,last_name,photo,nickname,photo_big;theme=flat;providers=vkontakte,instagram,facebook;callback=getForm"></div>--}}


{{--                            <div class="col-12 d-flex justify-content-center align-content-center" >--}}

{{--                                --}}{{--                            <div id="instagram-button" class="m-2 providericon instagram" style="width: 50px; height: auto">--}}
{{--                                --}}{{--                                --}}{{----}}{{--                                <?xml version="1.0" ?><!DOCTYPE svg  PUBLIC '-//W3C//DTD SVG 1.1//EN'  'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>--}}
{{--                                --}}{{--                                <svg enable-background="new 0 0 50 50" id="Layer_1" version="1.1" viewBox="0 0 50 50" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M34.1,12.6c0,0-18.5,0-18.6,0c-1.6,0-2.9,1.3-2.9,2.9l0,0c0,0,0,19,0,19c0,1.6,1.3,2.9,2.9,2.9  c0,0,19,0,19,0c1.5-0.1,2.9-1.3,2.9-2.9c0,0,0-19,0-19C37.3,14,35.6,12.7,34.1,12.6z M25,20.5c2.5,0,4.5,2,4.5,4.5  c0,2.5-2,4.5-4.5,4.5c-2.5,0-4.5-2-4.5-4.5C20.5,22.5,22.5,20.5,25,20.5z M34.1,32.4c0,0.9-0.7,1.6-1.6,1.6H25h-7.4  c-0.9,0-1.6-0.7-1.6-1.6v-9.5h1.9c-0.2,0.7-0.3,1.3-0.3,2.1c0,4.1,3.3,7.4,7.4,7.4s7.4-3.3,7.4-7.4c0-0.7-0.1-1.4-0.3-2.1h1.9V32.4z   M34.1,16.8v2.5h0c0,0.4-0.4,0.8-0.8,0.8v0h-2.5c-0.4,0-0.8-0.4-0.8-0.8h0v-2.5h0c0,0,0,0,0,0c0-0.5,0.4-0.8,0.8-0.8h2.5v0  C33.7,15.9,34.1,16.3,34.1,16.8C34.1,16.8,34.1,16.8,34.1,16.8L34.1,16.8z" fill="#527FA3" id="camera"/><path d="M25,1C11.7,1,1,11.7,1,25s10.7,24,24,24s24-10.7,24-24S38.3,1,25,1z M25,44C14.5,44,6,35.5,6,25S14.5,6,25,6  s19,8.5,19,19S35.5,44,25,44z" fill="#527FA3"/></svg>--}}
{{--                                --}}{{--                            </div>--}}
{{--                                <div id="facebook-button" class="m-2 providericon facebook" style="width: 50px; height: auto">--}}
{{--                                    --}}{{--                                <?xml version="1.0" ?><!DOCTYPE svg  PUBLIC '-//W3C//DTD SVG 1.1//EN'  'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>--}}
{{--                                    <svg enable-background="new 0 0 50 50" id="Layer_2" version="1.1" viewBox="0 0 50 50" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M30,26l1-5l-5,0v-4c0-1.5,0.8-2,3-2h2v-5c0,0-2,0-4,0c-4.1,0-7,2.4-7,7v4h-5v5h5v14h6V26H30z" fill="#3A5BA0" id="f_1_"/><path d="M25,1C11.7,1,1,11.7,1,25s10.7,24,24,24s24-10.7,24-24S38.3,1,25,1z M25,44C14.5,44,6,35.5,6,25S14.5,6,25,6  s19,8.5,19,19S35.5,44,25,44z" fill="#3A5BA0"/></svg>--}}
{{--                                </div>--}}
{{--                                <div id="vkontakte-button" class="m-2 providericon vkontakte" style="width: 50px; height: auto">--}}
{{--                                    --}}{{--                                <?xml version="1.0" ?><!DOCTYPE svg  PUBLIC '-//W3C//DTD SVG 1.1//EN'  'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>--}}
{{--                                    <svg enable-background="new 0 0 50 50" id="Layer_3" version="1.1" viewBox="0 0 50 50" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M26,34c1,0,1-1.4,1-2c0-1,1-2,2-2s2.7,1.7,4,3c1,1,1,1,2,1s3,0,3,0s2-0.1,2-2c0-0.6-0.7-1.7-3-4  c-2-2-3-1,0-5c1.8-2.5,3.2-4.7,3-5.3c-0.2-0.6-5.3-1.6-6-0.7c-2,3-2.4,3.7-3,5c-1,2-1.1,3-2,3c-0.9,0-1-1.9-1-3c0-3.3,0.5-5.6-1-6  c0,0-2,0-3,0c-1.6,0-3,1-3,1s-1.2,1-1,1c0.3,0,2-0.4,2,1c0,1,0,2,0,2s0,4-1,4c-1,0-3-4-5-7c-0.8-1.2-1-1-2-1c-1.1,0-2,0-3,0  c-1,0-1.1,0.6-1,1c2,5,3.4,8.1,7.2,12.1c3.5,3.6,5.8,3.8,7.8,3.9C25.5,34,25,34,26,34z" fill="#54769B" id="VK_1_"/><path d="M25,1C11.7,1,1,11.7,1,25s10.7,24,24,24s24-10.7,24-24S38.3,1,25,1z M25,44C14.5,44,6,35.5,6,25S14.5,6,25,6  s19,8.5,19,19S35.5,44,25,44z" fill="#54769B"/></svg>--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                            <div style="display: none" id="commentForm">--}}

{{--                                <form action="{{route('lander.add.comment')}}" method="post">--}}
{{--                                    @csrf--}}
{{--                                    --}}{{--                                <input type="hidden" name="name" id="name" value="">--}}
{{--                                    --}}{{--                                <input type="hidden" name="network" id="network" value="">--}}
{{--                                    --}}{{--                                <input type="hidden" name="profile" id="profile" value="">--}}
{{--                                    --}}{{--                                <input type="hidden" name="photo" id="photo" value="">--}}
{{--                                    --}}{{--                                <input type="hidden" name="link" id="link" value="">--}}
{{--                                    --}}{{--                                <input type="hidden" name="page" id="page" value="wakeup">--}}
{{--                                    <input type="hidden" name="page" id="page" value="rebirth">--}}
{{--                                    <input type="hidden" name="rating" id="rating" value="5">--}}
{{--                                    <input type="hidden" name="token" id="token" value="">--}}


{{--                                    <div class="col-12 col-md-8  offset-md-2  p-3">--}}
{{--                                        <div class="row">--}}

{{--                                            <div class="col-md-2 col-12 text-center">--}}
{{--                                                <img class="comment-img img-fluid img-circle" id="new-comment-photo"--}}
{{--                                                     src="" alt="">--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-9 col-12">--}}
{{--                                                <div class="comment-name text-center-mobile">--}}
{{--                                                    <h5 class="text-center" id="new-comment-name"></h5>--}}
{{--                                                </div>--}}
{{--                                                <textarea class="form-control comment-textarea m-3" name="comment" id="comment"--}}
{{--                                                          rows="5"></textarea>--}}
{{--                                                <div class="rating-block text-center-mobile">--}}
{{--                                                    @for( $i=1; $i<=5; $i++ )--}}
{{--                                                        <img src="{{asset('img/cms/star-full@2x.png')}}"--}}
{{--                                                             title="{{$i}}" data-rating="{{$i}}"--}}
{{--                                                             class="comment-star comment-star-full comment-star-select"--}}
{{--                                                             alt="{{$i}}">--}}
{{--                                                    @endfor--}}
{{--                                                </div>--}}
{{--                                                <div class="mobile-text-center mt-3">--}}
{{--                                                    <button type="submit"--}}
{{--                                                            class="btn btn-purple pt-2 pl-2 pr-5 pl-5 button-shadow fz24 hvr-fade hvr-bob">--}}
{{--                                                        опубликовать--}}
{{--                                                    </button>--}}
{{--                                                </div>--}}


{{--                                            </div>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}


{{--                                    --}}{{--                            <div class="row">--}}
{{--                                    --}}{{--                                <div class="col-8 offset-2 col-md-1">--}}
{{--                                    --}}{{--                                    <img class="comment-img img-fluid img-circle"--}}
{{--                                    --}}{{--                                         id="photo-Img" src="" width="50" height="50" alt="">--}}
{{--                                    --}}{{--                                </div>--}}
{{--                                    --}}{{--                                <div class="col-12 col-md-8">--}}
{{--                                    --}}{{--                                    <div id="commentName"  class="comment-name"></div>--}}
{{--                                    --}}{{--                                    <div class="form-group">--}}
{{--                                    --}}{{--                                        <label for="score">Рейтинг</label>--}}
{{--                                    --}}{{--                                        <select name="rating" id="rating">--}}
{{--                                    --}}{{--                                            <option value="1">1</option>--}}
{{--                                    --}}{{--                                            <option value="2">2</option>--}}
{{--                                    --}}{{--                                            <option value="3">3</option>--}}
{{--                                    --}}{{--                                            <option value="4">4</option>--}}
{{--                                    --}}{{--                                            <option selected value="5">5</option>--}}
{{--                                    --}}{{--                                        </select>--}}
{{--                                    --}}{{--                                    </div>--}}

{{--                                    --}}{{--                                    <div class="form-group">--}}
{{--                                    --}}{{--                                        <label for="text">Текст</label>--}}
{{--                                    --}}{{--                                        <textarea class="form-control"  name="comment" id="comment" rows="5"></textarea>--}}
{{--                                    --}}{{--                                    </div>--}}
{{--                                    --}}{{--                                    <button type="submit" class="btn btn-primary">Submit</button>--}}

{{--                                    --}}{{--                                </div>--}}
{{--                                    --}}{{--                            </div>--}}


{{--                                </form>--}}

{{--                            </div>--}}

{{--                            @if (Session::has('success'))--}}
{{--                                <div>--}}
{{--                                    <script>--}}
{{--                                        swal("Спасибо", '{!! Session::get('success') !!}', "success");--}}
{{--                                    </script>--}}
{{--                                </div>--}}
{{--                            @endif--}}

{{--                            @if($errors->any())--}}
{{--                                --}}{{--                   PLS  DON`T KILL ME --}}
{{--                                --}}{{--                    <div class="col-12">--}}
{{--                                --}}{{--                        <div class="box-body">--}}
{{--                                --}}{{--                            <div class="alert alert-danger alert-dismissible">--}}
{{--                                --}}{{--                               --}}
{{--                                --}}{{--                            </div>--}}
{{--                                --}}{{--                        </div>--}}
{{--                                --}}{{--                    </div>--}}
{{--                                <script>--}}
{{--                                    swal("Ошибка", '{!! implode('', $errors->all('<div>:message</div>')) !!}', "error");--}}
{{--                                </script>--}}
{{--                            @endif--}}


{{--                        </div>--}}


{{--                        <div id="all-comments">--}}

{{--                            <div class="comment-block">--}}


{{--                                @foreach($landerComments as $landerComment)--}}

{{--                                    <div class="col-12 col-md-10  offset-md-1 mb-4  mt-4  p-2">--}}
{{--                                        <div class="row">--}}

{{--                                            <div class="col-md-2 col-12 text-center">--}}
{{--                                                <a href="{{$landerComment->link}}" target="_blank">--}}
{{--                                                       <img class="comment-img img-fluid img-circle"--}}
{{--                                                             src={{ asset('img/comments/'.$landerComment->avatar)  }}--}}
{{--                                                                 alt="{{$landerComment->name}}">--}}

{{--                                                    --}}{{-- <img class="comment-img img-fluid img-circle" --}}
{{--                                                    --}}{{--      src={{ asset('img/comments/no-ava.jpg')  }} --}}
{{--                                                    --}}{{--          alt="{{$landerComment->name}}"> --}}


{{--                                                </a>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-9 col-12">--}}
{{--                                                <div class="comment-name text-center-mobile">--}}
{{--                                                    <h5 class="text-center-mobile">--}}
{{--                                                        <a href="{{$landerComment->link}}" target="_blank">--}}
{{--                                                            {{$landerComment->name}}--}}
{{--                                                        </a>--}}
{{--                                                    </h5>--}}
{{--                                                    <div class="m-2">--}}
{{--                                                        @for( $i=1; $i<=5; $i++ )--}}
{{--                                                            @if($landerComment->rating>=$i)--}}
{{--                                                                <img src="{{asset('img/cms/star-full@2x.png')}}"--}}
{{--                                                                     class="comment-star comment-star-full" alt="">--}}
{{--                                                            @else--}}
{{--                                                                <img src="{{asset('img/cms/star-empty@2x.png')}}"--}}
{{--                                                                     class="comment-star comment-star-empty" alt="">--}}
{{--                                                            @endif--}}

{{--                                                        @endfor--}}

{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="comment-text">--}}
{{--                                                    {{$landerComment->comment}}--}}
{{--                                                </div>--}}

{{--                                            </div>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                    --}}{{--                    </div>--}}


{{--                                @endforeach--}}


{{--                            </div>--}}

{{--                        </div>--}}


{{--                    </div>--}}
{{--                </div>--}}


{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}

<!--
<section id="faq">

    <div aria-label="Question Accordions" class="accordion-section clearfix ">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 pb-2">
                    <h2 class="assets-title mobile-text-center"> Часто задаваемые вопросы</h2>
                </div>
            </div>
            <div aria-multiselectable="true" class="panel-group pb-5" id="accordion" role="tablist">


                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading1" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse1" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse1" role="button"
                               title="">
                                Для меня это дорого!
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading1" class="panel-collapse collapse" id="collapse1" role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                Всегда для нас «дорого» лишь то, к чему мы не готовы. Значит, пока нам не по пути.
                            </p>
                            <p>
                                Захотите купить хлеб — найдете деньги на еду. Захотите стать счастливой — придут
                                деньги
                                на тренинг. Только так, а не на наоборот- вот будут деньги, пойду на тренинг. Сперва
                                ЖЕЛАНИЕ, потом действие.
                            </p>
                            <p>
                                Кстати, вопроса «цены» и «дороговизны» вообще не существует. Но это поймет лишь тот,
                                кто
                                пройдет тренинг до конца ;)
                                И еще один момент. Что значит «дорого». На самом деле вопрос приоритетов. Большая
                                часть
                                тех, кто думает, что все очень дорого, легко тратит деньги на одежду и маникюр,
                                салоны
                                красоты и украшения, вкусняшки и подарки на дни рождения. Я проверял это сотни раз.
                                На
                                все это деньги есть? Находятся! Поэтому говорить: денег нет — не совсем верно.
                                Просто,
                                все вышеперечисленное, сейчас для вас важнее, чем инвестировать в себя.
                            </p>
                            <p>
                                Сумма в 44 900 рублей за 6 месяцев, на самом деле, уникально мала по сравнению с тем
                                потоком бонусов, который вы получите по прохождению тренинга. Инвестировать в себя
                                мешает страх, жадность по отношению к себе и неумение себя любить. Любящая себя
                                женщина
                                не жалеет на себя средств.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading2" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse2" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse2" role="button"
                               title="">
                                А если курс мне не даст результата?
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading2" class="panel-collapse collapse" id="collapse2" role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                Ну, что я могу сказать. Женщин в мире не более 10%. Остальные плетутся в хвосте
                                цивилизации и мучительно пытаются урвать у судьбы какие-никакие подарки. А первый
                                признак женщины — это ее уверенность в том, что мир даст ей все необходимое. Ваш
                                выбор —
                                сделать шаг навстречу новому и проверить, убедиться, что вам будет дано. Или не
                                делать.
                                И не убеждаться. Оставаться там, где стоите. А зато безопасно, да? Но и не узнать,
                                что
                                подарок вы ТОЧНО получите.
                            </p>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading3" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse3" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse3" role="button"
                               title="">
                                Я проходила уже много женских тренингов, что нового я узнаю на вашем?
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading3" class="panel-collapse collapse" id="collapse3" role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                Хотел пошутить про воз и лукошко, так, что и не унесете, да передумал. Или нет, так
                                и
                                есть. Я даю то, о чем умалчивают 95% тренеров и коучей. Я просто вручаю ключи. И
                                основной ключ — моя искренняя любовь к тем, кому я даю эти знания. А знания, которые
                                легли на благодатную почву правильного состояния, усваиваются на 100%. Может, мой
                                тренинг не даст вам новой информации, но он поможет перевести вас из состояния
                                «желать»
                                в состояние «делать». И потом, если вы это спрашиваете, значит, предыдущие тренинги
                                не
                                помогли вам применить их информацию. Вот вам и ответ — ЧТО дам я. Я научу вас
                                действовать и применять.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading4" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse4" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse4" role="button"
                               title="">
                                Шесть месяцев обучения — слишком долго для меня.
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading4" class="panel-collapse collapse" id="collapse4" role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                Положа руку на сердце… А сколько лет вы пытаетесь измениться самостоятельно? Как
                                ваши
                                успехи? Сколько должна идти трансформация? Минимум — год. Это честно, это истина. Мы
                                делаем невозможное — мы создали для вас тренинг, который длится 6 месяцев — и это
                                реально супер быстро!
                            </p>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading5" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse5" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse5" role="button"
                               title="">
                                Я и так Женщина, зачем мне этот тренинг?
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading5" class="panel-collapse collapse" id="collapse5" role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                Женщина — это магия, это целое состояние и это, если ваша жизнь полна счастья, любви
                                и
                                богатства. Небольшой чек-лист. Есть ли у вас секс «глаза в глаза»? Есть ли у вас
                                счастливые дети? Вы живете в изобилии? Без долгов и страхов? Вам не нужно работать,
                                чтобы кормить семью, но у вас всегда есть доход? Ваш мужчина счастлив? Процветает?
                                Вы
                                путешествуете 3–5 раз в год? Вы живете объективно счастливо? Вы довольны собой,
                                своим
                                телом и внешностью? К вам с восхищением и уважением относятся мужчины? Вы в гармонии
                                с
                                собой? Вы занимаетесь любимым делом? Тогда ок, вам этот курс действительно не нужен.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading6" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse6" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse6" role="button"
                               title="">
                                Я сейчас одинока, наверное мне стоит пройти курс, когда появится мужчина?
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading6" class="panel-collapse collapse" id="collapse6" role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                За время курса у вас появится мужчина. Всё по подобию. Мужчина у вас будет такой,
                                какая
                                вы. Вы станете лучше, свободнее, обретете настоящую любовь, а значит, по подобию к
                                вам
                                притянется мужчина, с которым вы сможете обрести счастье.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading7" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse7" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse7" role="button"
                               title="">
                                Почему тренинг о том, как стать Женщиной, ведет мужчина? Он не может почувствовать
                                все,
                                что чувствует женщина!
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading7" class="panel-collapse collapse" id="collapse7" role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                Как раз очень даже может. Мужчину мужчиной может сделать только женщина. Женщину
                                женщиной -только мужчина. Мужчина четко чувствует и видит, какое в вас состояние.
                                Женщина такое никогда не почувствует.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading8" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse8" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse8" role="button"
                               title="">
                                У меня сейчас нет возможности пройти курс, я пройду его потом.
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading8" class="panel-collapse collapse" id="collapse8" role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                Когда — потом? Через пару-тройку годиков? Потом — суп с котом, слышали поговорку?
                                Она
                                означает, что потом — значит никогда. Но мы не будем вас убеждать. Пусть вас убедит
                                жизнь. Сами по себе изменения на голову не свалятся. Подождите еще несколько лет,
                                понаблюдайте, как не меняется почти ничего, это выход.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading9" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse9" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse9" role="button"
                               title="">
                                Если тренинг не сработает или мне что-то не понравится, вы вернете мне деньги?
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading9" class="panel-collapse collapse" id="collapse9" role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                Возврата денег нет и не будет. Это вопрос ваших страхов и полнейшего недоверия
                                жизни.
                                Если боитесь потерять свои средства, просто не начинайте. Сомневаетесь — не делайте.
                                Делаете — не сомневайтесь. Тренинг сработает, это проверено.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading10" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse10" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse10" role="button"
                               title="">
                                Я не смогу, у меня за первый тренинг и так не получилось измениться. Думаю, и этот
                                мне
                                не поможет.
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading10" class="panel-collapse collapse" id="collapse10"
                         role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                Первый тренинг — это тест-драйв, или естественный отбор, или как хотите это
                                называйте.
                                Он не дает четких и быстрых изменений, слишком малый срок для этого, но он тормошит
                                сознание, меняет установки и цели. А далее зависит от степени готовности. У кого-то
                                блоков меньше — те сразу ощутили какие-то результаты. А кому-то нужна глубинная
                                работа.
                                Вот она-то и будет на втором тренинге. И на нем НЕ ощутить результаты невозможно.
                                Ощутите. Гарантирую. Ключевой эффект первого тренинга — готовность меняться и ворох
                                новых эмоций. Это вы ощущаете? Значит, тренинг вам помог. Да и сам факт того, что вы
                                «ничего не смогли изменить» уже похож на красный сигнал светофора. Значит, все
                                настолько
                                глубинно и сокрыто внутри, что еще меняться и меняться. Добро пожаловать! Поможем.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default mb-3">
                    <div class="panel-heading p-3" id="heading11" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse11" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse11" role="button"
                               title="">
                                Как я смогу применять знания после тренинга?
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading11" class="panel-collapse collapse" id="collapse11"
                         role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                За время женского клуба, в котором вы будете принимать участие 6 месяцев, случится
                                глобальная трансформация. Новые ощущения войдут в вашу жизнь на уровне состояний и
                                чувств. Вы пропишете свой план на 5 лет вперед и сможете четко руководствоваться им.
                            </p>
                        </div>
                    </div>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading p-3" id="heading12" role="tab">
                        <h3 class="panel-title">
                            <a aria-controls="collapse12" aria-expanded="true" class="collapsed pr-3"
                               data-parent="#accordion" data-toggle="collapse" href="#collapse12" role="button"
                               title="">
                                Чем так ценен именно ваш тренинг? Их сотни и тысячи. Все только деньги гребут. Не
                                верю я
                                никому уже!
                            </a>
                        </h3>
                    </div>
                    <div aria-labelledby="heading12" class="panel-collapse collapse" id="collapse12"
                         role="tabpanel">
                        <div class="panel-body pr-4 pb-4 pl-4">
                            <p>
                                Ну да, лекарств от рака тоже много, только что-то многие болеют и болеют. Я
                                разработал
                                инструмент, который сработал. И работает. И улучшаю его изо дня в день. И главное —
                                у
                                меня есть результаты. И они есть у тех, кому я помог. Жизнь этих девушек изменилась
                                на
                                180 градусов после прохождения моих курсов.
                            </p>
                            <p>
                                И я понимаю, чем вызваны ваши вопросы. Хочется счастья, но страшно, очень страшно,
                                что
                                снова пустышка, снова не то! А то, глядишь, соврут или воды нальют, пользы никакой!
                            </p>
                            <p>
                                Дорогие мои девочки, я в вас верю. Поверьте в себя и вы. Все у нас получится. Не
                                стоит
                                ничего бояться. Все хорошо, правда. Я с вами. И знания мои слишком ценны, чтобы
                                просто
                                так из-за сомнений от них отказаться. Не лишайте себя радости стать самой классной,
                                какой вы еще не были. Я понимаю ваши страхи, иначе бы я вообще не помогал вам.
                                Каждая из
                                вас — прекрасный цветок. Я даю то, что поможет вам раскрыться и расцвести. Все будет
                                хорошо. Просто начнем. Смелее!
                            </p>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</section> -->

<footer>
    <div class="container">
        <div class="row p-4">
            <div class="col-md-9 mobile-text-center">
                <p class="footer-text">Woman Love Inside</p>
            </div>
            <div class="col-md-3 mobile-text-center">
                <!--                <a class="footer-link d-inline-block" href="https://www.facebook.com/nadojit/" target="_blank">-->
                <!--                    <span class="icon-facebook"></span>-->
                <!--                </a>-->
                <a class="footer-link d-inline-block" href="https://www.instagram.com/woman_love_inside/"
                   target="_blank">
                    <span class="icon-instagram"></span>
                </a>
            </div>
        </div>
    </div>
</footer>

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
<script src="./js/rebirth/jquery-2.2.4.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"
        type="text/javascript"></script>
<script crossorigin="anonymous" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script crossorigin="anonymous" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        src="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="./js/rebirth/swiper.min.js"></script>
<script src="./js/rebirth/aos.js"></script>

<script src="{{asset('js/all/scripts.js') }}"></script>

{{--<script src="//ulogin.ru/js/ulogin.js"></script>--}}



<script>
    // function getForm(token) {
    //     $.getJSON("//ulogin.ru/token.php?host=" + encodeURIComponent(location.toString()) + "&token=" + token + "&callback=?", function (data) {
    //         data = $.parseJSON(data.toString());
    //         console.log(data);
    //         if ( !data.error ) {
    //             //alert("Привет, "+data.first_name+" "+data.last_name+"!");
    //
    //             $("#link").attr("src", data.identity);
    //             $("#photo-Img").attr("src", data.photo_big);
    //             $("#photo").attr("value", data.photo_big);
    //             $("#network").attr("value", data.network);
    //             $("#profile").attr("value", data.profile);
    //
    //             $("#name").attr("value", data.first_name + " " + data.last_name);
    //             $("#commentName").html(data.first_name + " " + data.last_name);
    //
    //            // $("#commentForm").slideDown();
    //         }
    //     });
    // }


    {{--$('#instagram-button').on('click',function (e) {--}}
    {{--    e.preventDefault();--}}
    {{--    console.log('clicked on ....')--}}
    {{--    // window.open('{{ route('auth.login',['auth'=>'instagrambasic'])  }}','Checking Auth',"height=500,width=300");--}}
    {{--    return false;--}}
    {{--})--}}

    $('#vkontakte-button').on('click',function (e) {
        e.preventDefault();
        console.log('clicked on vkontakte...')
        window.open('{{ route('auth.login',['auth'=>'vkontakte'])  }}','Checking Auth',"height=380,width=650");

        return false;
    })
    $('#facebook-button').on('click',function (e) {
        e.preventDefault();
        console.log('clicked on facebook...')
        window.open('{{ route('auth.login',['auth'=>'facebook'])  }}','Checking Auth',"height=600,width=800");

        return false;
    })




    $(function () {



    })
</script>



<script>

    $.fn.isOnScreen = function () {

        var win = $(window);

        var viewport = {
            top: win.scrollTop(),
            left: win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

    };
    // $(window).load(function () {
    //     return window.signature.animate();
    // });

    window.addEventListener("DOMContentLoaded", function () {
        //return window.signature.animate();
    });

    (function () {
        window.signature = {
            initialize: function () {
                return $('.signature svg').each(function () {
                    console.log("I: " + $(this));
                    var delay, i, len, length, path, paths, previousStrokeLength, results, speed;
                    paths = $('path, circle, rect', this);
                    delay = 0;
                    results = [];
                    for (i = 0, len = paths.length; i < len; i++) {
                        //if (window.CP.shouldStopExecution(0)) break;
                        path = paths[i];
                        length = path.getTotalLength();
                        previousStrokeLength = speed || 0;
                        speed = length < 100 ? 20 : Math.floor(length);
                        delay += previousStrokeLength + 100;
                        results.push($(path).css('transition', 'none').attr('data-length', length).attr('data-speed', speed).attr('data-delay', delay).attr('stroke-dashoffset', length).attr('stroke-dasharray', length + ',' + length));
                    }
                    // window.CP.exitedLoop(0);
                    return results;
                });

            },
            animate: function () {
                return $('.signature svg').each(function () {
                    console.log("A: " + $(this));
                    var delay, i, len, length, path, paths, results, speed;
                    paths = $('path, circle, rect', this);
                    results = [];
                    for (i = 0, len = paths.length; i < len; i++) {
                        //if (window.CP.shouldStopExecution(1)) break;
                        path = paths[i];
                        length = $(path).attr('data-length');
                        speed = $(path).attr('data-speed');
                        delay = $(path).attr('data-delay');
                        results.push($(path).css('transition', 'stroke-dashoffset ' + speed + 'ms ' + delay + 'ms linear').attr('stroke-dashoffset', '0'));
                    }
                    //window.CP.exitedLoop(1);
                    return results;
                });
            }
        };

    }).call(this);




    var executed = false;
    $(window).on("scroll", function () {
        if (!executed) {
            if ($('#inViewport').isOnScreen()) {
                executed = true;
                window.signature.animate();
            }
        }
    });

    $(document).ready(function () {


        window.signature.initialize();


        //window.signature.initialize();
        //initialize swiper when document ready
        var mySwiper = new Swiper("#swiper-container-goals", {
            slidesPerView: 4,
            centeredSlides: true,
            //slidesPerView: 'auto',
            spaceBetween: 10,
            loop: true,
            scrollbar: {
                el: ".swiper-scrollbar",
                //hide: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                320: {
                    //slidesPerView: 1,
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
                767: {
                    //slidesPerView: 2,
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
                991: {
                    slidesPerView: 2,
                    spaceBetween: 10,
                },
                1280: {
                    slidesPerView: 'auto',
                    //slidesPerView: 3,
                    spaceBetween: 20,
                }
            }
        });

        var mySwiper2 = new Swiper("#swiper-container-programs", {
            slidesPerView: 6,
            //slidesPerView: 'auto',
            spaceBetween: 2,
            autoHeight: true,
            //loop: true,
            scrollbar: {
                el: ".swiper-scrollbar",
                hide: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    slidesPerView: "auto",
                    spaceBetween: 20,
                },
                767: {
                    slidesPerView: 1,
                    //slidesPerView: "auto",
                    spaceBetween: 20,
                },
                991: {
                    slidesPerView: 3,
                    spaceBetween: 22,
                },
                1280: {
                    slidesPerView: 3,
                    //slidesPerView: "auto",
                    spaceBetween: 10,
                }
            }
        });


        var mySwiper3 = new Swiper("#swiper-container-plan", {
        slidesPerView: 1,
        //slidesPerView: 'auto',
        spaceBetween: 10,
        loop: false,
        scrollbar: {
            el: ".swiper-scrollbar",
            hide: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
            hide: true,
        },

        breakpoints: {
            320: {
                //slidesPerView: 1,
                slidesPerView: 1,
                spaceBetween: 20,
            },
            767: {
                //slidesPerView: 2,
                slidesPerView: 1,
                spaceBetween: 20,
            },
            991: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            1280: {
                slidesPerView: 2,
                spaceBetween: 10,
            }
        }
        })

        AOS.init({
            delay: 50,
        });



    });
    //go to form
    $(".goto").click(function (e) {
        let link = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(link).offset().top + 60
        }, "slow");
        return false
    });







</script>

{{--<script type="text/javascript">--}}
{{--    cackle_widget = window.cackle_widget || [];--}}
{{--    cackle_widget.push({ widget: "Review", id: 72137 });--}}
{{--    (function () {--}}
{{--        var mc = document.createElement("script");--}}
{{--        mc.type = "text/javascript";--}}
{{--        mc.async = true;--}}
{{--        mc.src = ("https:" == document.location.protocol ? "https" : "http") + "://cackle.me/widget.js";--}}
{{--        var s = document.getElementsByTagName("script")[0];--}}
{{--        s.parentNode.insertBefore(mc, s.nextSibling);--}}
{{--    })();--}}
{{--</script>--}}

</body>

</html>
