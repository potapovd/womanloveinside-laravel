<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <title>{{$landerMeta['title']}}</title>
    <meta content="{{$landerMeta['keywords']}}" name="keywords"/>
    <meta content="{{$landerMeta['description']}}" name="description"/>
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <link href="img/favicon.ico" rel="icon" type="image/x-icon">
    <meta content="#FFA3810B" name="theme-color">
    <meta content="#FFA3810B" name="msapplication-navbutton-color">
    <meta content="#FFA3810B" name="apple-mobile-web-app-status-bar-style">
    <!-- inject:css -->
    <link rel="stylesheet" href="./css/rich/style.css">
    <!-- endinject -->
        <link rel="canonical" href="{{route('lander.rebirth')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" sizes="196x196" href="{{asset('img/icons/favicon-196.png')}}"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/icons/apple-icon-180.png')}}"/>
    <link rel="apple-touch-icon" sizes="167x167" href="{{asset('img/icons/apple-icon-167.png')}}"/>
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/icons/apple-icon-152.png')}}"/>
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/icons/apple-icon-120.png')}}"/>
    <script src="{{asset('js/all/sweetalert.min.js') }}"></script>>
    <style>
        @media (max-width: 768px) {
            .reviews {
                width: 100%;
                margin: 0 auto;
            }
        }

        .providericon svg {
            filter: grayscale(.7);
            opacity: .65;
            transition: all .5s;
            cursor: pointer;
        }

        .providericon svg:hover,
        .providericon svg.providericon-active {
            opacity: 1;
            filter: grayscale(0);
        }
    </style>

</head>

<body class="din-font">

<header id="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 position-relative">
                <div
                    class="d-flex flex-column flex-wrap justify-content-center align-items-center align-content-center position-relative">

                    <div class="mb-1 bm-lg-3 pt-5 text-center-mobile">
                        <img alt="" class="main-logo img-fluid" src="./img/rich/mainlogo@2x.png">
                    </div>

                    <div class="mt-4 dark-yellow-text bebas-font may11 text-uppercase lax"
                         data-lax-preset="blurInOut-30">
                        {{ $landerLines['date'] }}
                    </div>

                    <div class="light-yellow-text course-start lax din-con-font"
                         data-lax-preset="blurInOut-30">
                        стартует курс
                    </div>

                    <div class="lax text-center-mobile"
                         data-lax-preset="blurInOut-30">
                        <img alt="" class="wealth-text img-fluid" src="./img/rich/wealth@2x.png">
                    </div>

                    <div class="sub-logo mt-4 mb-5 lax  position-relative"
                         data-lax-preset="blurOut-10">
                        <svg data-name="Layer 1" id="Layer_1" viewBox="0 0 248.78 217"
                             xmlns="http://www.w3.org/2000/svg">
                            <defs>
                                <style>.cls-1 {
                                        fill: #a3810b;
                                    }</style>
                            </defs>
                            <path class="cls-1"
                                  d="M86.78,40.17a25.18,25.18,0,0,0,37.71,21.75,24.45,24.45,0,0,0,9.14-9.14,25.44,25.44,0,0,0,0-25.2,25.09,25.09,0,0,0-21.75-12.51,24.38,24.38,0,0,0-12.59,3.38,25.18,25.18,0,0,0-9.13,9.12l1.39.82-1.39-.82A24.56,24.56,0,0,0,86.78,40.17Zm12.35,0a12.8,12.8,0,0,1,12.78-12.78,12.11,12.11,0,0,1,9,3.75,12.27,12.27,0,0,1,3.73,9,12.74,12.74,0,0,1-12.72,12.72,12.29,12.29,0,0,1-9-3.73A12.11,12.11,0,0,1,99.13,40.2Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M196.17,65a5.8,5.8,0,0,0,2,.35A6.14,6.14,0,0,0,204,61.16l.54-1.67h15.59l.54,1.64a6.13,6.13,0,0,0,2.27,3,6,6,0,0,0,3.58,1.16,6.11,6.11,0,0,0,2.83-.7,5.9,5.9,0,0,0,2.2-1.92,5.78,5.78,0,0,0,1.16-3.48,6.71,6.71,0,0,0-.34-2L219.72,19.26A6.18,6.18,0,0,0,213.87,15h-3a6,6,0,0,0-3.63,1.17,6.29,6.29,0,0,0-2.22,3l-12.78,38a6.2,6.2,0,0,0-.3,1.88A6.34,6.34,0,0,0,196.17,65Zm12.52-17.89,3.66-10.9L216,47.07Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M242.55,65.33a6.18,6.18,0,0,0,6.17-6.21V42.64l9.74,19.26a5.82,5.82,0,0,0,2.22,2.5,6.17,6.17,0,0,0,3.26.93h2.34a6.16,6.16,0,0,0,6.13-6.21V21.28a6.06,6.06,0,0,0-1.8-4.4,6.18,6.18,0,0,0-10.54,4.4V37.73l-9.74-19.31h0A5.58,5.58,0,0,0,249,16.62a6.3,6.3,0,0,0-1.94-1.2,6,6,0,0,0-2.19-.42h-2.27a6.18,6.18,0,0,0-6.21,6.21V59.12a6.18,6.18,0,0,0,6.18,6.21Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M24.57,23.16l12.7,37.91h0a5.67,5.67,0,0,0,2.19,3.1,6.15,6.15,0,0,0,3.65,1.16h2.68a6.11,6.11,0,0,0,3.8-1.3,6.2,6.2,0,0,0,2.21-3.32l4.62-18.5L61,60.61A5.86,5.86,0,0,0,63.15,64,6.12,6.12,0,0,0,67,65.33h2.68a6.18,6.18,0,0,0,3.64-1.16A5.71,5.71,0,0,0,75.5,61.1L88.26,23.16a6.21,6.21,0,0,0,.35-2,6.17,6.17,0,0,0-9.77-5l.93,1.31-.94-1.31a6.18,6.18,0,0,0-2.26,3L69.05,41.61,63.64,19.73a6.17,6.17,0,0,0-1.25-2.46,6.24,6.24,0,0,0-2.12-1.66A6.06,6.06,0,0,0,57.62,15H55.35a6.07,6.07,0,0,0-3.8,1.3,5.81,5.81,0,0,0-2.16,3.39l-5.58,22L36.34,19.26a6.05,6.05,0,0,0-1.82-2.74A6.25,6.25,0,0,0,30.46,15a6.82,6.82,0,0,0-1.87.27,6.14,6.14,0,0,0-2.8,1.84,6.34,6.34,0,0,0-1.38,2.81,7,7,0,0,0-.13,1.3,6.57,6.57,0,0,0,.27,1.88Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M146.8,65.33A6.16,6.16,0,0,0,153,59.12V50.4l4.18,10.94a6,6,0,0,0,2.22,2.87,6.12,6.12,0,0,0,3.54,1.12H166a6.07,6.07,0,0,0,3.53-1.12,6.52,6.52,0,0,0,2.28-2.81l0-.06,4.1-10.87v8.65a6.18,6.18,0,1,0,12.35,0V21.21a6.13,6.13,0,0,0-1.81-4.4A6,6,0,0,0,182.14,15h-1.72a6,6,0,0,0-3.53,1.12,6.54,6.54,0,0,0-2.28,2.82l0,.06L164.48,45.78,154.31,19l0,0A6.46,6.46,0,0,0,152,16.12,6,6,0,0,0,148.55,15h-1.71a6.18,6.18,0,0,0-6.21,6.21V59.12a6.16,6.16,0,0,0,6.17,6.21Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M159.62,169a7.41,7.41,0,0,0-7.39,7.44v48.09a7.39,7.39,0,0,0,7.39,7.44,7.14,7.14,0,0,0,5.23-2.17,7.22,7.22,0,0,0,2.17-5.27V176.47a7.41,7.41,0,0,0-7.4-7.44Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M38,169a7.41,7.41,0,0,0-7.39,7.44v48.09A7.39,7.39,0,0,0,38,232a7.41,7.41,0,0,0,7.4-7.44V176.47A7.41,7.41,0,0,0,38,169Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M270.9,181.65A7.4,7.4,0,0,0,265.71,169H240a7.57,7.57,0,0,0-7.45,7.44v48.09A7.48,7.48,0,0,0,240,232h25.74a7.39,7.39,0,0,0,7.35-7.39,7.23,7.23,0,0,0-2.16-5.23,7.14,7.14,0,0,0-5.19-2.17H247.32v-9.3h10.89a7.28,7.28,0,0,0,5.27-2.16,7.4,7.4,0,0,0-5.27-12.63H247.32v-9.3h18.39A7.14,7.14,0,0,0,270.9,181.65Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M132.18,195.19l-12.11-3.66-.46,1.53.41-1.54a6.14,6.14,0,0,1-2.93-1.63,2.78,2.78,0,0,1-1-2,2.54,2.54,0,0,1,.41-1.35,4.36,4.36,0,0,1,1.31-1.29,9,9,0,0,1,2.24-1,8.71,8.71,0,0,1,2.65-.41h2.62a8.83,8.83,0,0,1,3.59.74,6,6,0,0,1,2.38,1.77,7.4,7.4,0,0,0,6,3.17,7,7,0,0,0,6.56-4,7.33,7.33,0,0,0,.89-3.46,7.14,7.14,0,0,0-1.39-4.16,20.1,20.1,0,0,0-7.66-6.49,23,23,0,0,0-10.4-2.4h-2.62a22.23,22.23,0,0,0-15.06,5.46,17.26,17.26,0,0,0-6.34,13.42,17,17,0,0,0,4,11,21,21,0,0,0,10.46,6.85L128,209.42a6.25,6.25,0,0,1,3,1.63,2.81,2.81,0,0,1,1,2h0a2.64,2.64,0,0,1-1,2,7.74,7.74,0,0,1-5.42,2.14h-2.86a8.64,8.64,0,0,1-3.59-.74,5.85,5.85,0,0,1-2.39-1.77,7.33,7.33,0,0,0-5.94-3.16,7.1,7.1,0,0,0-6.6,3.94,7.37,7.37,0,0,0-.94,3.5,7.22,7.22,0,0,0,1.39,4.14,20.14,20.14,0,0,0,7.67,6.49,23,23,0,0,0,10.4,2.39h2.94a22,22,0,0,0,15.7-6.28,16.92,16.92,0,0,0,5.37-12.58V213a17.25,17.25,0,0,0-4-11A20.51,20.51,0,0,0,132.18,195.19Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M88.41,169a7.2,7.2,0,0,0-5.23,2.17A7.3,7.3,0,0,0,81,176.47v22.8L67.79,173.05h0a6.61,6.61,0,0,0-1.62-2.17,7.62,7.62,0,0,0-2.31-1.43,6.87,6.87,0,0,0-2.63-.5H58.35a7.21,7.21,0,0,0-5.28,2.16,7.31,7.31,0,0,0-2.17,5.27v48.18a7.26,7.26,0,0,0,2.17,5.27,7.39,7.39,0,0,0,10.46,0,7.25,7.25,0,0,0,2.16-5.27V201.74L78.92,227.9a7,7,0,0,0,2.67,3,7.24,7.24,0,0,0,3.9,1.11h3a7.15,7.15,0,0,0,5.18-2.16,7.34,7.34,0,0,0,2.17-5.28V176.47a7.31,7.31,0,0,0-2.17-5.27A7.24,7.24,0,0,0,88.41,169Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M222.34,184.24a31.12,31.12,0,0,0-11.67-11.17,32,32,0,0,0-15.74-4H180a7.55,7.55,0,0,0-7.44,7.44v48.09a7.18,7.18,0,0,0,2.22,5.29A7.29,7.29,0,0,0,180,232h15.44a31.59,31.59,0,0,0,8.28-1.1,32.07,32.07,0,0,0,7.67-3.22,30.61,30.61,0,0,0,6.58-5.15,32.76,32.76,0,0,0,5-6.7,30.72,30.72,0,0,0,3.08-7.78,28.9,28.9,0,0,0,.85-7.06c0-.44,0-.86,0-1.25A30.53,30.53,0,0,0,222.34,184.24ZM212.1,200.52a16.66,16.66,0,0,1-16.65,16.69h-8.09V183.82h8.09a16.66,16.66,0,0,1,16.65,16.7Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M41.15,151.92H73A7,7,0,1,0,73,138H48.06V87.22a6.78,6.78,0,0,0-2.05-5,7,7,0,0,0-11.88,5v57.69a7,7,0,0,0,7,7Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M41.15,151.92v1.37H73a8.18,8.18,0,0,0,5.94-2.44A8.33,8.33,0,0,0,73,136.63H49.42V87.22a8.33,8.33,0,0,0-8.33-8.38,8,8,0,0,0-5.88,2.44,8.15,8.15,0,0,0-2.45,5.94v57.69a8.35,8.35,0,0,0,8.39,8.38v-2.74a5.6,5.6,0,0,1-5.65-5.64V87.22a5.35,5.35,0,0,1,1.64-4,5.59,5.59,0,0,1,7.91,0,5.42,5.42,0,0,1,1.64,4v52.14H73A5.42,5.42,0,0,1,77,141a5.59,5.59,0,0,1-4,9.54H41.15Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M152.07,98.06a35.76,35.76,0,0,0-31-17.85,34.87,34.87,0,0,0-18,4.81,35.89,35.89,0,0,0-13,13,35.95,35.95,0,0,0,0,36,36,36,0,0,0,49,13,34.92,34.92,0,0,0,13-13,36.34,36.34,0,0,0,0-36ZM136.52,131.5a21.92,21.92,0,0,1-30.94,0,21.81,21.81,0,0,1,0-30.93,21.87,21.87,0,0,1,30.94,30.93Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M152.07,98.06l1.18-.69A37.14,37.14,0,0,0,121,78.84a36.2,36.2,0,0,0-18.64,5A37.21,37.21,0,0,0,88.86,97.36a37.31,37.31,0,0,0,0,37.35h0a37.43,37.43,0,0,0,50.86,13.53,36.21,36.21,0,0,0,13.54-13.54,37.67,37.67,0,0,0,0-37.33h0l-1.18.69-1.19.68a34.5,34.5,0,0,1-12.53,47.13A34.26,34.26,0,0,1,121,150.45a34.22,34.22,0,0,1-29.8-17.12h0a34.6,34.6,0,0,1,0-34.58A34.53,34.53,0,0,1,103.76,86.2a34.63,34.63,0,0,1,34.58,0,34,34,0,0,1,12.54,12.53h0ZM136.52,131.5l-1-1a20.56,20.56,0,0,1-29,0,20.41,20.41,0,0,1,0-29,20.49,20.49,0,0,1,35,14.52,19.68,19.68,0,0,1-6,14.48l1,1,1,1a23.3,23.3,0,0,0,0-32.86,23.27,23.27,0,0,0-39.71,16.45,22.33,22.33,0,0,0,6.84,16.42,23.3,23.3,0,0,0,32.87,0Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M209.28,80.1a6.7,6.7,0,0,0-4.08,1.31,6.87,6.87,0,0,0-2.51,3.51l-15.19,45-15-45a7,7,0,0,0-6.59-4.82A6.64,6.64,0,0,0,160.28,83a6.86,6.86,0,0,0-1,6.33l19.26,57.8a6.75,6.75,0,0,0,2.62,3.5,7,7,0,0,0,4.08,1.31h4.61a7,7,0,0,0,4.13-1.31,6.88,6.88,0,0,0,2.57-3.5l19.37-57.8a7,7,0,0,0-6.6-9.21Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M209.28,80.1V78.73a8.1,8.1,0,0,0-4.89,1.58h0a8.26,8.26,0,0,0-3,4.18l-13.88,41.13L173.83,84.49h0a8.34,8.34,0,0,0-7.89-5.76,8,8,0,0,0-6.76,3.45,8.15,8.15,0,0,0-1.66,4.89,9,9,0,0,0,.41,2.64v0l19.27,57.81a8.19,8.19,0,0,0,3.14,4.2h0a8.39,8.39,0,0,0,4.86,1.56h4.61a8.34,8.34,0,0,0,4.93-1.57,8.23,8.23,0,0,0,3.07-4.18h0l19.37-57.79h0a8.7,8.7,0,0,0,.48-2.82A7.91,7.91,0,0,0,216,82.18h0a8,8,0,0,0-6.76-3.45v2.74a5.24,5.24,0,0,1,4.55,2.32h0a5.17,5.17,0,0,1,1.08,3.14,6.14,6.14,0,0,1-.33,1.95l-19.37,57.79h0a5.49,5.49,0,0,1-2.06,2.83,5.55,5.55,0,0,1-3.34,1.05H185.2a5.63,5.63,0,0,1-3.3-1.06h0a5.36,5.36,0,0,1-2.1-2.82L160.54,88.88l-1.3.43,1.31-.39a6.33,6.33,0,0,1-.29-1.85,5.41,5.41,0,0,1,1.13-3.28,5.24,5.24,0,0,1,4.55-2.32,5.62,5.62,0,0,1,5.29,3.88h0l16.26,48.89L204,85.36h0a5.59,5.59,0,0,1,2-2.83h0a5.34,5.34,0,0,1,3.27-1Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M233.67,151.92h30.89a6.64,6.64,0,0,0,4.86-2,7,7,0,0,0,0-9.84,6.64,6.64,0,0,0-4.86-2h-24V123h15a7,7,0,1,0,0-13.92h-15v-15h24a6.64,6.64,0,0,0,4.86-2,7,7,0,0,0,0-9.84,6.64,6.64,0,0,0-4.86-2H233.67a7.14,7.14,0,0,0-7,7v57.69a6.66,6.66,0,0,0,2.09,5A6.84,6.84,0,0,0,233.67,151.92Z"
                                  transform="translate(-24.28 -15)"/>
                            <path class="cls-1"
                                  d="M233.67,151.92v1.37h30.89a8.33,8.33,0,0,0,0-16.66H242V124.39h13.6a8.32,8.32,0,0,0,5.94-14.21h0a8.15,8.15,0,0,0-5.94-2.45H242V95.5h22.61a8.33,8.33,0,0,0,0-16.66H233.67a8.09,8.09,0,0,0-5.89,2.49h0a8.11,8.11,0,0,0-2.49,5.89v57.69a8,8,0,0,0,2.5,6,8.24,8.24,0,0,0,5.88,2.43v-2.74a5.4,5.4,0,0,1-4-1.65,5.28,5.28,0,0,1-1.68-4V87.22a5.33,5.33,0,0,1,1.69-4h0a5.33,5.33,0,0,1,3.95-1.69h30.89a5.25,5.25,0,0,1,3.9,1.64,5.58,5.58,0,0,1,0,7.9,5.25,5.25,0,0,1-3.9,1.64H239.21v17.71h16.34a5.37,5.37,0,0,1,4,1.64h0a5.59,5.59,0,0,1,0,7.91,5.45,5.45,0,0,1-4,1.64H239.21v17.7h25.35a5.62,5.62,0,0,1,5.54,5.6,5.35,5.35,0,0,1-1.64,3.95,5.25,5.25,0,0,1-3.9,1.64H233.67Z"
                                  transform="translate(-24.28 -15)"/>
                        </svg>
                    </div>

                    <div class="lax mt-5 pb-5 register-button position-relative"
                         data-lax-preset="eager-70">
                        <a class="btn btn-block btn-gold pt-2 pl-2 pr-5 pl-5 button-shadow fz24 hvr-fade hvr-bob"
                           href="{{ $landerLines['link'] }}">
                            зарегистрироваться
                        </a>
                    </div>

                    <img alt="" class="coins-mobile hide coins-left img-fluid lax" data-lax-preset="eager-350"
                         data-speed="0.02" src="./img/rich/coins@2x.png">

                    <img alt="" class="coins coins-left img-fluid lax" data-lax-preset="eager-350"
                         data-speed="0.02" src="./img/rich/coins-left-full@2x.png">
                    <img alt="" class="coins coins-right img-fluid lax" data-lax-preset="eager-450"
                         data-speed="0.02" src="./img/rich/coins-right-full@2x.png">

                    <div class="gradientback">
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>

<section id="course-info">
    <div class="container">
        <div class="row">
            <div class="col-12 pt-lg-3 pb-lg-3 pl-lg-5 pr-lg-5" data-aos="fade-in" data-aos-delay="50">
                {!! $landerText['description-short'] !!}
                <div class="show-full-text hide">
                    {!! $landerText['description-extension'] !!}
                </div>
                <p data-aos="fade-in" data-aos-delay="150">
                    <a class="more-link" href="{{ $landerLines['link'] }}" id="full-text">
                        подробнее
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>

<section id="course-gallery">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center lax" data-lax-preset="blurInOut-3">
                <h3 class="din-con-font fz50">
                    По окончанию курса вы <br>
                    будете знать как получить
                </h3>
            </div>
        </div>
    </div>
    <div class="pt-lg-5 pb-3 pt-3  ">
        <div class="swiper-container" id="swiper-container-goals">
            <div class="swiper-wrapper">
                <div class="swiper-slide pb-3">
                    <img alt="" class="img-fluid" src="./img/rich/Layer-5.jpg">
                </div>
                <div class="swiper-slide pb-3">
                    <img alt="" class="img-fluid" src="./img/rich/Layer-2.jpg">
                </div>
                <div class="swiper-slide pb-3">
                    <img alt="" class="img-fluid" src="./img/rich/Layer-4.jpg">
                </div>
                <div class="swiper-slide pb-3">
                    <img alt="" class="img-fluid" src="./img/rich/Layer-1.jpg">
                </div>
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-scrollbar"></div>
        </div>
    </div>
</section>

<section id="course-numbers">
    <div class="container pt-0 pt-lg-5">
        <div class="row">
            <div class="col-12 pt-lg-5 mt-5 mb-4 lax" data-lax-preset="blurInOut-3">
                <h2 class="bebas-font text-center text-uppercase fz70">
                    Курс “Богатсво”
                </h2>
            </div>
        </div>
        <div class="row">

            @php $odd=true @endphp
            @foreach($landerIcon as $OneLanderIcon)
                @if($odd)
                    <div class="col-12 col-lg-4 col-xxl-3 offset-xxl-3 offset-lg-2 mb-4">
                        @else
                            <div class="col-12 col-lg-4 col-xxl-3 offset-xxl-3 mb-4">
                                @endif
                                <div
                                    class="d-flex flex-row justify-content-center align-items-center align-content-center lax icon-block"
                                    data-lax-preset="blurInOut-5">
                                    <div class="col-2 offset-0  text-right  d-flex justify-content-center">
                                        {{--                            {{ $OneLanderIcon->icon }}--}}
                                        <img src="{{ url('img/rich/'.$OneLanderIcon->icon)}}" class="icon-img"
                                             alt="">
                                    </div>
                                    <div class="col-8 bebas-font fz20 pl-2 decr">
                                        {{ $OneLanderIcon->text }}
                                    </div>
                                </div>
                            </div>
                            @php $odd=!$odd @endphp
                            @endforeach
                    </div>
        </div>
</section>

<section id="course-if">
    <div class="container">
        <div class="col-12 mt-lg-5 mt-4 mb-4 lax" data-lax-preset="blurInOut-3">
            <h2 class="bebas-font text-center text-uppercase fz70">
                Регистрируйся если:
            </h2>
        </div>
        <div class="row">
            <div class="col-sm-8 offset-sm-2 col-12 pb-lg-5">
                <div class="swiper-container pb-3" id="swiper-container-if">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide pb-3">
                            <div
                                class="row pl-5 pr-5 d-flex flex-row justify-content-center align-items-center height-100">
                                <div class="col-5 text-right">
                                    <img alt="" class="img-fluid coins-if" src="./img/rich/slide1-if@2x.png">
                                </div>
                                <div class="col-7">
                                    устала экономить <br>
                                    на всем подряд
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide pb-3">
                            <div
                                class="row pl-5 pr-5 d-flex flex-row justify-content-center align-items-center height-100">
                                <div class="col-5 text-right">
                                    <img alt="" class="img-fluid coins-if" src="./img/rich/slide2-if@2x.png">
                                </div>
                                <div class="col-7">
                                    хочешь покупать, то что<br>
                                    хочешь, а не то что можешь<br>
                                    себе позволить
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide pb-3">
                            <div
                                class="row pl-5 pr-5 d-flex flex-row justify-content-center align-items-center height-100">
                                <div class="col-5 text-right">
                                    <img alt="" class="img-fluid coins-if" src="./img/rich/slide3-if@2x.png">
                                </div>
                                <div class="col-7">
                                    деньги утекают<br>
                                    сквозь пальцы
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide pb-3">
                            <div
                                class="row pl-5 pr-5 d-flex flex-row justify-content-center align-items-center height-100">
                                <div class="col-5 text-right">
                                    <img alt="" class="img-fluid coins-if" src="./img/rich/slide4-if@2x.png">
                                </div>
                                <div class="col-7">
                                    у тебя много<br>
                                    долгов и кредитов
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide pb-3">
                            <div
                                class="row pl-5 pr-5 d-flex flex-row justify-content-center align-items-center height-100">
                                <div class="col-5 text-right">
                                    <img alt="" class="img-fluid coins-if" src="./img/rich/slide5-if@2x.png">
                                </div>
                                <div class="col-7">
                                    никогда не<br>
                                    чувствовала легкости
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="course-price">
    <div class="container mt-lg-5 pb-5">
        <div class="row">
            <div class="col-12 pb-5 position-relative">
                <h2 class="bebas-font text-center text-uppercase lax fz70" data-lax-preset="blurInOut-3">
                    Стоимость
                </h2>
                <img alt="" class="img-fluid diam position-absolute" src="./img/rich/diamond2.gif">
            </div>
        </div>
        <div class="row">
            <div class="col-12 mt-5 pt-5 pb-5 p-lg-5">
                <div class="price-block width-price position-relative">

                    <div class="all-prices pt-5 pl-5 pr-5">
                        <div class="line-first">
                            <div class="line-first-text text-center">полная цена</div>
                            <div class="line-first-sum bebas-font text-center position-relative">
                                {{--                                10000--}}
                                {{ $landerLines['full-price'] }}
                                <div class="line-through text-center"></div>
                            </div>
                        </div>
                        <div class="line-first mt-3 mb-3 " id="action-price">
                            <div class="line-first-text text-center">акционная цена</div>
                            <div class="line-first-sum bebas-font text-center position-relative final-price">
                            {{--                                3500--}}
                            {{ $landerLines['action-price'] }}
                            <!--                                <div class="line-through text-center"></div>-->
                            </div>
                        </div>
                        <div class="line-first" id="offer-price">
                            <div class="line-first-text text-center">
                                специальное предложение, <br> цена повышается <br> на 100 рублей через:<br>
                                <b><span id="countdown"></span></b>
                            </div>
                            <div class="line-first-sum bebas-font text-center final-price position-relative">
                                <span id="total-price">...</span>
                            </div>
                        </div>
                    </div>
                    <div class="button-row">
                        <div class="lax register-button position-relative text-center "
                             data-lax-preset="blurInOut-3">
                            <a class="btn  btn-gold pt-2 pl-2 pr-5 pl-5 button-shadow fz24 hvr-fade hvr-bob"
                               href="{{ $landerLines['link'] }}">
                                зарегистрироваться
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>


<section id="comments">
    <div class="container pb-5">
        <div class="col-12 mt-lg-5 mt-4 mb-4 lax" data-lax-preset="blurInOut-3">
            <h2 class="bebas-font text-center text-uppercase fz70">
                Отзывы
            </h2>
        </div>

        @include('blocks.comments',['page'=>'rich'])

    </div>
</section>

<footer>
    <div class="pl-5 pr-5 pt-3 pb-3 container">
        <div class="row d-flex flex-row align-items-center text-center-mobile justify-content-center">
            <div class="col-12 col-sm-4 offset-sm-2 pl-lg-5 p-3 footer-text bebas-font">
                Курс Богатство
            </div>
            <div class="col-12 col-sm-4 pr-sm-5 p-3 text-center">
                <a class="footer-link d-inline-block" href="https://www.instagram.com/woman_love_inside/"
                   target="_blank">
                    <svg class="img-fluid ig-icon" data-name="Capa 1" id="Capa_2" viewBox="0 0 513 513"
                         xmlns="http://www.w3.org/2000/svg">
                        <defs>
                            <style>.cls-2 {
                                    fill: #fff;
                                    stroke: #000;
                                    stroke-miterlimit: 10;
                                }</style>
                        </defs>
                        <path class="cls-2"
                              d="M352,0H160A160,160,0,0,0,0,160V352A160,160,0,0,0,160,512H352A160,160,0,0,0,512,352V160A160,160,0,0,0,352,0ZM464,352A112.12,112.12,0,0,1,352,464H160A112.12,112.12,0,0,1,48,352V160A112.12,112.12,0,0,1,160,48H352A112.12,112.12,0,0,1,464,160Z"
                              transform="translate(0.5 0.5)"/>
                        <path class="cls-2"
                              d="M256,128A128,128,0,1,0,384,256,128,128,0,0,0,256,128Zm0,208a80,80,0,1,1,80-80A80.11,80.11,0,0,1,256,336Z"
                              transform="translate(0.5 0.5)"/>
                        <circle class="cls-1" cx="394.1" cy="118.9" r="17.06"/>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</footer>


<!-- inject:js -->

<script src="{{asset('js/all/jquery.min.js') }}"></script>
<script src="{{asset('js/all/swiper.min.js') }}"></script>
<script src="{{asset('js/all/popper.min.js') }}"></script>
<script src="{{asset('js/all/bootstrap.min.js') }}"></script>

<script src="./js/rich/scripts.js"></script>
<script src="{{asset('js/all/scripts.js') }}"></script>
{{--<script src="//ulogin.ru/js/ulogin.js"></script>--}}

<script>
    // function getForm(token) {
    //     $.getJSON("//ulogin.ru/token.php?host=" + encodeURIComponent(location.toString()) + "&token=" + token + "&callback=?", function (data) {
    //         data = $.parseJSON(data.toString());
    //         console.log(data);
    //         if ( !data.error ) {
    //             //alert("Привет, "+data.first_name+" "+data.last_name+"!");
    //
    //             $("#link").attr("src", data.identity);
    //             $("#photo-Img").attr("src", data.photo_big);
    //             $("#photo").attr("value", data.photo_big);
    //             $("#network").attr("value", data.network);
    //             $("#profile").attr("value", data.profile);
    //
    //             $("#name").attr("value", data.first_name + " " + data.last_name);
    //             $("#commentName").html(data.first_name + " " + data.last_name);
    //
    //             //$("#commentForm").slideDown();
    //         }
    //     });
    // }



    $("#vkontakte-button").on("click", function (e) {
        e.preventDefault();
        console.log("clicked on vkontakte...")
        window.open('{{ route('auth.login',['auth'=>'vkontakte'])  }}', "Checking Auth", "height=380,width=650");

        return false;
    })
    $("#facebook-button").on("click", function (e) {
        e.preventDefault();
        console.log("clicked on facebook...")
        window.open('{{ route('auth.login',['auth'=>'facebook'])  }}', "Checking Auth", "height=600,width=800");

        return false;
    })




    $(function () {



//Countdown
        //var countDownDate = new Date("June 30, 2020 00:00:00").getTime();
        var countDownDate = new Date('{{  date("F d, Y 00:00:00", strtotime($landerLines['action-date']))  }}').getTime();
        console.log("DB" + countDownDate)

        //today countdowm
        var todayCounter = new Date();
        todayCounter.setHours(23, 59, 59, 0);
        console.log(todayCounter);
        if ( todayCounter < countDownDate ) {
            var total = $("#total-price")
            var x = setInterval(function () {
                var now = new Date().getTime();
                var distance = todayCounter - now;
                var response = distanceParser(distance);

                document.getElementById("countdown").innerHTML =
                    response["hours"] + "ч.:" + response["minutes"] + "м.:" + response["seconds"] + "c ";

                if ( distance < 0 ) {
                    clearInterval(x);
                    $("#countdown, #action-price").hide()
                    // $("#total-price").html("10000")
                    //document.getElementById("countdown").innerHTML = "EXPIRED";
                }
            }, 1000)
        } else {
            $("#countdown, #offer-price").hide()
            $("#action-price .line-through").hide()
        }


        //today amount
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var responseDist = distanceParser(distance);
        var d = 0;
        console.log(responseDist);
        if ( responseDist["days"] > 0 ) {
            //if ( responseDist["days"] <= 4 ) {
            d = (5 - responseDist["days"] - 1) * 100;
            d += {{ $landerLines['today-price'] }}
            // } else {
            //     d = {{ $landerLines['today-price'] }}
            // }
        } else if ( responseDist["days"] == 0 ) {
            d = {{ $landerLines['action-price'] }}
        } else {
            d = {{ $landerLines['action-price'] }}
        }

        total.html(d)


    })
</script>


<!-- endinject -->
<!--[if lt IE 9]>
<p class="browserupgrade">
    You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
    browser</a> to improve your experience.
</p>
<![endif]-->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(55714759, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/55714759" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
