@if($landerComments->count()>0)
    <div class="row">
        <div class="col-lg-6 border-right-custom">
            <div class="all-stars h-100">
                <div
                    class="d-flex h-100 justify-content-center align-content-center align-items-center flex-wrap flex-lg-nowrap p-3 p-lg-5 ">
                    {{--                                <div class="d-flex  justify-content-center align-items-center">--}}

                    @if ($landerCommentsByDate->count()>0)
                        <div class="all-stars-rating mr-4">
                            {{ round($landerCommentsSum/$landerComments->count(),1)  }}
                        </div>
                        <div class="all-stars-stars">
                            @for( $i=1; $i<=5; $i++ )
                                @if(round($landerCommentsSum/$landerComments->count())>=$i)
                                    <img src="{{asset('img/cms/star-full@2x.png')}}"
                                         title="{{round($landerCommentsSum/$landerComments->count(),1) }}"
                                         class="comment-star comment-star-full m-1" alt="">
                                @else
                                    <img src="{{asset('img/cms/star-empty@2x.png')}}"
                                         title="round($landerCommentsSum/$landerComments->count(),1) "
                                         class="comment-star comment-star-empty m-1" alt="">@endif
                            @endfor
                        </div>
                    @endif

                    {{--                                </div>--}}
                </div>
            </div>
        </div>
        <div class="col-lg-6">


            @foreach($ratings as $key=>$value)
                <div
                    class="row all-rating pl-lg-5 pr-lg-5 pb-lg-1 pt-lg-0 pt-2 pb-2 pl-4 pr-4 d-flex justify-content-center align-items-center">
                    <div class="col-5 col-lg-4 text-center">

                        @for( $i=1; $i<=5; $i++ )
                            @if($key>=$i)
                                <img src="{{asset('img/cms/star-full@2x.png')}}"
                                     class="all-rating-star comment-star-full" alt="">
                            @else
                                <img src="{{asset('img/cms/star-empty@2x.png')}}"
                                     class="all-rating-star comment-star-empty" alt="">
                            @endif
                        @endfor

                    </div>
                    <div class="col-5 col-lg-6">
                        <div class="all-rating-progress-bar">
                        <span class="all-rating-progress-line h-100"
                              style="width:{{  ($value/$landerComments->count())*100 }}%"
                        ></span>
                        </div>
                    </div>
                    <div class="col-2 all-rating-total">{{$value}}</div>
                </div>
            @endforeach

        </div>
    </div>
@endif

<div class="row">
    <div class="col-12">
        <div class="leave-comment-txt text-center pt-3 mt-4 mr-5 ml-5 mb-0">
            оставьте отзыв нажимая на иконку:
        </div>
    </div>


    <div class="col-12">
        <div style="display: none" id="commentForm">

            <form action="{{route('lander.add.comment')}}" method="post">
                @csrf
                {{--                                <input type="hidden" name="name" id="name" value="">--}}
                {{--                                <input type="hidden" name="network" id="network" value="">--}}
                {{--                                <input type="hidden" name="profile" id="profile" value="">--}}
                {{--                                <input type="hidden" name="photo" id="photo" value="">--}}
                {{--                                <input type="hidden" name="link" id="link" value="">--}}
                {{--                                <input type="hidden" name="page" id="page" value="wakeup">--}}
                <input type="hidden" name="page" id="page" value="{{$page}}">
                <input type="hidden" name="rating" id="rating" value="5">
                <input type="hidden" name="token" id="token" value="">


                <div class="col-12 col-md-8  offset-md-2  p-3">
                    <div class="row">

                        <div class="col-lg-2 offset-lg-1 text-center">
                            <img src="" id="new-comment-photo"
                                 class="img-fluid comment-img" alt="">
                            <div class="comment-log text-center m-auto">
                                {{--                                                    <svg enable-background="new 0 0 50 50" id="Layer_3" version="1.1"--}}
                                {{--                                                         viewBox="0 0 50 50" xml:space="preserve"--}}
                                {{--                                                         xmlns="http://www.w3.org/2000/svg"--}}
                                {{--                                                         xmlns:xlink="http://www.w3.org/1999/xlink"><path--}}
                                {{--                                                            d="M26,34c1,0,1-1.4,1-2c0-1,1-2,2-2s2.7,1.7,4,3c1,1,1,1,2,1s3,0,3,0s2-0.1,2-2c0-0.6-0.7-1.7-3-4  c-2-2-3-1,0-5c1.8-2.5,3.2-4.7,3-5.3c-0.2-0.6-5.3-1.6-6-0.7c-2,3-2.4,3.7-3,5c-1,2-1.1,3-2,3c-0.9,0-1-1.9-1-3c0-3.3,0.5-5.6-1-6  c0,0-2,0-3,0c-1.6,0-3,1-3,1s-1.2,1-1,1c0.3,0,2-0.4,2,1c0,1,0,2,0,2s0,4-1,4c-1,0-3-4-5-7c-0.8-1.2-1-1-2-1c-1.1,0-2,0-3,0  c-1,0-1.1,0.6-1,1c2,5,3.4,8.1,7.2,12.1c3.5,3.6,5.8,3.8,7.8,3.9C25.5,34,25,34,26,34z"--}}
                                {{--                                                            fill="#cccccc" id="VK_1_"/>--}}

                                {{--                                                 </svg>--}}
                            </div>
                        </div>
                        <div class="col-md-9 col-12">
                            {{--                                            <div class="comment-name text-center-mobile">--}}
                            {{--                                                <h5 class="text-center" id="new-comment-name">OLOO </h5>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="rating-block text-center-mobile">--}}
                            {{--                                                @for( $i=1; $i<=5; $i++ )--}}
                            {{--                                                    <img src="{{asset('img/cms/star-full@2x.png')}}"--}}
                            {{--                                                         title="{{$i}}" data-rating="{{$i}}"--}}
                            {{--                                                         class="comment-star comment-star-full comment-star-select"--}}
                            {{--                                                         alt="{{$i}}">--}}
                            {{--                                                @endfor--}}
                            {{--                                            </div>--}}
                            {{--                                            <textarea class="form-control comment-textarea m-3"--}}
                            {{--                                                      name="comment" id="comment"--}}
                            {{--                                                      rows="5"></textarea>--}}
                            {{--                                            <div class="mobile-text-center mt-3">--}}
                            {{--                                                <button type="submit"--}}
                            {{--                                                        class="btn btn-mint pt-2 pl-2 pr-5 pl-5 button-shadow fz24 hvr-fade hvr-bob">--}}
                            {{--                                                    опубликовать--}}
                            {{--                                                </button>--}}
                            {{--                                            </div>--}}
                            <div class="col-12 comment-date">
                                {{date('Y-m-d')}}
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div
                                        class="col-12 comment-name d-flex align-content-center flex-wrap flex-lg-nowrap">
                                        <a href="#" id="new-comment-name">
                                        </a>
                                        <div class="ml-5 all-stars-stars rating-block">
                                            @for( $i=1; $i<=5; $i++ )
                                                <img src="{{asset('img/cms/star-full@2x.png')}}"
                                                     title="{{$i}}" data-rating="{{$i}}"
                                                     class="comment-star comment-star-full comment-star-select"
                                                     alt="{{$i}}">
                                            @endfor
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 comment-text mt-3">
                                                            <textarea class="form-control comment-textarea"
                                                                      name="comment" id="comment" rows="5"></textarea>
                                        <div class=" mt-3">
                                            <button type="submit"
                                                    class="btn btn-block btn-danger btn-custom p-2 ">
                                                опубликовать
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>

                    </div>

                </div>
            </form>

        </div>

        @if (Session::has('success'))
            <div>
                <script>
                    swal("Спасибо", '{!! Session::get('success') !!}', "success");
                </script>
            </div>
        @endif

        @if($errors->any())
            {{--                   PLS  DON`T KILL ME --}}
            {{--                    <div class="col-12">--}}
            {{--                        <div class="box-body">--}}
            {{--                            <div class="alert alert-danger alert-dismissible">--}}
            {{--                               --}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            <script>
                swal("Ошибка", '{!! implode('', $errors->all('<div>:message</div>')) !!}', "error");
            </script>
        @endif
    </div>


    <div class="col-12">
        <div
            class="leave-comment-icons-out text-center mt-3 mr-5 ml-5 mb-2 pb-4 text-center d-flex justify-content-center">
            <div id="facebook-button" class="m-2 providericon facebook"
                 style="width: 50px; height: auto">
                {{--                                <?xml version="1.0" ?><!DOCTYPE svg  PUBLIC '-//W3C//DTD SVG 1.1//EN'  'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>--}}
                <svg enable-background="new 0 0 50 50" id="Layer_2" version="1.1"
                     viewBox="0 0 50 50" xml:space="preserve"
                     xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink"><path
                        d="M30,26l1-5l-5,0v-4c0-1.5,0.8-2,3-2h2v-5c0,0-2,0-4,0c-4.1,0-7,2.4-7,7v4h-5v5h5v14h6V26H30z"
                        fill="#3A5BA0" id="f_1_"/>
                    <path
                        d="M25,1C11.7,1,1,11.7,1,25s10.7,24,24,24s24-10.7,24-24S38.3,1,25,1z M25,44C14.5,44,6,35.5,6,25S14.5,6,25,6  s19,8.5,19,19S35.5,44,25,44z"
                        fill="#3A5BA0"/>
                                    </svg>
            </div>
            <div id="vkontakte-button" class="m-2 providericon vkontakte"
                 style="width: 50px; height: auto">
                {{--                                <?xml version="1.0" ?><!DOCTYPE svg  PUBLIC '-//W3C//DTD SVG 1.1//EN'  'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>--}}
                <svg enable-background="new 0 0 50 50" id="Layer_3" version="1.1"
                     viewBox="0 0 50 50" xml:space="preserve"
                     xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink"><path
                        d="M26,34c1,0,1-1.4,1-2c0-1,1-2,2-2s2.7,1.7,4,3c1,1,1,1,2,1s3,0,3,0s2-0.1,2-2c0-0.6-0.7-1.7-3-4  c-2-2-3-1,0-5c1.8-2.5,3.2-4.7,3-5.3c-0.2-0.6-5.3-1.6-6-0.7c-2,3-2.4,3.7-3,5c-1,2-1.1,3-2,3c-0.9,0-1-1.9-1-3c0-3.3,0.5-5.6-1-6  c0,0-2,0-3,0c-1.6,0-3,1-3,1s-1.2,1-1,1c0.3,0,2-0.4,2,1c0,1,0,2,0,2s0,4-1,4c-1,0-3-4-5-7c-0.8-1.2-1-1-2-1c-1.1,0-2,0-3,0  c-1,0-1.1,0.6-1,1c2,5,3.4,8.1,7.2,12.1c3.5,3.6,5.8,3.8,7.8,3.9C25.5,34,25,34,26,34z"
                        fill="#54769B" id="VK_1_"/>
                    <path
                        d="M25,1C11.7,1,1,11.7,1,25s10.7,24,24,24s24-10.7,24-24S38.3,1,25,1z M25,44C14.5,44,6,35.5,6,25S14.5,6,25,6  s19,8.5,19,19S35.5,44,25,44z"
                        fill="#54769B"/>
                                    </svg>
            </div>
        </div>
    </div>
</div>

@if($landerCommentsByDate->count()>0)
    <div class="row">
        <div class="col-12">
            <nav class="pl-5 pr-5">
                <div class="nav nav-tabs nav-fill justify-content-center" id="nav-tab" role="tablist">
                    <a class="nav-link active"
                       id="nav-date-tab"
                       data-toggle="tab"
                       href="#nav-date"
                       role="tab"
                       aria-controls="nav-date-tab"
                       aria-selected="true">по дате</a>
                    <a class="nav-link"
                       id="nav-rating-tab"
                       data-toggle="tab"
                       href="#nav-rating"
                       role="tab"
                       aria-controls="nav-rating-tab"
                       aria-selected="false">по оценке</a>

                </div>
            </nav>
            <div class="tab-content pl-5 pr-5 pt-3 pb-3" id="nav-tabContent">
                <div class="tab-pane fade show active"
                     id="nav-date"
                     role="tabpanel"
                     aria-labelledby="nav-date-tab">


                    @foreach($landerCommentsByDate as $landerCommentByDate)
                        <div class="row mb-3">
                            <div class="col-lg-2 offset-lg-1 text-center">
                                <img src="{{ asset('img/comments/'.$landerCommentByDate->avatar)  }}"
                                     class="img-fluid comment-img" alt="">
                                <div class="comment-log text-center m-auto">
                                    @if($landerCommentByDate->provider=='vkontakte')
                                        <svg enable-background="new 0 0 50 50" id="Layer_3"
                                             version="1.1"
                                             viewBox="0 0 50 50" xml:space="preserve"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" class="mt-1">
                                                            <path
                                                                d="M26,34c1,0,1-1.4,1-2c0-1,1-2,2-2s2.7,1.7,4,3c1,1,1,1,2,1s3,0,3,0s2-0.1,2-2c0-0.6-0.7-1.7-3-4  c-2-2-3-1,0-5c1.8-2.5,3.2-4.7,3-5.3c-0.2-0.6-5.3-1.6-6-0.7c-2,3-2.4,3.7-3,5c-1,2-1.1,3-2,3c-0.9,0-1-1.9-1-3c0-3.3,0.5-5.6-1-6  c0,0-2,0-3,0c-1.6,0-3,1-3,1s-1.2,1-1,1c0.3,0,2-0.4,2,1c0,1,0,2,0,2s0,4-1,4c-1,0-3-4-5-7c-0.8-1.2-1-1-2-1c-1.1,0-2,0-3,0  c-1,0-1.1,0.6-1,1c2,5,3.4,8.1,7.2,12.1c3.5,3.6,5.8,3.8,7.8,3.9C25.5,34,25,34,26,34z"
                                                                fill="#cccccc" id="VK_2_"/>
                                                        </svg>
                                    @elseif($landerCommentByDate->provider=='facebook')
                                        <svg enable-background="new 0 0 50 50" id="Layer_2"
                                             version="1.1"
                                             viewBox="0 0 50 50" xml:space="preserve"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" class="mt-2">
                                                                <path
                                                                    d="M30,26l1-5l-5,0v-4c0-1.5,0.8-2,3-2h2v-5c0,0-2,0-4,0c-4.1,0-7,2.4-7,7v4h-5v5h5v14h6V26H30z"
                                                                    fill="#cccccc" id="f_2_"/>
                                                             </svg>
                                    @elseif($landerCommentByDate->provider=='instagram')
                                        <svg enable-background="new 0 0 50 50"
                                             viewBox="0 0 256 256"
                                             version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             preserveAspectRatio="xMidYMid" width="70%" height="70%"
                                             class="mt-3">
                                            <g>
                                                <path
                                                    d="M127.999746,23.06353 C162.177385,23.06353 166.225393,23.1936027 179.722476,23.8094161 C192.20235,24.3789926 198.979853,26.4642218 203.490736,28.2166477 C209.464938,30.5386501 213.729395,33.3128586 218.208268,37.7917319 C222.687141,42.2706052 225.46135,46.5350617 227.782844,52.5092638 C229.535778,57.0201472 231.621007,63.7976504 232.190584,76.277016 C232.806397,89.7746075 232.93647,93.8226147 232.93647,128.000254 C232.93647,162.177893 232.806397,166.225901 232.190584,179.722984 C231.621007,192.202858 229.535778,198.980361 227.782844,203.491244 C225.46135,209.465446 222.687141,213.729903 218.208268,218.208776 C213.729395,222.687649 209.464938,225.461858 203.490736,227.783352 C198.979853,229.536286 192.20235,231.621516 179.722476,232.191092 C166.227425,232.806905 162.179418,232.936978 127.999746,232.936978 C93.8200742,232.936978 89.772067,232.806905 76.277016,232.191092 C63.7971424,231.621516 57.0196391,229.536286 52.5092638,227.783352 C46.5345536,225.461858 42.2700971,222.687649 37.7912238,218.208776 C33.3123505,213.729903 30.538142,209.465446 28.2166477,203.491244 C26.4637138,198.980361 24.3784845,192.202858 23.808908,179.723492 C23.1930946,166.225901 23.0630219,162.177893 23.0630219,128.000254 C23.0630219,93.8226147 23.1930946,89.7746075 23.808908,76.2775241 C24.3784845,63.7976504 26.4637138,57.0201472 28.2166477,52.5092638 C30.538142,46.5350617 33.3123505,42.2706052 37.7912238,37.7917319 C42.2700971,33.3128586 46.5345536,30.5386501 52.5092638,28.2166477 C57.0196391,26.4642218 63.7971424,24.3789926 76.2765079,23.8094161 C89.7740994,23.1936027 93.8221066,23.06353 127.999746,23.06353 M127.999746,0 C93.2367791,0 88.8783247,0.147348072 75.2257637,0.770274749 C61.601148,1.39218523 52.2968794,3.55566141 44.1546281,6.72008828 C35.7374966,9.99121548 28.5992446,14.3679613 21.4833489,21.483857 C14.3674532,28.5997527 9.99070739,35.7380046 6.71958019,44.1551362 C3.55515331,52.2973875 1.39167714,61.6016561 0.769766653,75.2262718 C0.146839975,88.8783247 0,93.2372872 0,128.000254 C0,162.763221 0.146839975,167.122183 0.769766653,180.774236 C1.39167714,194.398852 3.55515331,203.703121 6.71958019,211.845372 C9.99070739,220.261995 14.3674532,227.400755 21.4833489,234.516651 C28.5992446,241.632547 35.7374966,246.009293 44.1546281,249.28042 C52.2968794,252.444847 61.601148,254.608323 75.2257637,255.230233 C88.8783247,255.85316 93.2367791,256 127.999746,256 C162.762713,256 167.121675,255.85316 180.773728,255.230233 C194.398344,254.608323 203.702613,252.444847 211.844864,249.28042 C220.261995,246.009293 227.400247,241.632547 234.516143,234.516651 C241.632039,227.400755 246.008785,220.262503 249.279912,211.845372 C252.444339,203.703121 254.607815,194.398852 255.229725,180.774236 C255.852652,167.122183 256,162.763221 256,128.000254 C256,93.2372872 255.852652,88.8783247 255.229725,75.2262718 C254.607815,61.6016561 252.444339,52.2973875 249.279912,44.1551362 C246.008785,35.7380046 241.632039,28.5997527 234.516143,21.483857 C227.400247,14.3679613 220.261995,9.99121548 211.844864,6.72008828 C203.702613,3.55566141 194.398344,1.39218523 180.773728,0.770274749 C167.121675,0.147348072 162.762713,0 127.999746,0 Z M127.999746,62.2703115 C91.698262,62.2703115 62.2698034,91.69877 62.2698034,128.000254 C62.2698034,164.301738 91.698262,193.730197 127.999746,193.730197 C164.30123,193.730197 193.729689,164.301738 193.729689,128.000254 C193.729689,91.69877 164.30123,62.2703115 127.999746,62.2703115 Z M127.999746,170.667175 C104.435741,170.667175 85.3328252,151.564259 85.3328252,128.000254 C85.3328252,104.436249 104.435741,85.3333333 127.999746,85.3333333 C151.563751,85.3333333 170.666667,104.436249 170.666667,128.000254 C170.666667,151.564259 151.563751,170.667175 127.999746,170.667175 Z M211.686338,59.6734287 C211.686338,68.1566129 204.809755,75.0337031 196.326571,75.0337031 C187.843387,75.0337031 180.966297,68.1566129 180.966297,59.6734287 C180.966297,51.1902445 187.843387,44.3136624 196.326571,44.3136624 C204.809755,44.3136624 211.686338,51.1902445 211.686338,59.6734287 Z"
                                                    fill="#cccccc"></path>
                                            </g>
                                        </svg>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <div class="col-12 comment-date">
                                    {{$landerCommentByDate->created}}
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div
                                            class="col-12 comment-name d-flex align-content-center flex-wrap flex-lg-nowrap">
                                            <a href="{{$landerCommentByDate->link}}" target="_blank">
                                                {{$landerCommentByDate->name}}
                                            </a>

                                            <div class="ml-5 all-stars-stars">
                                                @for( $i=1; $i<=5; $i++ )
                                                    @if($landerCommentByDate->rating>=$i)
                                                        <img src="{{asset('img/cms/star-full@2x.png')}}"
                                                             class="comment-star comment-star-full"
                                                             alt="">
                                                    @else
                                                        <img
                                                            src="{{asset('img/cms/star-empty@2x.png')}}"
                                                            class="comment-star comment-star-empty"
                                                            alt="">
                                                    @endif

                                                @endfor
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 comment-text mt-3">
                                            {{$landerCommentByDate->comment}}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{$landerCommentsByDate->onEachSide(0)->links()}}


                </div>
                <div class="tab-pane fade"
                     id="nav-rating"
                     role="tabpanel"
                     aria-labelledby="nav-rating-tab">


                    @foreach($landerCommentsByRating as $landerCommentByRating)
                        <div class="row mb-3">
                            <div class="col-lg-2 offset-lg-1 text-center">
                                <img src="{{ asset('img/comments/'.$landerCommentByRating->avatar)  }}"
                                     class="img-fluid comment-img" alt="">
                                <div class="comment-log text-center m-auto">
                                    @if($landerCommentByRating->provider=='vkontakte')
                                        <svg enable-background="new 0 0 50 50" id="Layer_3"
                                             version="1.1"
                                             viewBox="0 0 50 50" xml:space="preserve"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" class="mt-1">
                                                            <path
                                                                d="M26,34c1,0,1-1.4,1-2c0-1,1-2,2-2s2.7,1.7,4,3c1,1,1,1,2,1s3,0,3,0s2-0.1,2-2c0-0.6-0.7-1.7-3-4  c-2-2-3-1,0-5c1.8-2.5,3.2-4.7,3-5.3c-0.2-0.6-5.3-1.6-6-0.7c-2,3-2.4,3.7-3,5c-1,2-1.1,3-2,3c-0.9,0-1-1.9-1-3c0-3.3,0.5-5.6-1-6  c0,0-2,0-3,0c-1.6,0-3,1-3,1s-1.2,1-1,1c0.3,0,2-0.4,2,1c0,1,0,2,0,2s0,4-1,4c-1,0-3-4-5-7c-0.8-1.2-1-1-2-1c-1.1,0-2,0-3,0  c-1,0-1.1,0.6-1,1c2,5,3.4,8.1,7.2,12.1c3.5,3.6,5.8,3.8,7.8,3.9C25.5,34,25,34,26,34z"
                                                                fill="#cccccc" id="VK_2_"/>
                                                        </svg>
                                    @elseif($landerCommentByRating->provider=='facebook')
                                        <svg enable-background="new 0 0 50 50" id="Layer_2"
                                             version="1.1"
                                             viewBox="0 0 50 50" xml:space="preserve"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" class="mt-2">
                                                                <path
                                                                    d="M30,26l1-5l-5,0v-4c0-1.5,0.8-2,3-2h2v-5c0,0-2,0-4,0c-4.1,0-7,2.4-7,7v4h-5v5h5v14h6V26H30z"
                                                                    fill="#cccccc" id="f_2_"/>
                                                             </svg>
                                    @elseif($landerCommentByDate->provider=='instagram')
                                        <svg enable-background="new 0 0 50 50"
                                             viewBox="0 0 256 256"
                                             version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             preserveAspectRatio="xMidYMid" width="70%" height="70%"
                                             class="mt-3">
                                            <g>
                                                <path
                                                    d="M127.999746,23.06353 C162.177385,23.06353 166.225393,23.1936027 179.722476,23.8094161 C192.20235,24.3789926 198.979853,26.4642218 203.490736,28.2166477 C209.464938,30.5386501 213.729395,33.3128586 218.208268,37.7917319 C222.687141,42.2706052 225.46135,46.5350617 227.782844,52.5092638 C229.535778,57.0201472 231.621007,63.7976504 232.190584,76.277016 C232.806397,89.7746075 232.93647,93.8226147 232.93647,128.000254 C232.93647,162.177893 232.806397,166.225901 232.190584,179.722984 C231.621007,192.202858 229.535778,198.980361 227.782844,203.491244 C225.46135,209.465446 222.687141,213.729903 218.208268,218.208776 C213.729395,222.687649 209.464938,225.461858 203.490736,227.783352 C198.979853,229.536286 192.20235,231.621516 179.722476,232.191092 C166.227425,232.806905 162.179418,232.936978 127.999746,232.936978 C93.8200742,232.936978 89.772067,232.806905 76.277016,232.191092 C63.7971424,231.621516 57.0196391,229.536286 52.5092638,227.783352 C46.5345536,225.461858 42.2700971,222.687649 37.7912238,218.208776 C33.3123505,213.729903 30.538142,209.465446 28.2166477,203.491244 C26.4637138,198.980361 24.3784845,192.202858 23.808908,179.723492 C23.1930946,166.225901 23.0630219,162.177893 23.0630219,128.000254 C23.0630219,93.8226147 23.1930946,89.7746075 23.808908,76.2775241 C24.3784845,63.7976504 26.4637138,57.0201472 28.2166477,52.5092638 C30.538142,46.5350617 33.3123505,42.2706052 37.7912238,37.7917319 C42.2700971,33.3128586 46.5345536,30.5386501 52.5092638,28.2166477 C57.0196391,26.4642218 63.7971424,24.3789926 76.2765079,23.8094161 C89.7740994,23.1936027 93.8221066,23.06353 127.999746,23.06353 M127.999746,0 C93.2367791,0 88.8783247,0.147348072 75.2257637,0.770274749 C61.601148,1.39218523 52.2968794,3.55566141 44.1546281,6.72008828 C35.7374966,9.99121548 28.5992446,14.3679613 21.4833489,21.483857 C14.3674532,28.5997527 9.99070739,35.7380046 6.71958019,44.1551362 C3.55515331,52.2973875 1.39167714,61.6016561 0.769766653,75.2262718 C0.146839975,88.8783247 0,93.2372872 0,128.000254 C0,162.763221 0.146839975,167.122183 0.769766653,180.774236 C1.39167714,194.398852 3.55515331,203.703121 6.71958019,211.845372 C9.99070739,220.261995 14.3674532,227.400755 21.4833489,234.516651 C28.5992446,241.632547 35.7374966,246.009293 44.1546281,249.28042 C52.2968794,252.444847 61.601148,254.608323 75.2257637,255.230233 C88.8783247,255.85316 93.2367791,256 127.999746,256 C162.762713,256 167.121675,255.85316 180.773728,255.230233 C194.398344,254.608323 203.702613,252.444847 211.844864,249.28042 C220.261995,246.009293 227.400247,241.632547 234.516143,234.516651 C241.632039,227.400755 246.008785,220.262503 249.279912,211.845372 C252.444339,203.703121 254.607815,194.398852 255.229725,180.774236 C255.852652,167.122183 256,162.763221 256,128.000254 C256,93.2372872 255.852652,88.8783247 255.229725,75.2262718 C254.607815,61.6016561 252.444339,52.2973875 249.279912,44.1551362 C246.008785,35.7380046 241.632039,28.5997527 234.516143,21.483857 C227.400247,14.3679613 220.261995,9.99121548 211.844864,6.72008828 C203.702613,3.55566141 194.398344,1.39218523 180.773728,0.770274749 C167.121675,0.147348072 162.762713,0 127.999746,0 Z M127.999746,62.2703115 C91.698262,62.2703115 62.2698034,91.69877 62.2698034,128.000254 C62.2698034,164.301738 91.698262,193.730197 127.999746,193.730197 C164.30123,193.730197 193.729689,164.301738 193.729689,128.000254 C193.729689,91.69877 164.30123,62.2703115 127.999746,62.2703115 Z M127.999746,170.667175 C104.435741,170.667175 85.3328252,151.564259 85.3328252,128.000254 C85.3328252,104.436249 104.435741,85.3333333 127.999746,85.3333333 C151.563751,85.3333333 170.666667,104.436249 170.666667,128.000254 C170.666667,151.564259 151.563751,170.667175 127.999746,170.667175 Z M211.686338,59.6734287 C211.686338,68.1566129 204.809755,75.0337031 196.326571,75.0337031 C187.843387,75.0337031 180.966297,68.1566129 180.966297,59.6734287 C180.966297,51.1902445 187.843387,44.3136624 196.326571,44.3136624 C204.809755,44.3136624 211.686338,51.1902445 211.686338,59.6734287 Z"
                                                    fill="#cccccc"></path>
                                            </g>
                                        </svg>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <div class="col-12 comment-date">
                                    {{$landerCommentByRating->created}}
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div
                                            class="col-12 comment-name d-flex align-content-center flex-wrap flex-lg-nowrap">
                                            <a href="{{$landerCommentByRating->link}}" target="_blank">
                                                {{$landerCommentByRating->name}}
                                            </a>

                                            <div class="ml-5 all-stars-stars">
                                                @for( $i=1; $i<=5; $i++ )
                                                    @if($landerCommentByRating->rating>=$i)
                                                        <img src="{{asset('img/cms/star-full@2x.png')}}"
                                                             class="comment-star comment-star-full"
                                                             alt="">
                                                    @else
                                                        <img
                                                            src="{{asset('img/cms/star-empty@2x.png')}}"
                                                            class="comment-star comment-star-empty"
                                                            alt="">
                                                    @endif

                                                @endfor
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 comment-text mt-3">
                                            {{$landerCommentByRating->comment}}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{$landerCommentsByRating->onEachSide(0)->links()}}


                </div>

            </div>
        </div>
    </div>
@endif
