{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Lander')

@section('content_header')
    <h1>
        {{ $lander->name  }}
        <a href="{{route('lander.rich')}}" target="_blank" title="ссылка">
            <small class=" text-muted">
                <i class="fa fa-share fa-1x"></i>
            </small>
        </a>
    </h1>

@stop

@section('content')

        @if (\Session::has('success'))
            <div class="col-12">
                <div class="box-body">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! \Session::get('success') !!}
                    </div>
                </div>
            </div>
        @elseif (\Session::has('fail'))
            <div class="col-12">
                <div class="box-body">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! \Session::get('fail') !!}
                    </div>
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-12">
                <div class="box-body">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! implode('', $errors->all('<div>:message</div>')) !!}
                    </div>
                </div>
            </div>
        @endif

        <div class="col-12">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-info-tab"
                               data-toggle="pill" href="#custom-tabs-info" role="tab"
                               aria-controls="custom-tabs-info" aria-selected="true">Текст</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-meta-tab"
                               data-toggle="pill" href="#custom-tabs-meta" role="tab"
                               aria-controls="custom-tabs-three-messages" aria-selected="false">Мета</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content" id="custom-tabs-three-tabContent">
                        <div class="tab-pane fade show active" id="custom-tabs-info" role="tabpanel" aria-labelledby="custom-tabs-info-tab">
                            <form action="{{route('cms.update.lander',[$lander->id])}}"
                                  method="post" class="" enctype="multipart/form-data">
                                @method('PATCH')
                                @csrf
                                <input type="hidden" name="lander-id" id="lander-id" value="{{ $lander->id}}">
                                <div class="row">
                                    @each('cms.bocks.form.input-text', $lines, 'lines')
                                </div>
                                <div class="row">
                                    @each('cms.bocks.form.textarea', $texts, 'texts')
                                </div>
                                <div class="row">
                                    @each('cms.bocks.form.icons', $icons, 'icons')
                                </div>
                                <div class="row">
                                    @include('cms.bocks.form.submit',['value'=>'Сохранить'])
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade"
                             id="custom-tabs-meta" role="tabpanel"
                             aria-labelledby="custom-tabs-meta-tab">
                            <form action="{{route('cms.update-meta.lander',[$lander->id])}}" method="post" class="">
                                @method('PATCH')
                                @csrf
                                <div class="row">
                                @foreach($meta as $keyMeta=>$keyValue)
                                        @include('cms.bocks.form.input-text-simple',['key'=>$keyMeta,'value'=>$keyValue])
                                @endforeach
                                </div>
                                <div class="row">
                                    @include('cms.bocks.form.submit',['value'=>'Сохранить'])
                                </div>
                            </form>

{{--                            <div class="row">--}}
{{--                                @each('cms.bocks.form.input-text-simple', $meta, 'meta')--}}
{{--                            </div>--}}

{{--                            <form action="{{route('cms.update.lander',[$lander->id])}}" method="post" class="">--}}
{{--                                @method('PATCH')--}}
{{--                                @csrf--}}
{{--                                <input type="hidden" name="lander-id" id="lander-id" value="{{ $lander->id}}">--}}
{{--                                <div class="row">--}}
{{--                                    @each('cms.bocks.form.input-text', $lines, 'lines')--}}
{{--                                </div>--}}
{{--                                <div class="row">--}}
{{--                                    @include('cms.bocks.form.submit',['value'=>'Сохранить'])--}}
{{--                                </div>--}}
{{--                            </form>--}}

                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>




@stop





@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script>
        $('.textarea').wysihtml5(
            {
                toolbar: {
                    "fa" : true,
                    "font-styles": false,
                    "lists": false,
                    "html": false,
                    "link": false,
                    "image": false,
                    "color": false,
                    "blockquote": false,
                    "size": "sm"
                }
            }
        );
    </script>
@stop
