<div class="form-group col-lg-6">
    <label for="{{ $texts->key }}">{{ $texts->name }}</label>
    <textarea
        class="form-control textarea"
        name="{{ $texts->key }}"
        id="{{ $texts->key }}"
        placeholder="{{ $texts->name }}"
        rows="10"
    >{{ $texts->value }}</textarea>
</div>
