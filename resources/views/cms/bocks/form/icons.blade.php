<div class="form-group col-lg-6">
    <div class="row">
        <div class="col-lg-5">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new img-thumbnail" style="width: 200px; height: 150px;">
                    <img src="{{ url('img/rich/'.$icons->icon)}}" alt="...">
                </div>
                <div class="fileinput-preview fileinput-exists img-thumbnail"
                     style="max-width: 200px; max-height: 150px;"></div>
                <div>
            <span class="btn btn-outline-secondary btn-file">
                <span class="fileinput-new">Выбрать</span>
                <span class="fileinput-exists">Изменить</span>
{{--                <input type="file" name="{{$icons->slug}}">--}}
                <input accept="image/*" type="file" name="{{$icons->slug}}[icon]">
            </span>
                    <a href="#" class="btn btn-outline-secondary fileinput-exists" data-dismiss="fileinput">Удалить</a>
                </div>
            </div>
        </div>
        <input type="hidden" name="{{$icons->slug}}[id]" value="{{$icons->id}}">
        <div class="form-group col-lg-7">
            <label for="" style="display: none"></label>
            <textarea
                class="form-control"
                name="{{$icons->slug}}[text]"
                id="ololo-text"
                placeholder="Текст"
                rows="6"
            >{{$icons->text}}</textarea>
        </div>
    </div>
</div>
