@if ( isset($col) )
    <div class="form-group col-lg-{{$col}}">
@else
    <div class="form-group col-lg-6">
@endif
@if ( isset($label) )
    <label for="{{ $key }}">{{ $label }}: </label>
@else
    <label for="{{ $key }}">{{ ucfirst($key)  }}: </label>
@endif
    <textarea
        class="form-control textarea"
        name="{{ $key }}"
        id="{{ $key }}"
        placeholder="{{ $label }}"
        rows="5"
    >{{ $value }}</textarea>
</div>
