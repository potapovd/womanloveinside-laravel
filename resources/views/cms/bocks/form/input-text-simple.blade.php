@if ( isset($col) )
    <div class="form-group col-lg-{{$col}}">
@else
    <div class="form-group col-lg-6">
@endif

@if ( isset($label) )
    <label for="{{ $key }}">{{ $label }}:</label>
@else
    <label for="{{ $key }}">{{ ucfirst($key) }}:</label>
@endif
    <input
        type="text"
        class="form-control"
        name="{{ $key }}"
        id="{{ $key }}"
        placeholder="{{ $key }}"
        value="{{ $value }}"
        @if ( isset($disabled) )
            disabled
        @endif
    />
</div>

