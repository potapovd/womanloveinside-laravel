@if ( isset($col) )
    <div class="form-group col-lg-{{$col}}">
@else
    <div class="form-group col-lg-6">
@endif

@if ( isset($label) )
    <label for="{{ $key }}">{{ $label }}:</label>
@else
    <label for="{{ $key }}">{{ ucfirst($key) }}:</label>
@endif
        <select
            class="form-control"
            name="{{ $key }}"
            id="{{ $key }}"
            placeholder="{{ $key }}">
            @foreach( $items as $item  )
                @if($defaultValue==$item['value'])
                    <option selected  value="{{$item['value']}}"> {{$item['option']}}  </option>
                @else
                    <option value="{{$item['value']}}"> {{$item['option']}}  </option>
                @endif
            @endforeach
        </select>
</div>

