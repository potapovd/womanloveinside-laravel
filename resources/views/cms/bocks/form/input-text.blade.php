<div class="form-group col-lg-6">
    <label for="{{ $lines->key }}">{{ $lines->name }}</label>
    @if ($lines->key =='action-date')
        <input
            type="date"
            min="{{date('yy-m-d')}}" max="2021-12-31"
            class="form-control"
            name="{{ $lines->key }}"
            id="{{ $lines->key }}"
            placeholder="{{ $lines->name }}"
            value="{{ $lines->value }}"
        />
    @else
        <input
            type="text"
            class="form-control"
            name="{{ $lines->key }}"
            id="{{ $lines->key }}"
            placeholder="{{ $lines->name }}"
            value="{{ $lines->value }}"
        />
    @endif

</div>
