{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
{{--    <h1>Dashboard</h1>--}}
@stop

@section('content')
{{--    <p>Welcome to this beautiful admin panel.</p>--}}
    <div class="container">
        <div class="row">
            <div class="col-sm-4 text-center">
                <a href="#">
                    <img src="{{secure_asset('img/cms/wake-up-logo@2x.png')}}" class="img-fluid dashboard-img dashboard-img-offline" alt="">
                </a>
            </div>
            <div class="col-sm-4 text-center">
                <a href="#">
                    <img src="{{secure_asset('img/cms/rebirth-logo@2x.png')}}" class="img-fluid dashboard-img dashboard-img-offline" alt="">
                </a>
            </div>
            <div class="col-sm-4 text-center">
                <a href="{{route('cms.edit.lander',['lander'=>1])}}">
                    <img src="{{asset('img/cms/rich-logo@2x.png')}}" class="img-fluid dashboard-img " alt="">
                </a>
            </div>
        </div>
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
{{--    <script> console.log('Hi!'); </script>--}}
@stop
