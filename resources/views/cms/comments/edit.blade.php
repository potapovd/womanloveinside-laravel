{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Comment')

@section('content_header')

    <h1>
        <div class="row">
            <div class="col-1">
                <a href="{{route('cms.comments.index')}}">
                    <i title="назад" class="fa fa-angle-left"></i>
                </a>
            </div>
            <div class="col-11">
                {{$comment->name}}
                <a href="{{$comment->link}}" target="_blank" title="ссылка">
                    <small class=" text-muted">
                        <i class="fa fa-share fa-1x"></i>
                    </small>
                </a>
            </div>
        </div>
    </h1>

@stop

@section('content')

    @if (\Session::has('success'))
        <div class="col-12">
            <div class="box-body">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('success') !!}
                </div>
            </div>
        </div>
    @elseif (\Session::has('fail'))
        <div class="col-12">
            <div class="box-body">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! \Session::get('fail') !!}
                </div>
            </div>
        </div>
    @endif

    @if($errors->any())
        <div class="col-12">
            <div class="box-body">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                </div>
            </div>
        </div>
    @endif

    <div class="col-12">
        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-tabs-info-tab"
                           data-toggle="pill" href="#custom-tabs-info" role="tab"
                           aria-controls="custom-tabs-info" aria-selected="true">Отзыв</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-meta-tab"
                           data-toggle="pill" href="#custom-tabs-meta" role="tab"
                           aria-controls="custom-tabs-three-messages" aria-selected="false">Дополнительно</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">

                    <div class="tab-pane fade show active" id="custom-tabs-info" role="tabpanel" aria-labelledby="custom-tabs-info-tab">
                        <form
                           action="{{route('cms.update.comment',[$comment->id])}}"
                              method="post" class="" >
                            @method('PATCH')
                            @csrf
                            <input type="hidden" name="commentId" id="commentId" value="{{ $comment->id}}">
                            <div class="row">
                                @include('cms.bocks.form.input-text-simple',
                                        ['key'=>'created','value'=>$comment->created,'label'=>'Дата','disabled'=>1,'col'=>5]
                                )
                                @include('cms.bocks.form.input-text-simple',
                                        ['key'=>'name','value'=>$comment->name,'label'=>'Имя','col'=>5]
                                )
                                @include('cms.bocks.form.select-simple',
                                        ['key'=>'is_visible','label'=>'Состояние','col'=>2, 'defaultValue'=>$comment->is_visible,
                                            'items'=>[['value'=>'0','option'=>'Выкл'],['value'=>'1','option'=>'Вкл']]
                                        ]
                                )
                            </div>
                            <div class="row">
                                @include('cms.bocks.form.input-text-simple',['key'=>'provider','value'=>$comment->provider,'label'=>'Источник','disabled'=>1,'col'=>5])
                                @include('cms.bocks.form.input-text-simple',['key'=>'link','value'=>$comment->link,'label'=>'Cсылка','disabled'=>1,'col'=>5])
                                @include('cms.bocks.form.select-simple',
                                        ['key'=>'rating','label'=>'Рейтинг','col'=>2, 'defaultValue'=>$comment->rating,
                                            'items'=>[
                                                ['value'=>'1','option'=>'1'],
                                                ['value'=>'2','option'=>'2'],
                                                ['value'=>'3','option'=>'3'],
                                                ['value'=>'4','option'=>'4'],
                                                ['value'=>'5','option'=>'5'],
                                            ]
                                        ]
                                )
                            </div>
                            <div class="row">
                                <div class="col-lg-1">
                                    <label for="photo">Фото:</label><br>
                                    <img src="/img/comments/{{$comment->avatar}}"
                                         class=" w-100 h-auto img-circle" id="photo" alt="">
                                </div>
                                @include('cms.bocks.form.textarea-simple',['key'=>'comment','value'=>$comment->comment,'label'=>'Текст','col'=>11])
                            </div>
                            <div class="row">
                                @include('cms.bocks.form.submit',['value'=>'Сохранить'])
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade"
                         id="custom-tabs-meta" role="tabpanel"
                         aria-labelledby="custom-tabs-meta-tab">
                        <form
{{--                            action="{{route('cms.update-meta.lander',[$lander->id])}}" --}}
                            method="post" class="">
                            <div class="row">
                                @include('cms.bocks.form.input-text-simple',['key'=>'mail','value'=>$comment->email,'label'=>'E-Mail','disabled'=>1])
                                @include('cms.bocks.form.input-text-simple',['key'=>'ip','value'=>$comment->ip,'label'=>'IP','disabled'=>1])
                            </div>
                            <div class="row">
                                @include('cms.bocks.form.input-text-simple',['key'=>'user_agent','value'=>$comment->user_agent,'label'=>'User Agent','disabled'=>1,'col'=>12])
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>




        @stop





        @section('css')
            <link rel="stylesheet" href="/css/admin_custom.css">
        @stop

        @section('js')

@stop
