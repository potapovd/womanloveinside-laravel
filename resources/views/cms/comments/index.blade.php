{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Comments')

@section('content_header')
    <h1>
        Комментарии
    </h1>

@stop

@section('content')

    @if (Session::has('success'))
        <div class="col-12">
            <div class="box-body">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>
    @elseif (Session::has('fail'))
        <div class="col-12">
            <div class="box-body">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! Session::get('fail') !!}
                </div>
            </div>
        </div>
    @endif

    @if($errors->any())
        <div class="col-12">
            <div class="box-body">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                </div>
            </div>
        </div>
    @endif

    <div class="col-12">
        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-body">



                <div class="table-responsive">
                    <table id="example" class="table table-bordered display dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Имя</th>
                            <th>Источник</th>
                            <th>Рейтинг</th>
                            <th>Страница</th>
                            <th>Видимость</th>
                        </tr>
                    </thead>
                    <tbody>


                    @foreach($allComments as $oneComment)
                        <tr>
                            <td>{{$oneComment->created}}</td>
                            <td>
                                <a href="{{route('cms.edit.comment',['id'=>$oneComment->id])}}">
                                    {{$oneComment->name}}
                                </a>
                            </td>
                            <td>{{$oneComment->provider}}</td>
                            <td>{{$oneComment->rating}}</td>
                            <td>
{{--                                {{$oneComment->getLander->name}}--}}
                                @foreach($oneComment->getLander as $lander)
                                    {{ $lander->name }}
                                @endforeach
                            </td>
                            <td>{{$oneComment->is_visible}}</td>


                        </tr>
                    @endforeach



                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Дата</th>
                            <th>Имя</th>
                            <th>Источник</th>
                            <th>Рейтинг</th>
                            <th>Страница</th>
                            <th>Видимость</th>
                        </tr>
                    </tfoot>
                </table>
                </div>

            </div>
            <!-- /.card -->
        </div>

        @stop





        @section('css')
            <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
            <script>
                $(function () {
                    $("#example").DataTable({
                        //"autoWidth": false,
                        "responsive": true,
                        "pageLength": 25,
                        "lengthChange": false,
                        "order": [[ 0, "desc" ]],
                        "createdRow": function( row, data, dataIndex ) {
                            if ( data[5] == 0 ) {
                                $(row).addClass('hidden-table-line');

                            }
                        },
                        "language": {
                            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Russian.json"
                        },
                        "initComplete": function() {
                            // this.api().columns().every(function() {
                            //     var column = this;
                            //     var select = $('<select><option value=""></option></select>')
                            //         .appendTo($(column.footer()).empty())
                            //         .on('change', function() {
                            //             var val = $.fn.dataTable.util.escapeRegex(
                            //                 $(this).val()
                            //             );
                            //
                            //             column
                            //                 .search(val ? '^' + val + '$' : '', true, false)
                            //                 .draw();
                            //         });
                            //     //var i = 0;
                            //     column.data().unique().sort().each(function(d, j) {
                            //         console.log(d)
                            //         select.append('<option value="' + d + '">' + d + '</option>')
                            //         //i++;
                            //     });
                            // });
                        }

                        // "columnDefs": [
                        //     {
                        //         "targets": [ 2 ],
                        //         "visible": false,
                        //         "searchable": false
                        //     }
                        // ]



                    });
                    // $('#example2').DataTable({
                    //     "paging": true,
                    //     "lengthChange": false,
                    //     "searching": false,
                    //     "ordering": true,
                    //     "info": true,
                    //     "autoWidth": false,
                    // });
                });
            </script>
@stop
