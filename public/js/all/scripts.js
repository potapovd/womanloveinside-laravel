//checking Element visible
$.fn.isOnScreen = function () {
    var win = $(window);
    var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();
    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};


//Cookies
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") c = c.substring(1, c.length);
        if ( c.indexOf(nameEQ) == 0 ) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function deleteAllCookies() {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

//URL
function urldecode(url) {
    //return decodeURIComponent((str + '').replace(/\+/g, '%20'));
    return decodeURIComponent(url.replace(/\+/g, " "));
}

//COMMENTS
function checkingCookies() {
    let token = readCookie("token")
    let provider = readCookie("provider")
    let avatar = readCookie("avatar")
    let name = readCookie("name")
    if (
        (token !== null)
    ) {
        name = name.replace("+", " ");
        console.log("token " + token);
        $("#new-comment-photo").attr("src", urldecode(avatar));
        $("#token").val(token);
        $("#new-comment-name").html(name);
        $("#commentForm").slideDown();
        //$(".providericon svg").removeClass("providericon-active");
        $(".leave-comment-txt").html("оставьте свой отзыв")
        $("#facebook-button").hide()
        $("#vkontakte-button").hide()
        //$(`.${ provider } svg`).addClass("providericon-active");
        stopChecking();
    }
    console.log("checkingCookies()")
}

function stopChecking() {
    clearInterval(chekingCookiesFunction);
}

// Comments Function
var chekingCookiesFunction = setInterval(checkingCookies, 1000);


$(".comment-star-select").on("click", function (e) {
    e.preventDefault();
    var rating = $(this).attr("data-rating");
    $("#rating").attr("value", rating);
    $(".rating-block").children("img").each(function () {
        const those = $(this)
        if ( those.attr("data-rating") <= rating ) {
            those.attr("src", "/img/cms/star-full@2x.png")
        } else {
            those.attr("src", "/img/cms/star-empty@2x.png")
        }
    });
})


//OTHER
//Toggle full text
$("#full-text").on("click", function () {
    $(".show-full-text").slideToggle("slow")
    return !1
});

//go to price block
$("#toPrice").click(function (e) {
    let link = $(this).attr("href");
    $("html, body").animate({
        scrollTop: $(link).offset().top + 0
    }, "slow");
    return !1
});


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split("&"),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split("=");

        if ( sParameterName[0] === sParam ) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
let page = getUrlParameter("page");
if ( page !== undefined ) {
    $("html, body").animate({
        scrollTop: $("#comments").offset().top + 60
    }, "slow");
}
