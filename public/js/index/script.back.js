jQuery(document).ready(function () {
    $('.slidersss').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 2000,
    })
    $('.sliderss').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 2000,
    })
    $('.slide-one').owlCarousel({
        dots: false,
        loop: true,
        stagePadding: 300,
        items: 1,
        nav: true,
        navContainer: ".nav",
        navElement: 'img class="custom-prev" src="./img/wakeup/img6.png" alt=""',
        navText: ["",""],
        responsiveClass:true,
        responsive:{
            0:{
                stagePadding: 50,
                items: 1,
            },
            768:{
                stagePadding: 300,
                items: 1,
            },

        }
    });
    $('.slide-two-one-modal').owlCarousel({
        dots: false,
        loop: false,
        //stagePadding: 300,
        items: 3,
        nav: false,
        navContainer: ".nav-modal-one",
        navElement: 'img class="modal-custom-prev" src="./img/wakeup/img6.png" alt=""',
        //navText: false
        responsiveClass:true,
        responsive:{
            0:{
                items: 1,
            },
            768:{
                items: 3,
            },
        }
    });
    $('.slide-two-two-modal').owlCarousel({
        dots: false,
        loop: false,
        //stagePadding: 300,
        items: 3,
        nav: false,
        navContainer: ".nav-modal-two",
        navElement: 'img class="modal-custom-prev" src="./img/wakeup/img6.png" alt=""',
        //navText: false
        responsiveClass:true,
        responsive:{
            0:{
                items: 1,
            },
            768:{
                items: 3,
            },
        }
    });
    $('.slide-two-three-modal').owlCarousel({
        dots: false,
        loop: false,
        //stagePadding: 300,
        items: 3,
        nav: false,
        navContainer: ".nav-modal-three",
        navElement: 'img class="modal-custom-prev" src="./img/wakeup/img6.png" alt=""',
        //navText: false
        responsiveClass:true,
        responsive:{
            0:{
                items: 1,
            },
            768:{
                items: 3,
            },
        }
    });
});

/*var slideIndex = 1;
showSlides(slideIndex);
autoSlider();


function plusSlide() {
    showSlides(slideIndex += 1);
}


function minusSlide() {
    showSlides(slideIndex -= 1);
}


function currentSlide(n) {
    showSlides(slideIndex = n);
}


function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("item");
    var dots = document.getElementsByClassName("slider-dots_item");
    if (n > slides.length) {
      slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
}

function autoSlider() {
var i;
var slides = document.getElementsByClassName("item");
var dots = document.getElementsByClassName("slider-dots_item");
for (i = 0; i < slides.length; i++) {
slides[i].style.display = "none";
}
slideIndex++;
if (slideIndex> slides.length) {slideIndex = 1}
for (i = 0; i < dots.length; i++) {
dots[i].className = dots[i].className.replace(" active", "");
}
slides[slideIndex-1].style.display = "block";
dots[slideIndex-1].className += " active";
setTimeout(autoSlider, 6000); // Измените значения авто прокрутки в миллисекундах (ms), пример 6000 это 6 секунд
}*/

jQuery(document).ready(function ($) {
    $('a[href^="#pricing"]').on('click', function (e) {
        $('html,body').stop().animate({ scrollTop: $(this.hash).offset().top }, 1000);
        e.preventDefault();
    });
});

/*var slides = document.querySelectorAll('#slides .slide');
var currentSlide = 0;
var slideInterval = setInterval(nextSlide,6000);

function nextSlide() {
 slides[currentSlide].className = 'slide';
 currentSlide = (currentSlide+1)%slides.length;
 slides[currentSlide].className = 'slide showing';
}*/

// Modal
$(document).ready(function() { // зaпускaем скрипт пoсле зaгрузки всех элементoв
    /* зaсунем срaзу все элементы в переменные, чтoбы скрипту не прихoдилoсь их кaждый рaз искaть при кликaх */
    var overlay = $('#overlay'); // пoдлoжкa, дoлжнa быть oднa нa стрaнице
    var open_modal = $('.open_modal'); // все ссылки, кoтoрые будут oткрывaть oкнa
    var close = $('.modal_close, #overlay, .close-in-popup'); // все, чтo зaкрывaет мoдaльнoе oкнo, т.е. крестик и oверлэй-пoдлoжкa
    var modal = $('.modal_div'); // все скрытые мoдaльные oкнa

    open_modal.click(function(event) { // лoвим клик пo ссылке с клaссoм open_modal
        event.preventDefault(); // вырубaем стaндaртнoе пoведение
        var div = $(this).attr('href'); // вoзьмем стрoку с селектoрoм у кликнутoй ссылки
        overlay.fadeIn(400, //пoкaзывaем oверлэй
            function() { // пoсле oкoнчaния пoкaзывaния oверлэя
                $(div) // берем стрoку с селектoрoм и делaем из нее jquery oбъект
                    .css('display', 'block')
                    .animate({
                        opacity: 1,
                        top: '50%'
                    }, 200); // плaвнo пoкaзывaем
            });
    });

    close.click(function() { // лoвим клик пo крестику или oверлэю
        modal // все мoдaльные oкнa
            .animate({
                    opacity: 0,
                    top: '45%'
                }, 200, // плaвнo прячем
                function() { // пoсле этoгo
                    $(this).css('display', 'none');
                    overlay.fadeOut(400); // прячем пoдлoжку
                }
            );
    });
});
