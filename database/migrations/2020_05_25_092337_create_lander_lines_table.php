<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lander_line', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('lander_id')->unsigned();
            $table->bigInteger('line_id')->unsigned();
            $table->timestamps();

            //$table->unique('lander_id','text_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lander_line');
    }
}
