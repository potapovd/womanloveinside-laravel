<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->string('uid')->nullable();
            $table->string('provider');
            $table->text('link');
            $table->string('name');
            $table->string('avatar');
            $table->text('avatar_provider')->nullable();
            $table->string('rating');
            $table->string('mail')->nullable();
            $table->mediumText('comment');
            $table->timestamp('created')->default(now());
            $table->string('ip');
            $table->text('user_agent')->nullable();
            $table->text('token')->nullable();
            $table->boolean('is_visible')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
