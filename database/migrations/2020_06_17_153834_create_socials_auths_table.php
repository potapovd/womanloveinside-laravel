<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialsAuthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socials_auths', function (Blueprint $table) {
            $table->id();
            $table->string('ip');
            $table->string('token')->unique();
            $table->string('csrf');
            $table->string('uid')->nullable();
            $table->string('provider');
            $table->text('link');
            $table->string('name');
            $table->string('avatar');
            $table->string('mail')->nullable();
            $table->text('avatar_provider')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socials_auths');
    }
}
