<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(LanderSeeder::class);
        $this->call(TextSeeder::class);
        $this->call(LineSeeder::class);
        $this->call(IconSeeder::class);
        $this->call(CommentSeeder::class);

    }
}
