<?php

use Illuminate\Database\Seeder;

use App\Comment;
use App\Lander;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        //https://graph.facebook.com/v2.11/1598106990328208/picture?type=large

        $json = File::get( config('landing.PATH_TO_COMMENTS_JSON') );
        $data = json_decode($json,true);
        $arrayReviews = (array) $data;
        $arrayReviews = ($arrayReviews['reviews']);
        //die();
        foreach ($arrayReviews as $arrayReview) {

            if(isset($arrayReview['author']['www'])){
                $link = $arrayReview['author']['www'];
            }else{
                $link = '';
            }
            if(isset($arrayReview['author']['hash'])){
                $hash = $arrayReview['author']['hash'];
            }else{
                $hash = '';
            }
            if(isset($arrayReview['author']['email'])){
                $mail = $arrayReview['author']['email'];
            }else{
                $mail = '';
            }
            if(isset($arrayReview['author']['avatar'])){
                $avatar = $arrayReview['author']['avatar']."?type=large";
            }else{
                $avatar = '';
            }
            $avatarName = md5(rand(0,100).microtime()).".jpg";


//            $local_file = config('landing.PATH_TO_COMMENTS_PHOTO')."/".$avatarName;
//            print_r($local_file);
//            die();

            $newComment = [
                'provider'=>$arrayReview['author']['provider'],
                'link'=>$link,
                'name'=>$arrayReview['author']['name'],
                'avatar'=>$avatarName,
                'mail'=>$mail,
                'avatar_provider'=>$avatar,
                'rating'=>$arrayReview['star'],
                'comment'=>$arrayReview['comment'],
                'created'=> date('Y-m-d H:i:s', round($arrayReview['created']/1000) ),
                'ip'=> $arrayReview['ip'],
                'user_agent'=> '',
                'is_visible'=>1,
                'token'=>$hash
            ];



            if(!empty($arrayReview['author']['avatar'])){

                $local_file = config('landing.PATH_TO_COMMENTS_PHOTO')."/".$avatarName;
                $fp = fopen ($local_file, 'w+');

                $handle = curl_init();

                // $url = "http://scontent-sin6-1.cdninstagram.com/v/t51
                //.2885-19/s150x150/61762576_2057270967908812_2870037837430915072_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_ohc=x0aQUSsRfxEAX8JD_ND&oh=f17a1815f531c5240c5c41fdd1b598fd&oe=5F0D4813";

                $url = $arrayReview['author']['avatar'];

                curl_setopt($handle, CURLOPT_URL, $url);
                curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);


                curl_setopt($handle, CURLOPT_HTTPHEADER, array(
                    'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)'
                ));

                curl_setopt($handle, CURLOPT_TIMEOUT, 50);
                curl_setopt($handle, CURLOPT_FILE, $fp);
                curl_setopt($handle, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($handle, CURLOPT_ENCODING, "");



                $output = curl_exec($handle);


                $httpCode =  curl_getinfo($handle, CURLINFO_HTTP_CODE);

                if($httpCode !== 200){
                    $newComment['avatar'] = 'no-ava.jpg';
                }
                echo $newComment['avatar']."\n";

                curl_close($handle);


            }



            //$rich = Lander::where('slug', '=', 'rich')->first();
            $rebirth = Lander::where('slug', '=', 'rebirth')->first();
            $wakeup = Lander::where('slug', '=', 'wakeup')->first();

            $newRecord = Comment::create($newComment);
            if($arrayReview['chan']['id']==358374){
                //rebith
                $newRecord ->getLander()->attach($rebirth);
            }elseif ($arrayReview['chan']['id']==357323){
                //wakeup
                $newRecord ->getLander()->attach($wakeup);
            }else{
                echo  $arrayReview['chan']['id'];
                //$newRecord ->getLander()->attach($rich);
            }


        }


    }
}
