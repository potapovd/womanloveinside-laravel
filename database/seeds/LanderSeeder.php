<?php

use Illuminate\Database\Seeder;
use App\Lander;

class LanderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $landers = [
            [
                'slug' => 'rich',
                'name'=>'Rich',
                'title'=>'Курс "Богатство"',
                'description'=>'Курс "Богатство"',
                'keywords'=>'Курс "Богатство"'
            ],
            [
                'slug' => 'rebirth',
                'name'=>'Rebirth',
                'title'=>'Курс Rebirth ',
                'description'=>'Курс "Rebirth"',
                'keywords'=>'Курс "Rebirth"'
            ],
            [
                'slug' => 'wakeup',
                'name'=>'Wakeup',
                'title'=>'Курс "Wakeup"',
                'description'=>'Курс "Wakeup"',
                'keywords'=>'Курс "Wakeup"'
            ],
            [
                'slug' => 'index',
                'name'=>'Index',
                'title'=>'Курс "Index"',
                'description'=>'Курс "Index"',
                'keywords'=>'Курс "Index"'
            ],
        ];
        foreach ($landers as $lander) {
            Lander::create([
                'slug' => $lander['slug'],
                'name' => $lander['name'],
                'title' => $lander['title'],
                'description' => $lander['description'],
                'keywords' => $lander['keywords']
            ]);
        }
    }
}
