<?php

use Illuminate\Database\Seeder;
use App\Lander;
use App\Line;

class LineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $rich = Lander::where('slug', '=', 'rich')->first();
        $rebirth = Lander::where('slug', '=', 'rebirth')->first();

        $richTexts = [
            [
                'name'  => 'Дата:',
                'key'   => 'date',
                'value' => '1 июня'
            ],
            [
                'name'  => 'Ссылка:',
                'key'   => 'link',
                'value' => 'https://lk.timurgilmanov.com/product/kurs-bogatstvo/'
            ],
            [
                'name'  => 'Полная цена:',
                'key'   => 'full-price',
                'value' => '1000'
            ],
            [
                'name'  => 'Акционная цена:',
                'key'   => 'action-price',
                'value' => '4000'
            ],
            [
                'name'  => 'Старновая акционная цена:',
                'key'   => 'today-price',
                'value' => '3500'
            ],
            [
                'name'  => 'Дата окончание акции:',
                'key'   => 'action-date',
                'value' => '29.05.2020'
            ],
        ];

        foreach ($richTexts as $richText) {
             $richRecord = Line::create([
                     'key' => $richText['key'],
                     'value' => $richText['value'],
                     'name' => $richText['name'],
                 ]);
             $richRecord->getLander()->attach($rich);
        }


    }
}
