<?php

use Illuminate\Database\Seeder;
use App\User;
Use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'RootAdmiN',
            'email' => 'potapov.dima@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('1Root@dmiN221!')
        ]);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('AdminPassword314s')
        ]);
    }
}
