<?php

use Illuminate\Database\Seeder;
use App\Icon;
use App\Lander;


class IconSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
//        $table->mediumText('icon')->nullable();
//        $table->mediumText('text')->nullable();
//        $table->string('slug');
        $rich = Lander::where('slug', '=', 'rich')->first();
        $icons = [
            [
                'icon'=>'six.png',
                'text'=>'вебинаров',
                'slug'=>'course-info-'
            ],
            [
                'icon'=>'one.png',
                'text'=>'индивидуальная консультация',
                'slug'=>'course-info-'
            ],
            [
                'icon'=>'chat.png',
                'text'=>'Онлайн чат',
                'slug'=>'course-info-'
            ],
            [
                'icon'=>'materials.png',
                'text'=>'Уникальные материалы',
                'slug'=>'course-info-'
            ]
        ];
        $i = 0;
        foreach ($icons as $icon){
            $richRecord = Icon::create([
                'icon'=>$icon['icon'],
                'text'=>$icon['text'],
                'slug'=>$icon['slug'].$i
            ]);
            $richRecord ->getLander()->attach($rich);
            $i ++;
        }

    }
}
