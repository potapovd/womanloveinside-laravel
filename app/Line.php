<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Line
 *
 * @property int $id
 * @property string $key
 * @property string $value
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Line whereValue($value)
 * @mixin \Eloquent
 */
class Line extends Model
{
    protected $fillable = [
        'key','value','updated_at'
    ];

    //
    public function getLander()
    {
        return $this->belongsToMany('App\Lander')->withTimestamps();
    }
}
