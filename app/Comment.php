<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Comment
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $uid
 * @property string $provider
 * @property string $link
 * @property string $name
 * @property string $avatar
 * @property string|null $avatar_provider
 * @property string $rating
 * @property string $comment
 * @property string $ip
 * @property string $user_agent
 * @property string $token
 * @property int $is_visible
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereAvatarProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereIsVisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUserAgent($value)
 * @property string $created
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCreated($value)
 * @property string|null $mail
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereMail($value)
 */
class Comment extends Model
{
    //
    //
    protected $guarded = ['id'];



    public function getLander()
    {
        return $this->belongsToMany('App\Lander')->withTimestamps();
    }
}
