<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Text
 *
 * @property int                             $id
 * @property Line                            $key
 * @property Line                            $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereValue($value)
 * @mixin \Eloquent
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Text whereName($value)
 */
class Text extends Model
{
    //
    public function getLander()
    {
        return $this->belongsToMany('App\Lander')->withTimestamps();
    }
}
