<?php

namespace App\Http\Controllers\Cms;

//use App\Lander;
//use App\Icon;
use App\SocialsAuth;
use App\Comment;
use App\Lander;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    //

    public function index()
    {
        $allComments = Comment::all()->sortByDesc('id');
        //dd($allComments);
        return view('cms.comments.index',compact('allComments'));
    }

    public function editComment(Request $request)
    {
        $comment = Comment::where('id', '=', $request->id)->first();
        $commentLander = Comment::where('id', '=', $request->id)
            ->first()
            ->getLander
            ->first();
        return view('cms.comments.edit',compact('commentLander','comment'));
    }

    public function updateComment(Request $request,Comment $comment)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'is_visible' => 'required|numeric',
            'rating' => 'required|numeric',
            'comment' => 'required|string',
        ]);
        if ($validator->fails()) {
            return
                redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $commentUpdate = Comment::find($request->commentId);
        $commentUpdate->name = $request->name;
        $commentUpdate->is_visible = $request->is_visible;
        $commentUpdate->rating = $request->rating;
        $commentUpdate->comment = $request->comment;
        $commentUpdate->save();


        return redirect()->back()->with('success', 'Обновления сохранены успешно!' );

    }

//
    static function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }else{
                        return '127.0.0.1';
                    }
                }
            }
        }
    }
    public function addComment(Request $request)
    {

       // dd($request->getPathInfo());

        //dd( $request->all() );
        $validator = Validator::make($request->all(), [
            'token' => 'required|string|max:250',
            'rating' => 'required|numeric|min:1|max:5',
            'comment' => 'required|string',
            //'photo' => 'required|string|max:255',
            //'name' => 'required|string|max:100',
            //'network' => 'required|string|max:100',
            //'profile' => 'required|string|max:100',
            'page' => 'required|in:rich,rebirth,wakeup',
        ]);
        if ($validator->fails()) {
            return
                redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        //getting info about click
        $clickInfo = SocialsAuth::where('token','=',$request->token)->first();
        if(!$clickInfo->count()){
//            echo ('error!');
            return redirect()->back()->with('error', 'Произошла ошибка! Обновите стараницу и попробуйте еще раз !' );
        }

            //
        $newComment  = [
            //
            'name'=> $clickInfo->name,
            'rating'=>$request->rating,
            'comment'=>$request->comment,
            'is_visible'=>1,
            'ip'=> $this->getIp(),
            'link'=>$clickInfo->link,
            'provider'=>$clickInfo->provider,
            //'avatar'=>$request->photo,
            'avatar_provider'=>$clickInfo->avatar_provider,
            'created'=>Carbon::now(),
            'created_at'=>Carbon::now(),
            'token'=>$clickInfo->token,
            'user_agent'=>$request->header('User-Agent'),
        ];

        //dd($newComment);

        if(!empty($clickInfo->avatar_provider)){
            $avatarName = md5(rand(0,100).microtime()).'.jpg';

            $local_file = config('landing.PATH_TO_COMMENTS_PHOTO')."/".$avatarName;
            $fp = fopen ($local_file, 'w+');

            $handle = curl_init();

            // $url = "http://scontent-sin6-1.cdninstagram.com/v/t51
            //.2885-19/s150x150/61762576_2057270967908812_2870037837430915072_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_ohc=x0aQUSsRfxEAX8JD_ND&oh=f17a1815f531c5240c5c41fdd1b598fd&oe=5F0D4813";

            $url = $clickInfo->avatar_provider;

            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($handle, CURLOPT_HTTPHEADER, array(
                'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)'
            ));

            curl_setopt($handle, CURLOPT_TIMEOUT, 50);
            curl_setopt($handle, CURLOPT_FILE, $fp);
            curl_setopt($handle, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($handle, CURLOPT_ENCODING, "");


            $output = curl_exec($handle);

            $httpCode =  curl_getinfo($handle, CURLINFO_HTTP_CODE);

            if($httpCode !== 200){
                $newComment['avatar'] = 'no-ava.jpg';
            }else{
                $newComment['avatar'] = $avatarName;
            }

            curl_close($handle);

        }

        //dd($newComment,$avatarName);

        $newRecord = Comment::create($newComment);
        $page = Lander::where('slug', '=', $request->page)->first();
        $newRecord ->getLander()->attach($page);

        setcookie("token", "", -1, "/");
        setcookie("provider", "", -1, "/");
        setcookie("name", "", -1, "/");
        setcookie("avatar", "", -1, "/");


        return redirect()->back()->with('success', 'Ваш Отзыв добавлен на сайт!' );

    }


}
