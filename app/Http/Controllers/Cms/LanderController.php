<?php

namespace App\Http\Controllers\Cms;


use App\Lander;
use App\Icon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LanderController extends Controller
{
    //

    public function viewIndex()
    {
//        $landerComments = Lander::where('slug', '=', 'wakeup')
//            ->first()
//            ->getComment
//            ->where('is_visible', '=', 1);
//        $landerCommentsByDate = $landerComments->sortByDesc('created');
//        $landerCommentsByRating = $landerComments->sortByDesc('rating');


        $landerCommentsByDate = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->orderByDesc('comments.created')
            ->paginate(10);


        $landerCommentsByRating = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->orderByDesc('comments.rating')
            ->paginate(10);

        $landerComments = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->get();

        $landerCommentsSum = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->sum("comments.rating");

        $ratings = [];
        for ($i = 1; $i <= 5; $i++) {
            $ratings[$i] = $landerComments
                ->where('rating', $i)
                ->count();
        }
        $ratings = array_reverse($ratings, true);

//        $landerCommentsSum = Lander::where('slug', '=', 'wakeup')
//            ->first()
//            ->getComment
//            ->where('is_visible', '=', 1)
//            ->sum("rating");

        return view('landers.index.index', compact(['landerCommentsByDate', 'landerCommentsByRating', 'landerComments', 'ratings', 'landerCommentsSum']));
    }


    public function viewLanderWakeup()
    {
//        $landerComments = Lander::where('slug', '=', 'wakeup')
//            ->first()
//            ->getComment
//            ->where('is_visible', '=', 1);
        //->get();
        //->simplePaginate(10);
        $landerCommentsByDate = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->orderByDesc('comments.created')
            ->paginate(10);


        $landerCommentsByRating = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->orderByDesc('comments.rating')
            ->paginate(10);

        $landerComments = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->get();

        $landerCommentsSum = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->sum("comments.rating");


        $ratings = [];
        for ($i = 1; $i <= 5; $i++) {
            $ratings[$i] = $landerComments
                ->where('rating', $i)
                ->count();
        }
        $ratings = array_reverse($ratings, true);

        return view('landers.wakeup.index', compact([
            'landerCommentsByDate', 'landerCommentsByRating', 'landerComments', 'ratings',
            'landerCommentsSum'
        ]));
    }

    public function viewLanderWakeupDiscount()
    {
//        $landerComments = Lander::where('slug', '=', 'wakeup')
//            ->first()
//            ->getComment
//            ->where('is_visible', '=', 1);
        //->get();
        //->simplePaginate(10);
        $landerCommentsByDate = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->orderByDesc('comments.created')
            ->paginate(10);


        $landerCommentsByRating = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->orderByDesc('comments.rating')
            ->paginate(10);

        $landerComments = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->get();

        $landerCommentsSum = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'wakeup')
            ->where('is_visible', '=', 1)
            ->sum("comments.rating");


        $ratings = [];
        for ($i = 1; $i <= 5; $i++) {
            $ratings[$i] = $landerComments
                ->where('rating', $i)
                ->count();
        }
        $ratings = array_reverse($ratings, true);

        return view('landers.wakeup.index-discount', compact([
            'landerCommentsByDate', 'landerCommentsByRating', 'landerComments', 'ratings',
            'landerCommentsSum'
        ]));
    }


    public function viewLanderRich()
    {
        $landerLines = Lander::where('slug', '=', 'rich')
            ->first()
            ->getLines
            ->pluck('value', 'key');

        $landerText = Lander::where('slug', '=', 'rich')
            ->first()
            ->getText
            ->pluck('value', 'key');

        $landerIcon = Lander::where('slug', '=', 'rich')
            ->first()
            ->getIcon;
        //->pluck('icon', 'text');

        $landerMeta = Lander::where('slug', '=', 'rich')
            ->select('title', 'description', 'keywords')
            ->first();


        $landerCommentsByDate = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'rich')
            ->where('is_visible', '=', 1)
            ->orderByDesc('comments.created')
            ->paginate(10);


        $landerCommentsByRating = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'rich')
            ->where('is_visible', '=', 1)
            ->orderByDesc('comments.rating')
            ->paginate(10);

        $landerComments = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'rich')
            ->where('is_visible', '=', 1)
            ->get();

        $landerCommentsSum = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'rich')
            ->where('is_visible', '=', 1)
            ->sum("comments.rating");

//        $landerComments = Lander::where('slug', '=', 'rich')
//            ->first()
//            ->getComment
//            ->where('is_visible', '=', 1);
//
//
//        $landerCommentsSum = Lander::where('slug', '=', 'rich')
//            ->first()
//            ->getComment
//            //->orderBy('id','DESC')
//            ->where('is_visible', '=', 1)
//            ->sum("rating");

        //$landerCommentsByDate = $landerComments->sortByDesc('created');
        //$landerCommentsByRating = $landerComments->sortByDesc('rating');

        $ratings = [];
        for ($i = 1; $i <= 5; $i++) {
            $ratings[$i] = $landerComments
                ->where('rating', $i)
                ->count();
        }
        $ratings = array_reverse($ratings, true);


        return view('landers.rich.index', compact(['landerLines', 'landerText', 'landerIcon', 'landerMeta', 'landerCommentsByDate', 'landerCommentsByRating', 'landerComments', 'ratings', 'landerCommentsSum']));
    }


    public function viewLanderRebirth()
    {

        $landerCommentsByDate = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'rebirth')
            ->where('is_visible', '=', 1)
            ->orderByDesc('comments.created')
            ->paginate(10);


        $landerCommentsByRating = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'rebirth')
            ->where('is_visible', '=', 1)
            ->orderByDesc('comments.rating')
            ->paginate(10);

        $landerComments = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'rebirth')
            ->where('is_visible', '=', 1)
            ->get();

        $landerCommentsSum = DB::table('comments')
            ->select(
                'comments.id', 'comments.provider', 'comments.created', 'comments.avatar',
                'comments.link', 'comments.name', 'comments.rating', 'comments.comment', 'comments.is_visible',
                'comment_lander.comment_id', 'comment_lander.lander_id', 'landers.slug'
            )
            ->leftJoin('comment_lander', 'comment_lander.comment_id', '=', 'comments.id')
            ->leftJoin('landers', 'landers.id', '=', 'comment_lander.lander_id')
            ->where('landers.slug', '=', 'rebirth')
            ->where('is_visible', '=', 1)
            ->sum("comments.rating");

//        $landerComments = Lander::where('slug', '=', 'rebirth')
//            ->first()
//            ->getComment
//            ->where('is_visible', '=', 1);
//
//        $landerCommentsSum = Lander::where('slug', '=', 'rebirth')
//            ->first()
//            ->getComment
//            ->where('is_visible', '=', 1)
//            ->sum("rating");
//
//        $landerCommentsByDate = $landerComments->sortByDesc('created');
//        $landerCommentsByRating = $landerComments->sortByDesc('rating');

        $ratings = [];
        for ($i = 1; $i <= 5; $i++) {
            $ratings[$i] = $landerComments
                ->where('rating', $i)
                ->count();
        }
        $ratings = array_reverse($ratings, true);

        return view('landers.rebirth.index', compact(['landerCommentsByDate', 'landerCommentsByRating', 'landerComments', 'ratings', 'landerCommentsSum']));
    }


    //CMS
    public function editLander(Lander $lander)
    {
        $lines = Lander::find($lander->id)->getLines;
        $texts = Lander::find($lander->id)->getText;
        $icons = Lander::find($lander->id)->getIcon;

        $meta = Lander::where('slug', '=', 'rich')
            ->select('title', 'description', 'keywords')
            ->first()
            ->toArray();


        return view('cms.landers.edit', compact(['lander', 'lines', 'texts', 'meta', 'icons']));

    }


    public function updateLanderRich(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'date'                  => 'required|string|max:100',
            'link'                  => 'required|url|max:100',
            'full-price'            => 'required|numeric',
            'action-price'          => 'required|nullable',
            'today-price'           => 'numeric',
            'action-date'           => 'nullable',
            'description-short'     => 'required',
            'description-extension' => 'required',
        ]);

        $requestData = $request->all();
//        dd($requestData);

        if ( $validator->fails() ) {
            return
                redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }


        $landerID = $requestData['lander-id'];
        foreach ($requestData as $keyLine => $valueLine) {
            DB::table('lines')
                ->leftJoin('lander_line', 'lines.id', '=', 'lander_line.line_id')
                ->where('lander_line.lander_id', '=', $landerID)
                ->where('lines.key', '=', $keyLine)
                ->select('lines.key', 'lines.value', 'lines.updated_at AS updated_at')
                ->update([
                    'value'            => $valueLine,
                    'lines.updated_at' => Carbon::now()
                ]);
        }
        foreach ($requestData as $keyText => $valueText) {
            if ( ($keyText == 'description-short') || ($keyText == 'description-extension') ) {
                DB::table('texts')
                    ->leftJoin('lander_text', 'texts.id', '=', 'lander_text.text_id')
                    ->where('lander_text.lander_id', '=', $landerID)
                    ->where('texts.key', '=', $keyText)
                    ->select('texts.key', 'texts.value', 'texts.updated_at AS updated_at')
                    ->update([
                        'value'            => $valueText,
                        'texts.updated_at' => Carbon::now()
                    ]);
            }
        }

        //$files = $request->file('files');
        $files = $request->allFiles();
        $f = [];


        //copying IMGs
        foreach (array_keys($requestData) as $k) {
            if ( preg_match('/^course-info-(\\d+)$/', $k, $matches) ) {
                $iconTable = DB::table('icons')
                    ->where('id', '=', $requestData[$k]['id']);
                if ( isset($requestData[$k]['icon']) ) {
                    $newFile = $requestData[$k]['icon'];
                    $fileName = time() . '-' . $newFile->getClientOriginalName();
                    $newFile->move('img/rich', $fileName);
                    $iconTable->update(['icon' => $fileName]);
                }
                $iconTable->update(['text' => $requestData[$k]['text']]);
            }
        }


        return redirect()->back()->with('success', 'Обновления сохранены успешно!');
    }

    public function updateMetaLanderRich(Lander $lander, Request $request)
    {

        //dd($lander);
        $validator = Validator::make($request->all(), [
            'title'       => 'required|max:100',
            'description' => 'nullable|max:100',
            'keywords'    => 'nullable|max:100',
        ]);

        $requestData = $request->all();
        // dd($requestData);

        if ( $validator->fails() ) {
            return
                redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
        }

        $landerPage = Lander::find($lander->id);
        $landerPage->title = $requestData['title'];
        $landerPage->description = $requestData['description'];
        $landerPage->keywords = $requestData['keywords'];
        $landerPage->save();

        return redirect()->back()->with('success', 'Мета Данные сохранены успешно!');
    }
}
