<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Laravel\Socialite\Facades\Socialite;
use App\SocialsAuth;

class SocialsLoginController extends Controller
{
    //
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::with($provider)->redirect();
        //return Socialite::with($provider)->redirect();
        //return Socialite::driver($provider)->redirect();


//        $clientId = config('services.instagrambasic.client_id');
//        $clientSecret = config('services.instagram.client_secret');
//        $redirectUrl = config( 'services.instagrambasic.redirect');
//        $additionalProviderConfig = ['scope' => 'meta.stackoverflow.com'];
//        $config = new \SocialiteProviders\Manager\Config($clientId, $clientSecret, $redirectUrl, $additionalProviderConfig);
//        return Socialite::with('instagrambasic')->setConfig($config)->redirect();


        //dd($provider);
        //return Socialite::with('instagrambasic')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */

    static function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }else{
                        return '127.0.0.1';
                    }
                }
            }
        }
    }


    public function handleProviderCallback($provider)
    {

        $user = Socialite::driver($provider)->user();

        $newRecordArray = [
            'provider'=>$provider,
            'uid'=>$user->id,
        ];

        // link
        if($provider=='vkontakte'){
            $newRecordArray['link']= 'https://vk.com/id'.$user->id;
        }elseif ($provider=='facebook'){
            $newRecordArray['link']= 'https://www.facebook.com/profile.php?id='.$user->id;
        }elseif ($provider=='instagrambase'){
            $newRecordArray['link']= 'https://www.instagram.com/'.$user->nickname;
        }

        // avatar
        if($provider=='vkontakte'){
            $newRecordArray['avatar_provider']= $user->avatar;
        }elseif ($provider=='facebook'){
            $newRecordArray['avatar_provider']= $user->avatar;
        }elseif ($provider=='instagrambase'){
            $newRecordArray['avatar_provider']=  $user->avatar; // ????
        }
        //$newRecordArray['avatar_provider'] = urlencode($newRecordArray['avatar_provider']);

        if(!empty($user->email)){
            $newRecordArray['mail'] = $user->email;
        }else{
            $newRecordArray['mail'] = 'none';
        }
        //dd($provider,$user,$newRecordArray);

        $newRecord  = new SocialsAuth();
        $newRecord -> token = $user->token;
        $newRecord -> csrf = csrf_token();
        $newRecord -> ip = $this->getIp();
        $newRecord -> uid = $newRecordArray['uid'];
        $newRecord -> provider = $newRecordArray['provider'];
        $newRecord -> link = $newRecordArray['link'];
        $newRecord -> name = $user->name;
        $newRecord -> mail = $newRecordArray['mail'];
        $newRecord -> avatar_provider = $newRecordArray['avatar_provider']; // ????
        $newRecord -> avatar = $newRecordArray['avatar_provider']; // ????
        $newRecord -> save();

        setcookie("token", $user->token, time()+1800, "/");
        setcookie("provider", $newRecordArray['provider'], time()+1800, "/");
        setcookie("name", $user->name, time()+1800, "/");
        setcookie("avatar", $newRecordArray['avatar_provider'] , time()+1800, "/");

        return view('socials.index');

        // $user->token;
    }
}
