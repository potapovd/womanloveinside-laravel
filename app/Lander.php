<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


/**
 * App\Lander
 *
 * @package App
 * @property int                             $id
 * @property Line                            $slug
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Lander newModelQuery()
 * @method static Builder|Lander newQuery()
 * @method static Builder|Lander query()
 * @method static Builder|Lander whereCreatedAt($value)
 * @method static Builder|Lander whereId($value)
 * @method static Builder|Lander whereSlug($value)
 * @method static Builder|Lander whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string|null $name
 * @property string|null $keywords
 * @property string|null $description
 * @property string|null $title
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Lander whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Lander whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Lander whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Lander whereTitle($value)
 */
class Lander extends Model
{
    //


    public function getLines()
    {
        return $this->belongsToMany('App\Line')->withTimestamps();
    }


    public function getText()
    {
        return $this -> belongsToMany('App\Text')->withTimestamps();
    }

    public function getIcon()
    {
        return $this -> belongsToMany('App\Icon')->withTimestamps();
    }

    public function getComment()
    {
        return $this -> belongsToMany('App\Comment')->orderBy('id','desc')->withTimestamps();
    }
}
