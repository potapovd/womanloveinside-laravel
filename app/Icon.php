<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Icon
 *
 * @property int $id
 * @property string|null $icon
 * @property string|null $text
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Icon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Icon newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Icon query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Icon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Icon whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Icon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Icon whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Icon whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Icon whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Icon extends Model
{
    //
    public function getLander()
    {
        return $this->belongsToMany('App\Lander')->withTimestamps();
    }
}
