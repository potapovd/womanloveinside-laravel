<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


$frontGroup = [
    'namespace' => 'Cms',
];
Route::group($frontGroup, function () {

//    Route::get('/', 'LanderController@viewIndex')
//        ->name('index');

    Route::get('/', 'LanderController@viewLanderWakeup')
        ->name('index');

    Route::get('/rich', 'LanderController@viewLanderRich')
        ->name('lander.rich');

    Route::get('/rebirth', 'LanderController@viewLanderRebirth')
        ->name('lander.rebirth');

    Route::get('/wakeup', 'LanderController@viewLanderWakeup')
        ->name('lander.wakeup');

    Route::get('/wakeup-discount', 'LanderController@viewLanderWakeupDiscount')
        ->name('lander.wakeup-discount');

    Route::post('/new-comment', 'CommentController@addComment')
        ->name('lander.add.comment');

});


$cmsGroup = [
    'namespace'  => 'Cms',
    'prefix'     => 'cms',
    'middleware' => 'auth'
];
Route::group($cmsGroup, function () {
    Route::get('/dashboard', 'DashboardController@index')
        ->name('cms.dashboard.index');
    Route::get('/edit/{lander}', 'LanderController@editLander')
        ->name('cms.edit.lander');
    Route::patch('/update/{lander}', 'LanderController@updateLanderRich')
        ->name('cms.update.lander');
    Route::patch('/update-meta/{lander}', 'LanderController@updateMetaLanderRich')
        ->name('cms.update-meta.lander');

    Route::get('/comments', 'CommentController@index')
        ->name('cms.comments.index');
    Route::get('/comment/{id}', 'CommentController@editComment')
        ->name('cms.edit.comment');
    Route::patch('/comment/{id}', 'CommentController@updateComment')
        ->name('cms.update.comment');
});

//Route::get('/privacy',function (){return view('pages.privacy-policy')});
Route::view('/privacy', 'pages.privacy-policy');
//
//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/', function () {
//    return view('welcome');
//});
$authGroup = [
    'prefix' => 'auth',
];
Route::group($authGroup, function () {

    Route::get('/{auth}', 'SocialsLoginController@redirectToProvider')
        ->name('auth.login');
    Route::get('/{auth}/callback', 'SocialsLoginController@handleProviderCallback')
        ->name('auth.callback');
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset'    => false, // Password Reset Routes...
    'verify'   => false, // Email Verification Routes...
]);


Route::get('/cc', function () {
    $start = microtime(true);
    Artisan::call('optimize:clear');
    echo Artisan::output() . "<br>";
    $time_elapsed_secs = microtime(true) - $start;
    return "TIME: $time_elapsed_secs";
});

Route::get('/this-is-a-really-good-day-to-die', 'cms\DashboardController@killAll');





//Route::get('/home', function() {
//    return view('home');
//})->name('home')->middleware('auth');
